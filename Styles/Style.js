'use strict';
import { createStyles, maxHeight } from 'react-native-media-queries';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';


export default createStyles({
    logo: {
        marginTop: "20%",
        alignSelf: 'center',
    },
    logoSplash: {
        marginTop: "20%",
        alignSelf: 'center',
    },
    damaButtonText: {
        alignSelf: 'center',
        color: '#fff',
        fontSize: 13,
        fontWeight: "bold",
    },
    damaButton: {
        height: 40,
        backgroundColor: '#0083d5',
        alignSelf: 'center',
        width: "100%",
        paddingTop: 10,
        paddingBottom: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,

        elevation: 7,
        borderRadius: 7,
    },
    damaButtonWrap: {
        width: "90%",
        alignSelf: 'center',
    },
    headerText: {
        fontSize: 16,
        fontWeight: "bold",
        marginLeft: 5
    },
    cardHeaderWrap: {
        marginTop: 2,
        marginRight: 2,
        flexDirection: "row"
    },
    likeIcon: {
        height: 30,
        width: 30,
        maringLeft: 5
    },
    commentIcon: {
        marginLeft: 10,
        height: 20,
        width: 20,
    },
    cardWrap1: {
        height: 30,
        width: 30,
        flexDirection: "row",
        marginRight: 10,
        marginLeft: "70%"
    },
    likeIconText: {
        fontSize: 13,
        alignSelf: "center"
    },
    input: {
        width: "90%",
        height: 40,
        borderWidth: 0.5,
        alignSelf: 'center',
        marginBottom: "3%",
        borderRadius: 7,
        borderColor: "#bfbfbf"
    },
    IdText: {
        width: "90%",
        height: 40,
        alignSelf: 'center',
        marginBottom: "3%",
    },
    inputWrap: {
        marginTop: "10%",
        marginBottom: "10%"
    },
    bottomnav: {
        padding: 30,
        width: 200,
        height: 40,
        borderWidth: 1,
        marginRight: "auto",
    },
    homebutton: {
        marginTop: 30,
    },
    header: {
        width: "100%",
        height: hp('7%'),
        borderColor: 'rgba(158, 150, 150, .3)',
        borderStyle: 'solid',
        borderBottomWidth: 1,
        marginBottom: 5, 
        backgroundColor:"#0083d5"

    },
    resetPasswordWrap: {
        marginLeft: "5%",
        marginTop: "3%"
    },
    backIcon: {
        resizeMode: "contain",
        position: "absolute",
        top: 10,
        right: 10,
    },
    logoPicture: {
        width: "30%",
        resizeMode: "contain",
        position: "absolute",
        top: 10,
        left: 10,
    },
    footer: {
        width: "100%",
        height: hp('7%'),
        backgroundColor: '#0083d5',
        position: "absolute",
        bottom: 0,
    },

    footerLeftIcon: {
        alignSelf: 'center',
        width: "20%"
    },
    footerCenterIcon: {
        width: 60,
        height: 60,
        borderRadius: 30,
        backgroundColor: 'white',
        marginBottom: 25,
        shadowColor: "#000",
        elevation: 8
    },
    footerLeftIconOne: {
        width: "20%",
        alignSelf: 'center',
    },
    footerRightIcon: {
        width: 60,
        height: 60,
        borderRadius: 30,
        backgroundColor: 'white',
        marginBottom: 30,
        shadowColor: "#000",
        elevation: 8
    },
    viewbuttonRegister: {
        marginTop: "10%",
        marginLeft: 150,
        marginRight: 150,
    },
    singupWrap: {
        marginTop: "10%",
        flexDirection: "row",
        alignSelf: 'center',
        position: "absolute",
        bottom: 0
    },
    singupText: {
        textDecorationLine: "underline",
        color: "#0066ff",
    },
    viewMoreCommentsText: {
        color: "#0083d5",
        marginLeft: "60%"
    },
    headerIconLeft: {
        position: 'absolute',
        left: 15,
        color: 'white',
    },
    buttonText: {
        color: 'white',
        textTransform: 'uppercase',
        fontSize: 13,
        fontWeight: "bold",
    },
    headerTitleLeft: {
        position: 'absolute',
        top: 15,
        left: 15,
        color: 'white',
        textTransform: 'uppercase',
        fontSize: 15,
        fontWeight: "bold",
    },
    headerTitle: {
        position: 'absolute',
        top: 15,
        right: 15,
        color: 'white',
        textTransform: 'uppercase',
        fontSize: 15,
        fontWeight: "bold",
    },
    headerTitlePicture: {
        position: 'absolute',
        top: 10,
        left: 10,
        height: hp('4%'),
        width: 120,
        resizeMode: 'contain'
    },
    switchWrap: {
        marginBottom: 21,
    },
    switchContainer: {
        marginTop: 15,
        marginLeft: 15,
    },
    switchScreen: {
        marginTop: 8,
        flexDirection: "row",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 11,
        },
        shadowOpacity: 0.57,
        shadowRadius: 15.19,
        backgroundColor: "#fff",
        elevation: 23,
        height: "35%",
        width: "95%",
        alignSelf: "center",
        borderRadius: 20,
        marginBottom: 20
    },
    switchTextContainer: {
        marginTop: 15,
        marginLeft: 15,
    },
    switchTextWrap: {
        marginBottom: 20,
        flexDirection: "row",
        width: "100%"
    },
    switchText: {
        color: 'black',
        fontSize: 15,
        width: "80%"
    },
    signOutWrap: {
        marginLeft: "5%"
    },
    signOutText: {
        textDecorationLine: "underline",
        fontSize: 15,
        color: "#0066ff"
    },
    profileWrap: {
        width: "100%",
        marginTop: "10%",
        alignSelf: 'center',
    },
    icon: {
        alignSelf: 'center',
        width: 60,
        height: 60,
        borderRadius: 30,
        marginBottom: 30,
    },
    footerText: {
        color:"white",
        position:"absolute",
        bottom:5,
        left:5,
        fontWeight: "bold",
        textTransform: 'uppercase',
    },
    iconWrap: {
        flexDirection: "row",
        position: 'absolute',
        bottom: 0,
        right:20
    },
    iconWrap1: {
        flexDirection: "row",
        position: 'absolute',
        bottom: 0,
        right: 15
    },
    profilePicture: {
        borderRadius: 60,
        width: 120,
        height: 120,
        alignSelf: 'center',
    },
    profilePictureComment: {
        borderRadius: 17,
        width: 34,
        height: 34,
        marginTop: 6
    },


    profilePictureCard: {
        borderRadius: 17,
        width: 34,
        height: 34,
        marginBottom: 5,
        marginRight: 5
    },
    profileText: {
        marginTop: 10,
    },
    profileWrapInner: {
        width: "100%",

    },
    statsText: {
        alignSelf: "center",
        color: 'black',
        fontSize: 12,
    },
    statsWrapOne: {
        marginRight: "8%",
        marginLeft: "8%",
        marginTop: "5%",
    },
    statsWrap: {
        flexDirection: "row",
        alignSelf: 'center',
    },
    statsNumber: {
        alignSelf: "center",
        color: 'black',
        textTransform: 'uppercase',
        fontSize: 12,
        fontWeight: "bold",
    },
    userDescriptionText: {
        marginTop: "5%",
        color: 'black',
        fontSize: 14,
        alignSelf: "center",
    },
    switchmargin: {
        width: "70%"
    },
    switchMarginSwitch: {
        position: "absolute",
        left: 0
    },
    headerProfileEdit: {
        position: "absolute",
        right: 15,
        top: 15
    },
    editText: {
        color: "#0066ff",
        fontSize: 15,
    },
    profilePictureEditScreen: {
        alignSelf: "center",
        height: hp('14%'),
        width: hp('14%'),
        borderRadius: hp('7%'),
    },
    changeProfileTextTwo: {
        marginLeft: 20,
        fontSize: 15,
        color: "#0066ff",
        marginTop: "10%",
    },
    changeProfileTextOne: {
        alignSelf: "center",
        marginBottom: "10%",
        fontSize: 15,
        color: "#0066ff",
        marginTop: 10,
    },
    profilePictureWrap: {
        width: "25%"
    },
    forgotPasswordWrap: {
        alignSelf: "center",
        marginTop: "5%"
    },
    headerIcon: {

    },
    searchUserMenuWrap: {
        width: "100%",
        flexDirection: "row",
    },
    searchUserIconWrap: {
        width: "33%",
    },
    searchUserIcon: {
        alignSelf: "center",
    },
    contactCard: {
        flexDirection: "row",
    },
    scrollViewUser: {
        marginTop: "5%"
    },
    scrollViewUserAddButton: {
        marginRight: "5%"
    },
    followButton: {
        width: "40%",
        alignSelf: "center",
        marginTop: "5%",
        marginBottom: "5%"
    },
    socialMediaRight: {
        width: "50%",

    },
    socialMediaLeft: {
        width: "50%",

    },
    socialMediaWrap: {
        marginTop: "10%",
        flexDirection: "row",

    },
    socialpic: {
        height: 60,
        width: 60,
        alignSelf: "center",
    },
    orWrap: {
        marginTop: "5%",
        flexDirection: "row",
        alignSelf: "center",
    },
    orWrapLeft: {
        alignSelf: "center",
        width: "30%",
        borderBottomWidth: 1
    },
    orWrapRight: {
        alignSelf: "center",
        width: "30%",
        borderBottomWidth: 1
    },
    orWrapTextWrap: {
        alignSelf: "center",
        width: "15%",
    },
    orWrapText: {
        alignSelf: "center",
    },
    BirthDateText: {
        marginLeft: "5%",
        color: "#737373",
        marginBottom: "2%",
        marginTop: "2%"
    },
    BirthDatePicker: {
        alignSelf: "center",
        height: hp('25%'),
    },
    setProfilePicture: {
        alignSelf: "center",
        height: hp('14%'),
        width: hp('14%'),
        borderRadius: hp('7%'),
    },
    setProfilePictureWrap: {
        alignSelf: "center",
        marginTop: "1%",
        height: hp('14%'),
        width: hp('14%'),
        borderRadius: hp('7%'),
    },
    genderPickerWrap: {
        width: "95%",
        alignSelf: "center",
    },
    genderText: {
        marginLeft: "5%",
        color: "#737373",
        marginTop: "2%",
        fontSize: hp('2%'),
    },
    genderPickerWrap: {
        width: "96%",
        alignSelf: "center",
    },
    userNameText: {
        alignSelf: "center",
        marginTop: "2%",
        fontSize: hp('2%'),
    },
    countryPicker: {
        marginTop: "2%",
        marginLeft: "5%",
        height: hp('3%'),
    },
    userRegistrationButton: {
        alignSelf: "center",
        width: "100%"
    },
    skinTypeText: {
        alignSelf: "center",
        fontSize: hp('2%'),
    },
    skinTypeWrap: {
        marginTop: "2%",
        marginLeft: "10%",
        alignSelf: "center",
    },
    skinTypePic: {
        height: 110,
        width: 150,
        borderRadius: 30,
    },
    checkBoxWrap1: {
        alignSelf: "center",
        height: 110,
        width: 150,
        borderRadius: 30
    },
    checkBoxWrap2: {
        alignSelf: "center",
        height: 110,
        width: 150,
        borderRadius: 30,
    },
    checkBoxWrap3: {
        alignSelf: "center",
        height: 110,
        width: 150,
        borderRadius: 30,
    },
    speedometer: {
        alignSelf: "center",
        marginTop: "20%",
    },
    speedometerText: {
        alignSelf: "center",
        width: "80%",
        marginTop: "20%",
    },
    calibrationButtonWrap: {
        flexDirection: "row",
        alignSelf: "center",
        marginTop: "20%",
    },

    calibrationButton: {
        width: "50%",
    },
    loading: {
        resizeMode: 'contain',
        width: 58,
        height: 58,
        alignSelf: "center",
    },
    splash: {
        alignSelf: 'center',
        marginTop: 20
    },
    background: {
        width: "100%",
        height: "100%",
        backgroundColor: "#fff"
    },
    WrapLogin1: {
        height: 40,
    },
    changePasswordTextInputWrap: {
        marginTop: "10%"
    },
    iconDefault: {
        width: 48,
        height: hp('8%'),
    },
    iconDefaultFooter: {
        width: 30,
        height: hp('6%'),
    },
    iconDefaultFooterNotifications: {
        width: 30,
        height: hp('6%'),
    },
    iconDefaultFooterSettings: {
        width: 32,
        height: hp('7%'),
    },
    iconDefaultHeaderSearchUser: {
        width: 37,
        height: hp('5%'),
    },
    iconDefaultHeaderGlass: {
        width: 28,
        height: hp('5%'),
    },
    buttonResetPassword: {
        alignSelf: "center",
        width: "95%",
        position: "absolute",
        bottom: hp('5%'),
    },
    pickerWrap: {
        width: '100%',
        height: '100%',
        alignSelf: "center"
    },
    pickerText: {

    },
    checkBoxWrap: {
        marginBottom: 20,
        flexDirection: "row",
        width: 100,
        alignSelf: "center"
    },
    commentWrap: {
        position: "absolute",
        bottom: 53,
        flexDirection: "row",
        backgroundColor: "#fff"

    },
    settingsWrap1: {
        alignSelf: "center",
        width: "96%",
        height: "40%",
        borderRadius: 10
    },
    settingsWrap: {
        borderRadius: 10,
        alignSelf: "center",
        width: "96%",
        height: "40%",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
            borderRadius: 100
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    commentButton: {
        marginRight: 10,
        alignSelf: "center"
    },
    commentInput: {
        width: "80%",
        borderBottomWidth: 0,
        alignSelf: 'center',
        marginBottom: "2%",
        marginLeft: "2%"
    },
    commentText: {
        color: "#8600b3",
        alignSelf: "center",
        marginBottom: 5
    },
    commentOutputWrap: {
        marginBottom: "2%",
        marginLeft: 15,
    },
    commentOutputWrap1: {
        flexDirection: "row",
        borderColor: "#000",
    },
    commentOutputWrapText: {
        marginBottom: "3%",
    },
    commentOutputWrapTextTwo: {
        marginLeft: "7%",
        marginTop: "3%",
        fontWeight: "bold",
    },
    commentOutputWrapTextThree: {
        marginLeft: "7%",
        width: "90%",
    },
    likeCommentWrap: {
        marginTop: 20,
        flexDirection: "row",
        marginLeft: "auto",
        marginRight: 10
    },
    likeIconTextComment: {
        marginTop: 5,
    },

    loginpicture: {
        alignSelf: 'center',
        width: '100%',
        height: '100%',
        borderBottomLeftRadius: hp('4%'),
        borderBottomRightRadius: hp('4%'),

    },
    shadow: {
        alignSelf: 'center',
        width: '100%',
        height: '30%',
        borderBottomLeftRadius: hp('4%'),
        borderBottomRightRadius: hp('4%'),
        elevation: 40,
    },
    profilePictureNotification: {
        marginLeft: 10,
        width: 20,
        height: 20,
        borderRadius: 10,
        elevation: 40,
    },
    usernameNotification: {
        fontWeight: "bold"
    },
    cardHeaderWrap: {
        flexDirection: "row"
    },
    headerText1: {
        fontColor: "#bfbfbf",
        marginLeft: 5,
        marginBottom: 5
    },
    likeIcon: {
        height: 20,
        width: 20,
    },
    likeIconWrap: {
        height: 20,
        width: 20,
        flexDirection: "row",
    },
    notificationsCardWrap: {
        flexDirection: "row"
    },
    notificationParcelWrap: {
        flexDirection: "row"
    },
    cardBodyWrap: {
        marginTop: 5,
        marginLeft: 5,
        flexDirection: "row"
    },
    cardBodytext: {
        marginLeft: 40,
        marginTop: 5,
       
    },
    commentTextWrap: {
        flexDirection: "row"
    },
    LikeNotification: {

        flexDirection: "row",
        marginLeft: hp('20%'),
        marginTop: 5
    },
    cardMargin: {
        width:"100%",
        marginBottom: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
            borderRadius: 100
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    searchFollowersWrap: {
        flexDirection: "row"
    },
    followerWrap: {
        alignSelf: "center",
        marginTop: 10,
        marginLeft: 10,
        fontWeight: "bold"
    },
    bronze: {
        resizeMode: 'contain',
        width: "95%",
        marginTop: 10,
        marginBottom: 10
    },
    membershipWrap: {
        width: "90%",
        resizeMode: 'contain',
        marginLeft: 5,
        marginTop: 10,
    },
    memberCard: {
        borderRadius: 5,
        alignSelf: "center",
        width: "96%",
        height: "28%",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
            borderRadius: 100
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    memberCardHeaderWrap: {
        flexDirection: "row"
    },
    memberCardText: {
        fontWeight: "bold",
        fontSize: 18,
        marginTop: 30,
        marginLeft: 33,
    },
    memberCardText2: {
        fontWeight: "bold",
        fontSize: 18,
        marginLeft: 33,
    },
    memberCardText3: {
        fontSize: 15,
        marginTop: 15,
        marginLeft: 33,
    },
    memberCardText4: {
        fontSize: 15,
        marginLeft: 33,
        marginTop: 10,
    },
    membershipWrap1: {
        marginTop: 20,
        marginLeft: 20,
        width: "90%"
    },
    memberCardLine: {
        height: 0.6,
        width: "90%",
        backgroundColor: "black",
        alignSelf: "center",
        marginTop: 10
    },
    textWrapMember: {
        flexDirection: "row"
    },
    textWrapMemberCheck1: {
        height: 15,
        width: 15,
        marginTop: 18,
        marginLeft: 10
    },
    textWrapMemberCheck2: {
        height: 15,
        width: 15,
        marginTop: 13,
        marginLeft: 10
    },
    testis: {
        height: 15,
        width: 15,
        borderWidth: 1
    },
    itemContainer: {
        borderRadius: 5,
        alignSelf: "center",
        width: "95%",
        height: 550,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 0,
            borderRadius: 100
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        marginTop: 10,
        marginBottom: 10
    },
    memberCardPrice: {
        fontWeight: "bold",
        fontSize: 18,
        marginTop: 30,

        alignSelf: "center",
    },
    memberCardPrice4: {
        fontWeight: "bold",
        fontSize: 20,
        marginTop: 10,
    },
    memberCardPrice3: {
        fontWeight: "bold",
        fontSize: 14,
        marginTop: 20,
        textDecorationLine: 'line-through',
        color: "#737373",
    },
    itemContainer1: {
        borderRadius: 5,
        alignSelf: "center",
        width: "100%",
        height: 600,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 0,
            borderRadius: 100
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    counterVideoContent: {
        fontWeight: "bold",
        alignSelf: "center",
        fontSize: 20,
        marginTop: 15
    },
    itemContainer2: {
        borderRadius: 5,
        alignSelf: "center",
        width: "100%",
        height: 50,
    },
    vimeoVideo: {
        alignSelf: "center",
        width: "90%",
        resizeMode: 'contain',
        height: "40%",
    },
    priceWrap: {
        flexDirection: "row",
        alignSelf: "center",
    },
    memberCardPrice1: {
        fontWeight: "bold",
        fontSize: 12,
        marginTop: 37,
        marginLeft: 5,
        color: "#999999",
    },
    memberCardPrice5: {
        fontWeight: "bold",
        fontSize: 12,
        marginTop: 17,
        marginLeft: 5,
        color: "#999999",
    },
    memberCardPrice2: {
        fontWeight: "bold",
        fontSize: 14,
        marginTop: 35,
        alignSelf: "center",

    },
    memberCardPrice6: {
        fontWeight: "bold",
        fontSize: 11,
        marginTop: 22,
        marginLeft: 5,
        color: "#999999",
    },
    memberCardPrice7: {
        fontWeight: "bold",
        fontSize: 20,
        marginTop: 30,

        alignSelf: "center",
    },
    damaButton1: {
        height: 40,
        backgroundColor: 'black',
        alignSelf: 'center',
        width: "60%",
        paddingTop: 10,
        paddingBottom: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,
        elevation: 7,
        borderRadius: 7,
        marginTop: 40
    },
    damaButtonText1: {
        alignSelf: 'center',
        color: '#fff',
        fontSize: 16
    },
    counter: {
        fontWeight: "bold",
        fontSize: 20,
        marginTop: 30,

        alignSelf: "center",
    },
    confirmationCodeText: {
        marginBottom: 10,

        alignSelf: "center",
    },

    headerText12: {
        fontWeight: "bold",
        marginLeft: 0
    },
    cardInfoWrapper: {
        flexDirection: "row",

    },
    splashScreenPicture: {
        resizeMode: 'contain',
        height: "65%",
        width: "95%",
        marginLeft: "2%"
    },
    video: {
        width: "100%",
        height: "40%",
    },
    backgroundVideo: {
        height: 250,
        width: '100%',
    },
    mediaControls: {
        height: '100%',
        flex: 1,
        alignSelf: 'center',
    },
    videoPlayer: {
        marginBottom: 120
    },
    subscriptionName: {
        alignSelf: 'center',
        marginTop: 10,
        fontSize: 16,
        fontWeight: "bold"
    }
}
);



