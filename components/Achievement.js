import React from 'react';
import { ListItem, Text, Left, Body, Thumbnail } from 'native-base';
import styles from '../styles/styles';
import moment from 'moment';
import { withTranslation } from 'react-i18next';

const icons = {
  PERMANENT_GUEST: require('../../assets/specialAchievementIcons/PERMANENT_GUEST.png'),
  SOCIAL_WORKER: require('../../assets/specialAchievementIcons/SOCIAL_WORKER.png'),
  TYPIST: require('../../assets/specialAchievementIcons/TYPIST.png'),
  CREATOR: require('../../assets/specialAchievementIcons/CREATOR.png'),
  COMPETITION_TYPE: require('../../assets/specialAchievementIcons/COMPETITION_TYPE.png'),
  ANNIVERSARY: require('../../assets/specialAchievementIcons/ANNIVERSARY.png'),
  DARLING_OF_THE_CROWDS: require('../../assets/specialAchievementIcons/DARLING_OF_THE_CROWDS.png'),
  INTERESTED_IN_MANY_WAYS: require('../../assets/specialAchievementIcons/INTERESTED_IN_MANY_WAYS.png')
};
const greyIcons = {
  PERMANENT_GUEST: require('../../assets/specialAchievementIcons/GREY_PERMANENT_GUEST.png'),
  SOCIAL_WORKER: require('../../assets/specialAchievementIcons/GREY_SOCIAL_WORKER.png'),
  TYPIST: require('../../assets/specialAchievementIcons/GREY_TYPIST.png'),
  CREATOR: require('../../assets/specialAchievementIcons/GREY_CREATOR.png'),
  COMPETITION_TYPE: require('../../assets/specialAchievementIcons/GREY_COMPETITION_TYPE.png'),
  ANNIVERSARY: require('../../assets/specialAchievementIcons/GREY_ANNIVERSARY.png'),
  DARLING_OF_THE_CROWDS: require('../../assets/specialAchievementIcons/GREY_DARLING_OF_THE_CROWDS.png'),
  INTERESTED_IN_MANY_WAYS: require('../../assets/specialAchievementIcons/GREY_INTERESTED_IN_MANY_WAYS.png')
};

export const Achievement = ({ createdAt, xp, actionType, t }) => {
  return <ListItem noIndent>
    <Left>
      <Thumbnail
        small
        source={createdAt ? icons[actionType] : greyIcons[actionType] }
      />
    </Left>
    <Body style={styles.achievementBody}>
      <Text style={styles.content}>
        {t('ACTION_TYPE.' + actionType)}
      </Text>
      <Text note={!createdAt} style={styles.achievementContent}>
        {t('ACTION_TYPE.' + actionType + '.DESCRIPTION')}
      </Text>
      <Text note={!createdAt} style={styles.achievementContent}>
        {createdAt ? t('screens.profileScreen.reached') + ' ' + moment(createdAt).format('L') : t('screens.profileScreen.notReachedYet')} | + {xp} XP
      </Text>
    </Body>
  </ListItem>;
};

export default withTranslation()(Achievement);
