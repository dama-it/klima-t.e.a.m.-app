
import { Ionicons } from '@expo/vector-icons';
import React from 'react';

const styles = {
  button: {
    backgroundColor: 'transparent',
    position: 'absolute',
    zIndex: 99,
    bottom: 45,
    alignSelf: 'center',
    shadowColor: 'black',
    shadowOpacity: 0.15,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    elevation: 3
  }
};

export const AddButton = ({ navigation }) => <Ionicons
  name="ios-add-circle-outline"
  color="#000"
  size={24}
  onPress={() => { navigation.navigate('Overlay'); }}
  style={styles.button} />;
