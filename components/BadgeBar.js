// React imports
import { CardItem, Thumbnail } from 'native-base';
import React from 'react';
import styles from '../styles/styles';
import { getIcon } from '../utils/imageUtils';

export const BadgeBar = ({ post }) => <CardItem>
  {post.competition.cancelledAt != null && post.competition.cancelReason != null
    ? <Thumbnail
      small
      source={{ uri: getIcon('challenge-icon-cancelled.png') }}
    />
    : null}

  {post.competition.cancelledAt != null && post.competition.cancelReason == null
    ? <Thumbnail
      style={styles.badgeStyle}
      small
      source={{ uri: getIcon('challenge-icon-lost.png') }}
    />
    : null}

  {post.competition.finishDate != null
    ? <Thumbnail
      style={styles.badgeStyle}
      small
      source={{ uri: getIcon('challenge-icon-done.png') }}
    />
    : null}

  {// new
    post.status === 1
      ? <Thumbnail
        style={styles.badgeStyle}
        small
        source={{ uri: getIcon('challenge-icon-new.png') }}
      />
      : null}

  {// updated
    post.status === 2
      ? <Thumbnail
        style={styles.badgeStyle}
        small
        source={{ uri: getIcon('challenge-icon-update.png') }}
      />
      : null}
</CardItem>;
