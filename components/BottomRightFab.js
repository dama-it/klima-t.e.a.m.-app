import React from 'react';
import styles from '../styles/styles';
import { useBottomTabBarHeight } from '@react-navigation/bottom-tabs';
import { Fab, Icon } from 'native-base';

const BottomRightFab = ({ onPress, iconName, iconType }) => (
  <Fab
    style={styles.fab}
    containerStyle={{ marginBottom: useBottomTabBarHeight() || 0 }}
    position="bottomRight"
    onPress={onPress}
  >
    <Icon type={iconType} name={iconName} />
  </Fab>
);
export default BottomRightFab;
