
import { Ionicons } from '@expo/vector-icons';
import { Text, View } from 'native-base';
import React from 'react';
import { connect } from 'react-redux';
import styles from '../styles/styles';

const MessagesTabBarIcon = ({ newMessages, focused, nameFocused, tintColor, name }) => (
  <>
    <Ionicons
      name={focused ? nameFocused : name}
      color={tintColor}
      size={24}
    />
    {
      newMessages > 0
        ? <View style={styles.newMessagesIndicator}>
          <Text style={styles.newMessagesCounter}></Text>
        </View> : null
    }
  </>
);
const mapStateToProps = (state) => {
  return {
    newMessages: state.identity.newMessages
  };
};
export default connect(mapStateToProps)(MessagesTabBarIcon);
