import React from 'react';
import { Text, View } from 'native-base';
import { withTranslation } from 'react-i18next';
import { TouchableOpacity } from 'react-native-gesture-handler';
import styles from '../styles/styles';

const CloseButton = ({ close, t }) => {
  return <View style={styles.bottomCloseButton}>
    <TouchableOpacity onPress={close}>
      <Text>{t('screens.CompetitionCancelScreen.close')}</Text>
    </TouchableOpacity>
  </View>;
};

export default withTranslation()(CloseButton);
