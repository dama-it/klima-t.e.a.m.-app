import { Button, H1, Text, Thumbnail, View } from 'native-base';
import React from 'react';
import { withTranslation } from 'react-i18next';
import { Image } from 'react-native';
import { getBannerSrc, getHighResolutionLogoSrc } from '../utils/imageUtils';
import styles from '../styles/styles';
import { connect } from 'react-redux';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import ClubContactInfo from '../screens/club/ClubContactInfo';
import ClubAccordion from '../screens/club/ClubAccordion';

const ClubDetails = ({ club, userIsMember, becomeMember, clubMembers, t }) => {
  const renderClubMembers = (setting) => {
    if (setting && setting.value === 'none') {
      return null;
    }

    if (setting && setting.value === 'exact') {
      if (clubMembers) {
        if (clubMembers.length === 1) {
          return clubMembers.length + ' ' + t('screens.club.member');
        }
        return clubMembers.length + ' ' + t('screens.club.members');
      }
      return 0 + ' ' + t('screens.club.members');
    }

    if (setting && setting.value === 'range') {
      if (clubMembers.length < 50) return t('screens.club.members.0And50') + ' ' + t('screens.club.members');

      if (clubMembers.length < 100) {
        return t('screens.club.members.50And100') + ' ' + t('screens.club.members');
      } else {
        return t('screens.club.members.moreThan100') + ' ' + t('screens.club.members');
      }
    }
  };

  const getMemberCountViewSetting = () => {
    return club.settings ? club.settings?.find(concreteSetting =>
      concreteSetting.setting.name === 'MEMBER_VIEW') : false;
  };

  return (
    <>
      <Image source={{ uri: getBannerSrc(club) }} style={styles.topicBanner}/>
      <Thumbnail source={{ uri: getHighResolutionLogoSrc(club) }} style={styles.clubLogo} />
      <View style={[styles.topicHeading, styles.content]}>
        <View style={{ padding: 10 }}>
          <H1 style={styles.topicHeading}>
            {club.name}
          </H1>
          <Text style={styles.topicMemberCount}>
            {renderClubMembers(getMemberCountViewSetting())}
          </Text>
        </View>
        <View style={styles.topicRowContainer}>
          <View style={{ width: '50%', padding: 10 }}>
            <Button full style={styles.button} onPress={becomeMember} disabled={userIsMember}>
              <Text uppercase style={styles.buttonText}>
                {userIsMember ? t('screens.club.member') : t('screens.club.becomeMember')}
              </Text>
              {userIsMember && <MaterialCommunityIcons style={{ paddingRight: 5 }} size={20} name="check" /> }
            </Button>
          </View>
        </View>
        <ClubContactInfo name={club.name} contact={club.contact} location={club.location} />
        <ClubAccordion statutes={club.statutes} description={club.description} members={club.members} />
      </View>
    </>
  );
};

const mapStateToProps = state => {
  return {
    clubMembers: state.clubs.clubMembers
  };
};

export default connect(mapStateToProps)(withTranslation()(ClubDetails));
