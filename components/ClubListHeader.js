import { Button, Segment, Text } from 'native-base';
import React from 'react';
import { withTranslation } from 'react-i18next';
import ClubMemberHeader from '../screens/club/ClubMemberHeader';
import EventListHeader from '../screens/club/EventListHeader';
import styles from '../styles/styles';

const ClubListHeader = ({ t, isEventView, setMemberView, setEventView, handleSortEvents, handleSortMembers, archived }) => {
  return (
    <>
      <Segment style={styles.listHeaderSegment}>
        <Button first onPress={setMemberView} active={!isEventView} style={styles.listHeaderSegmentButton}>
          <Text style={ !isEventView ? styles.listHeaderSegmentButtonTextActive : styles.listHeaderSegmentButtonTextInActive }>
            {t('screens.club.members')}
          </Text>
        </Button>
        <Button last onPress={setEventView} active={isEventView} style={styles.listHeaderSegmentButton}>
          <Text style={isEventView ? styles.listHeaderSegmentButtonTextActive : styles.listHeaderSegmentButtonTextInActive }>
            {t('screens.club.events')}
          </Text>
        </Button>
      </Segment>
      {isEventView
        ? <EventListHeader archived={archived} handleSort={handleSortEvents} />
        : <ClubMemberHeader handleSort={handleSortMembers} />
      }
    </>
  );
};

export default withTranslation()(ClubListHeader);
