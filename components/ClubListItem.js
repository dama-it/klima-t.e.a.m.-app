
import { Icon, Body, ListItem, Right, Text, Thumbnail, View } from 'native-base';
import React, { PureComponent } from 'react';
import { TouchableWithoutFeedback } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Menu, { MenuItem } from 'react-native-material-menu';
import styles from '../styles/styles';
import { getLogoSrc } from '../utils/imageUtils';
import { reportClubCall, dismissClubMemberInvitation, leaveClubCall } from '../utils/restUtils';
import { showError, showMessage } from './Messages';

class ClubListItem extends PureComponent {
    _menu = null;

    setMenuRef = ref => {
      this._menu = ref;
    };

    hideMenu = () => {
      this._menu.hide();
    };

    showMenu = () => {
      this._menu.show();
    };

    getClubLogoSrc = () => { return { uri: getLogoSrc(this.props.clubMembership.club) }; }

    renderMenuButton = () =>
      <TouchableOpacity style={{ padding: 10 }} onPress={this.showMenu}>
        <Icon name="md-more" />
      </TouchableOpacity>

    dismissInvitation = () => {
      const invitationId = this.props.clubMembership.id;
      dismissClubMemberInvitation(invitationId).then(success => showMessage('SUCCESS.DISMISSED_INVITATION', this.props.t), e => showError(e.status, this.props.t));
      this.props.onRefreshingInfo();
      this.hideMenu();
    }

    acceptInvitation = () => {
      this.props.navigation.navigate('BecomeMemberScreen', { club: this.props.clubMembership.club });
      this.hideMenu();
    }

    leaveClub = () => { leaveClubCall(this.props.clubMembership.club.id).then(success => this.props.onRefreshingInfo(), e => showError(e.status, this.props.t)); this.hideMenu(); }

    visitClub = () => {
      const isMuted = this.props.mutedClubs.find(club => club.id === this.props.clubMembership.club.id);
      this.props.navigation.navigate('Clubs', { screen: 'ClubDetailScreen', params: { club: this.props.clubMembership.club, isMuted: isMuted, refreshing: this.props.onRefreshingInfo } });
      this.hideMenu();
    }

    reportClub = () => {
      reportClubCall(this.props.clubMembership.club.id).then(success => showMessage('SUCCESS.CLUB_REPORTED', this.props.t), e => showError(e.status, this.props.t));
      this.hideMenu();
    }

    render () {
      const { clubMembership, isClubMember, isInvitation, type, itsMe, t } = this.props;
      if (clubMembership.deletedAt) return null;
      return <ListItem noIndent style={{ height: 60, backgroundColor: '#fff' }}>
        <TouchableWithoutFeedback onPress={this.visitClub}>
          <Body style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
            <Thumbnail small source={this.getClubLogoSrc()}/>

            <View style={{ paddingLeft: 10 }}>
              <Text style={styles.content}>{clubMembership.club.name}</Text>
              {type === 'member' && clubMembership.ranks
                ? <Text numberOfLines={1}>{clubMembership.ranks.map(rank => <Text note>{rank.name} </Text>)}</Text>
                : null}
            </View>
          </Body>
        </TouchableWithoutFeedback>
        <Right>
          <Menu
            ref={this.setMenuRef}
            onDismiss={this._closeMenu}
            button={this.renderMenuButton()}
          >
            <MenuItem key={0} onPress={this.visitClub}><Text style={styles.menuText}>{t('screens.profileScreen.visitClub')}</Text></MenuItem>
            {isClubMember && type === 'member' && itsMe
              ? <MenuItem key={1} onPress={this.leaveClub}><Text style={styles.menuText}>{t('screens.profileScreen.leaveClub')}</Text></MenuItem>
              : null
            }
            {isInvitation && type === 'invitation' && itsMe
              ? <MenuItem key={1} onPress={this.acceptInvitation}><Text style={styles.menuText}>{t('screens.profileScreen.acceptMembership')}</Text></MenuItem>
              : null
            }
            <MenuItem key={2} onPress={this.reportClub}><Text style={styles.menuText}>{t('screens.club.reportClub')}</Text></MenuItem>
          </Menu>
        </Right>
      </ListItem>;
    }
}
export default ClubListItem;
