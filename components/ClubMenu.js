import { Button, Icon, Text } from 'native-base';
import React from 'react';
import { withTranslation } from 'react-i18next';
import { leaveClubCall, muteClubCall, reportClubCall } from '../utils/restUtils';
import { showError, showMessage } from './Messages';
import styles from '../styles/styles';
import { connect } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import Menu, { MenuItem } from 'react-native-material-menu';

const ClubMenu = ({ t, club, isMuted, onRefreshingMembers, userIsMember, becomeMember, allowMembers }) => {
  const navigation = useNavigation();

  var _menu = null;

  const setMenuRef = ref => {
    _menu = ref;
  };

  const hideMenu = () => {
    _menu.hide();
  };

  const showMenu = () => {
    _menu.show();
  };

  const leaveClub = () => {
    leaveClubCall(club.id).then(success => {
      showMessage('SUCCESS.CLUB_LEAVE', t);
      onRefreshingMembers();
    }, e => showError(e.status, t));
    hideMenu();
  };

  const reportClub = () => {
    reportClubCall(club.id).then(success => showMessage('SUCCESS.CLUB_REPORTED', t), e => showError(e.status, t));
    hideMenu();
  };

  const muteClub = () => {
    muteClubCall(club.id).then(success => {
      hideMenu();
      navigation.setParams({ isMuted: !isMuted });
    }, e => {
      showError(e.status);
      hideMenu();
    });
  };

  const join = () => {
    becomeMember();
    hideMenu();
  };

  const MenuButton = <Button transparent onPress={showMenu}>
    <Icon name="md-more" style={styles.mainHeaderIcon}/>
  </Button>;

  return (
    <Menu
      style={{ marginTop: 30 }}
      ref={setMenuRef}
      onDismiss={hideMenu}
      button={MenuButton}
    >
      { allowMembers && !userIsMember
        ? <MenuItem key={0} onPress={join}>
          <Text style={styles.menuText}>
            {t('screens.club.becomeMember')}
          </Text>
        </MenuItem>
        : <MenuItem key={0} onPress={leaveClub}>
          <Text style={styles.menuText}>
            {t('screens.profileScreen.leaveClub')}
          </Text>
        </MenuItem>
      }
      <MenuItem key={2} onPress={reportClub}><Text style={styles.menuText}>{t('screens.club.reportClub')}</Text></MenuItem>
      {userIsMember &&
        <MenuItem key={3} onPress={muteClub}>
          <Text style={styles.menuText}>
            {isMuted ? t('screens.club.unMuteClub') : t('screens.club.muteClub')}
          </Text>
        </MenuItem>}
    </Menu>
  );
};

const mapStateToProps = state => {
  return {
    topic: state.topic ? state.topic.currentTopic : null
  };
};

export default connect(mapStateToProps)(withTranslation()(ClubMenu));
