import moment from 'moment';
import { PropTypes } from 'prop-types';
import React from 'react';
import { withTranslation } from 'react-i18next';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';
import styles from '../styles/styles';
import CommentActions from './CommentActions';

class Comment extends React.PureComponent {
renderIndentationSring = indentation => {
  let indentationString = '';
  if (indentation === undefined || indentation === null) {
    return indentationString;
  }
  indentation = parseInt(indentation);

  for (let i = 1; i < indentation; i++) {
    indentationString += '- ';
  }

  return indentationString;
};

render () {
  const {
    t,
    comment,
    handlePressMenuButton,
    currentUser,
    text,
    createdAt,
    author,
    updatedAt,
    deletedAt,
    level,
    blocked,
    handleUpVote,
    handleDownVote,
    handleReply,
    currentUsersVote
  } = this.props;
  return (
    <View style={localStyles.comment}>
      <View style={localStyles.container}>
        <View style={localStyles.indentationContainer}>
          <Text>
            {this.renderIndentationSring(level)}
          </Text>
        </View>
        <View style={localStyles.contentContainer}>
          <Text style={localStyles.headerText}>{author.username + ' | ' + moment(createdAt).fromNow()}</Text>
          {deletedAt
            ? <Text style={localStyles.deletedText}>{t('components.comment.deletedBy')}</Text>
            : text && !blocked
              ? <Text style={[localStyles.contentText, styles.montserat]}>{text}</Text>
              : <Text style={localStyles.deletedText}>{t('components.comment.authorBlocked')}</Text>}
        </View>
      </View>
      {updatedAt
        ? <View style={localStyles.editedContainer}>
          <Text style={localStyles.editedText}>{t('components.comment.edited')}</Text>
        </View> : null }
      <CommentActions
        editable={author.username === currentUser}
        comment={comment}
        handlePressMenuButton={handlePressMenuButton}
        handleUpVote={handleUpVote}
        handleDownVote={handleDownVote}
        handleReply={handleReply}
        currentUsersVote={currentUsersVote}
        {...comment}
      />
    </View>);
}
}

Comment.propTypes = {
  handleUpVote: PropTypes.func.isRequired,
  handleDownVote: PropTypes.func.isRequired,
  handleReply: PropTypes.func.isRequired,
  handlePressMenuButton: PropTypes.func.isRequired,

  // Comment object shape
  comment: PropTypes.shape({
    text: PropTypes.string,
    createdAt: PropTypes.string.isRequired,
    votes: PropTypes.number,

    levelPath: PropTypes.string,
    blocked: PropTypes.bool,
    deletedBy: PropTypes.user,
    deletedAt: PropTypes.string,
    edited: PropTypes.bool,

    // User object shape
    author: PropTypes.shape({
      username: PropTypes.string.isRequired
    }).isRequired
  }).isRequired
};

Comment.defaultProps = {
  indentation: 0,
  edited: false,
  blocked: false
};

export default withTranslation()(Comment);

const localStyles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    paddingBottom: 5
  },
  contentContainer: {
    paddingLeft: 5
  },
  editedContainer: {
    paddingLeft: 10
  },
  indentationContainer: {
    paddingLeft: 5,
    paddingTop: 5
  },
  text: {
    color: '#000'
  },
  headerText: {
    color: '#808080'
  },
  contentText: {
    lineHeight: 22
  },
  name: {
    fontWeight: 'bold'
  },
  createdAt: {
    color: '#BBB'
  },
  comment: {
    paddingBottom: 15
  },
  deletedText: {
    color: '#A12121'
  },
  editedText: {
    color: '#808080'
  }

});
