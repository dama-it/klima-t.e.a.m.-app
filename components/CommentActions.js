import { Icon, Text, View } from 'native-base';
import React from 'react';
import { withTranslation } from 'react-i18next';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Menu, { MenuItem } from 'react-native-material-menu';
// Style imports
import styles from '../styles/styles';

class CommentActions extends React.PureComponent {
  _menu = null;

  setMenuRef = ref => {
    this._menu = ref;
  };

  hideMenu = () => {
    this._menu.hide();
  };

  showMenu = () => {
    this._menu.show();
  };

  handleUpVote = () => {
    if (this.props.currentUsersVote === false || this.props.currentUsersVote === null) { this.props.handleUpVote(this.props.comment); }
  }

  handleDownVote = () => {
    if (this.props.currentUsersVote === true || this.props.currentUsersVote === null) {
      this.props.handleDownVote(this.props.comment);
    }
  }

  handleReply = () =>
    this.props.handleReply(this.props.comment)

  handlePressEdit = () => {
    this.props.handlePressMenuButton(1, this.props.comment, this.props.editable);
    this.hideMenu();
  }

  handlePressDelete = () => {
    this.props.handlePressMenuButton(2, this.props.comment, this.props.editable);
    this.hideMenu();
  }

  handlePressReport = () => {
    this.props.handlePressMenuButton(3, this.props.comment, this.props.editable);
    this.hideMenu();
  }

  handleBlock = () => {
    this.props.handlePressMenuButton(4, this.props.comment, this.props.editable);
    this.hideMenu();
  }

  handleStartConversation = () => {
    this.props.handlePressMenuButton(5, this.props.comment, this.props.editable);
    this.hideMenu();
  }

  renderMenuButton = () => <Text onPress={this.showMenu} style={styles.moreMenu} >
    <Icon name="md-more" style={styles.threadTools} /></Text>

  render () {
    const {
      t,
      comment,
      currentUsersVote,
      editable,
      upVotesCount,
      downVotesCount,
      deletedAt
    } = this.props;

    return (
      <View style={styles.commentActionContainer}>
        <View style={styles.commentActionContainer}>
          <View style={styles.commentActionRowContainer}>
            <TouchableOpacity onPress={this.handleUpVote} style={styles.commentVoteContainer}>
              {currentUsersVote === true
                ? (<Icon name="md-arrow-round-up" style={styles.active} />)
                : (<Icon name="ios-arrow-round-up" style={styles.threadTools} />)}
            </TouchableOpacity>
            <View style={styles.commentVoteContainer}>
              <Text style={styles.commentVoteCount}>
                {upVotesCount - downVotesCount}
              </Text>
            </View>
            <TouchableOpacity onPress={this.handleDownVote} style={styles.commentDownVoteContainer}>
              {currentUsersVote === false
                ? (<Icon name="md-arrow-round-down" style={styles.active } />)
                : (<Icon name="ios-arrow-round-down" style={styles.threadTools} />)}
            </TouchableOpacity>
          </View>

          <TouchableOpacity onPress={this.handleReply} style={styles.replyButton}>
            <Icon name="ios-undo" style={styles.threadTools} />
            <Text style={styles.replyComment}>
              {t('screens.commentActions.reply')}
            </Text>
          </TouchableOpacity>

          <View
            style={styles.centerActionContainer}>
            {deletedAt && editable
              ? <Text style={styles.moreMenu} >
                <Icon name="md-more" style={styles.threadTools} />
              </Text>
              : <Menu
                ref={this.setMenuRef}
                onDismiss={this._closeMenu}
                button={<Text onPress={this.showMenu} style={styles.moreMenu} >
                  <Icon name="md-more" style={styles.threadTools} /></Text>}
              >
                {editable ? [
                  <MenuItem key={1} onPress={this.handlePressEdit} >
                    {t('screens.commentActions.edit')}
                  </MenuItem>,
                  <MenuItem key={2} onPress={this.handlePressDelete} >
                    <Text style={styles.deleteTextStyle}>{t('screens.commentActions.delete')}</Text>
                  </MenuItem>]
                  : [<MenuItem key={3} onPress={this.handlePressReport} >
                    {t('screens.commentActions.report')}
                  </MenuItem>,
                  <MenuItem key={4} onPress={this.handleBlock} >
                    {comment.blocked
                      ? t('screens.commentActions.unBlockUser')
                      : t('screens.commentActions.blockUser')}
                  </MenuItem>,
                  <MenuItem key={7} onPress={this.handleStartConversation} >
                    {t('screens.competitionThreadScreen.actions.startConversation')}
                  </MenuItem>]}
              </Menu>}
          </View>
        </View>
      </View>);
  }
};

export default withTranslation()(CommentActions);
