import { MaterialCommunityIcons } from '@expo/vector-icons';
import { Text, View } from 'native-base';
import React from 'react';
import { withTranslation } from 'react-i18next';

const CommentHeader = ({ author, postTitle, t }) =>
  <View style={{ flexDirection: 'row', paddingBottom: 10 }}>
    <View style={{ flex: 1, padding: 5 }}>
      <MaterialCommunityIcons size={20} color={'#808080'} name="comment"/>
    </View>
    <View style={{ flex: 14 }}>
      <Text note style={{ color: '#808080' }}>
        {author + t('components.commentHeader.has')}
        &quot;{postTitle}&quot;
        {' ' + t('components.commentHeader.commented')}
      </Text>
    </View>
  </View>;
export default withTranslation()(CommentHeader);
