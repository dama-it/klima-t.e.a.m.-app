import 'moment/locale/de-at';
import { Button, Icon, Text, View } from 'native-base';
import React from 'react';
import { withTranslation } from 'react-i18next';
import Menu, { MenuDivider, MenuItem } from 'react-native-material-menu';

class CommentListHeader extends React.PureComponent {
  _menu = null;

  setMenuRef = ref => {
    this._menu = ref;
  };

  hideMenu = () => {
    this._menu.hide();
  };

  showMenu = () => {
    this._menu.show();
  };

  sortNewest = () => {
    this.props.handleSort('newest');
    this.hideMenu();
  }

  sortOldest = () => {
    this.props.handleSort('oldest');
    this.hideMenu();
  }

  sortBest = () => {
    this.props.handleSort('best');
    this.hideMenu();
  }

  renderMenuButton = () =>
    <Button
      iconRight
      transparent
      small
      style={{ justifyContent: 'flex-start' }}
      onPress={this.showMenu}
    >
      <Text style={{ color: '#000000', fontWeight: 'bold' }}>
        {this.props.sort === 'newest'
          ? this.props.t('screens.threadScreen.newPosts').toUpperCase()
          : this.props.sort === 'oldest'
            ? this.props.t('screens.threadScreen.oldest').toUpperCase()
            : this.props.t('screens.threadScreen.bestComments').toUpperCase()}
      </Text>
      <Icon
        style={{ color: '#000000' }}
        name="ios-arrow-down"
      />
    </Button>

  render () {
    const { t } = this.props;

    return (
      <View>
        <View
          style={{
            flexDirection: 'row',
            backgroundColor: '#EEEEEE'
          }}
        >
          <View style={{ flex: 1 }}>
            <Menu
              ref={this.setMenuRef}
              onDismiss={this._closeMenu}
              button={this.renderMenuButton} >
              <MenuItem disabled>{t('screens.threadScreen.sortAfter')}</MenuItem>
              <MenuDivider />
              <MenuItem onPress={this.sortNewest}>{t('screens.threadScreen.newPosts')}</MenuItem>
              <MenuItem onPress={this.sortOldest}>{t('screens.threadScreen.oldest')}</MenuItem>
              <MenuItem onPress={this.sortBest}>{t('screens.threadScreen.bestComments')}</MenuItem>
            </Menu>
          </View>
        </View>
      </View>);
  }
}

export default withTranslation()(CommentListHeader);
