import { Text, View } from 'native-base';
import React from 'react';
import { withTranslation } from 'react-i18next';

const styles = {
  competitionFooterTimeRemainingContainer: {
    width: 90,
    height: 90,
    borderRadius: 45,
    borderColor: '#ffffff',
    borderStyle: 'solid',
    borderWidth: 4,
    justifyContent: 'center',
    alignItems: 'center'
  }
};

export const CompetitionFooter = ({ t, timeDiff, unit }) =>
  <View style={{ backgroundColor: '#00A29B', alignItems: 'center', paddingTop: 10, paddingBottom: 10 }}>
    <View style={styles.competitionFooterTimeRemainingContainer}>
      <Text style={{ color: '#ffffff', fontSize: 40 }}>
        {timeDiff}
      </Text>
    </View>
    <View>
      <Text style={{ color: '#ffffff', fontSize: 20 }}>
        {t('timeUnits.' + unit)} {t('timeUnits.left')}
      </Text>
    </View>
  </View>;

export default withTranslation()(CompetitionFooter);
