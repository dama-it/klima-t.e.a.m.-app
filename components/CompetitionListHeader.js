import 'moment/locale/de-at';
import { ActionSheet, Button, Icon, Text, View } from 'native-base';
import React from 'react';
import { withTranslation } from 'react-i18next';

const styles = {
  iconDown: { color: '#01A19A' }
};

const cancelIndexSortPost = 2;

const CompetitionListHeader = ({ t, handleSort, handleDisplayAs, ...props }) => {
  const buttonsSortPosts = [t('screens.homeDetailsScreen.newPosts'),
    t('screens.homeDetailsScreen.hot'),
    t('screens.homeDetailsScreen.cancel')
  ];

  const showMenu = () =>
    ActionSheet.show(
      {
        options: buttonsSortPosts,
        cancelButtonIndex: cancelIndexSortPost,
        title: t('screens.homeDetailsScreen.sortPosts')
      },
      buttonIndex => handleSort(buttonIndex)
    );

  return (
    <View>
      <View
        style={{
          flexDirection: 'row',
          backgroundColor: '#eeeeee'
        }}
      >
        <View style={{ flex: 1 }}>
          <Button
            iconRight
            transparent
            small
            style={{ justifyContent: 'flex-start' }}
            onPress={showMenu}
          >
            <Text style={{ color: '#01A19A' }}>{t('screens.homeDetailsScreen.sortPosts')}</Text>
            <Icon
              style={styles.iconDown}
              name="ios-arrow-down"
            />
          </Button>
        </View>
      </View>
    </View>);
};

export default withTranslation()(CompetitionListHeader);
