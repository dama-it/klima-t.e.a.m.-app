// React imports
import moment from 'moment';
import { Body, Card, CardItem, Left, Right, Text, Thumbnail, View } from 'native-base';
import React from 'react';
// Other imports
import { GENERAL_CHALLENGES, QUANTITATIVE_CHALLENGES } from '../utils/competitionTypes';
import { getIconSrc, getIcon } from '../utils/imageUtils';
import { BadgeBar } from './BadgeBar';
import PostActions from './PostActions';
import { TouchableOpacity, TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { replaceDotWithComma } from '../utils/formatUtils';
import { withTranslation } from 'react-i18next';
import styles from './../styles/styles';
import CreatedBy from './post/CreatedBy';

class CompetitionListItem extends React.PureComponent {
  selectPost = () => this.props.selectPost(this.props.post)
  selectTopic = () => this.props.selectTopic(this.props.post.topic)
  setFavourite = () => this.props.setFavourite(this.props.post)
  iconSrc = { uri: getIconSrc(this.props.post && this.props.post.topic) }
  render () {
    const { t, post, selectPost, downVoteCall, upVoteCall, displayAsListItem } = this.props;
    const competition = post.competition;
    return (
      <Card transparent={displayAsListItem} noShadow={displayAsListItem} style={displayAsListItem ? styles.listItemStyle : null}>
        <CardItem header>
          <Left>
            <TouchableOpacity onPress={this.selectTopic}>
              <Thumbnail
                small
                source={this.iconSrc}
              />
            </TouchableOpacity>
            <Body>
              <Text
                onPress={this.selectTopic}
              >
                {post.topic ? post.topic.name : null}
              </Text>
              <CreatedBy post={post}/>
            </Body>
          </Left>
        </CardItem>
        <TouchableWithoutFeedback onPress={this.selectPost}>
          <CardItem cardBody>
            <Left style={styles.competitionLeftContainer}>
              <Text style={{ paddingBottom: 10, alignSelf: 'flex-start' }}>{post.title}</Text>
              <Text style={{ paddingTop: 0, alignSelf: 'flex-start', paddingRight: 5 }} note>
                {t('screens.competitionThreadScreen.subtitle') + ' ' +
                     competition.timeAmount + ' ' +
                      t('timeUnits.' + competition.timeUnit) + ' ' +
                     competition.goal}
                {competition.type === GENERAL_CHALLENGES ? '.' : null}
                {competition.type === QUANTITATIVE_CHALLENGES
                  ? ' von ' + replaceDotWithComma(competition.startValue) + competition.unit +
                ' auf ' + replaceDotWithComma(competition.endValue) + competition.unit
                  : null}
                {competition.type === QUANTITATIVE_CHALLENGES && (competition.startValue - competition.endValue > 0)
                  ? t('screens.competitionThreadScreen.decrease') : null}
                {competition.type === QUANTITATIVE_CHALLENGES && (competition.startValue - competition.endValue < 0)
                  ? t('screens.competitionThreadScreen.increase') : null}
              </Text>
            </Left>
          </CardItem>
        </TouchableWithoutFeedback>
        <BadgeBar post={post}/>
        <PostActions
          post={post}
          isFavourite={post.isFavourite}
          upVotesCount={post.upVotesCount}
          downVotesCount={post.downVotesCount}
          isPinned={post.isPinned}
          commentsCount={post.commentsCount}
          currentUsersVote={post.currentUsersVote}
          selectPost={selectPost}
          downVoteCall={downVoteCall}
          upVoteCall={upVoteCall}
        />
        <View style={[styles.competitionListItemCornerEffect, styles.competitionListItemCornerEffectHider]}/>
        <View style={[styles.competitionListItemCornerEffect, styles.competitionListItemCornerEffectFlap]}/>
      </Card>
    );
  }
};

export default withTranslation()(CompetitionListItem);
