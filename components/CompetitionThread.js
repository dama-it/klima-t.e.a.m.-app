import { MaterialCommunityIcons } from '@expo/vector-icons';
import moment from 'moment';
import { Body, Button, Card, CardItem, Container, Footer, H1, Header, Icon, Left, Right, Text, Thumbnail, View } from 'native-base';
import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import { ActivityIndicator, Image, Modal, RefreshControl } from 'react-native';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import Menu, { MenuItem } from 'react-native-material-menu';
import { connect } from 'react-redux';
import { reset } from 'redux-form';
import Comment from '../components/Comment';
import CommentListHeader from '../components/CommentListHeader';
import CompetitionFooter from '../components/CompetitionFooter';
import { showError, showMessage } from '../components/Messages';
import PostActions from '../components/PostActions';
import QuantitativeCompetitionFooter from '../components/QuantitativeCompetitionFooter';
import { addComments, deleteCommentAction, downVoteCommentAction, selectComment, setComments, upVoteCommentAction } from '../store/actions/comments';
import { setCompetition } from '../store/actions/competition';
import { setCurrentDiscussionId } from '../store/actions/discussions';
import { updateIdentity } from '../store/actions/identity';
import { deletePostAction, downVotePost, favouritePost, selectPost, syncPost, upVotePost } from '../store/actions/posts';
import styles from '../styles/styles';
import { GENERAL_CHALLENGES, QUANTITATIVE_CHALLENGES } from '../utils/competitionTypes';
import { replaceDotWithComma } from '../utils/formatUtils';
import { getBannerSrc, getIconSrc } from '../utils/imageUtils';
import { blockAuthorCall, createConversation, deleteComment, deleteCompetition, getComments, getCompetition, getIdentity, pinUnpinCompetition, reportCall, reportCompetitionCall, setFavouriteCompetition, viewCompetition } from '../utils/restUtils';
import { calculateTimeDiffAndUnit } from '../utils/timeUtils';
import { downVote, downVoteComment, upVote, upVoteComment } from '../utils/voteRestUtils';
import { deleteTopicPostAction, selectTopic, selectTopicScreen } from '../store/actions/topics';
import ListFooter from './ListFooter';

const inlineStyles = { iconStyle: { paddingTop: 15, paddingLeft: 10 } };
const contentInset = { top: 0, bottom: 200, left: 0, right: 0 };

class ThreadScreen extends Component {
  state = {
    refreshing: false,
    commentsPerPage: 10,
    page: 1,
    loadingNewPage: false,
    loading: false,
    lastPageLoadedAt: null,
    sort: 'newest',
    evidenceModal: false
  };

  _menu = null;

  setMenuRef = ref => {
    this._menu = ref;
  };

  hideMenu = () => {
    this._menu.hide();
  };

  showMenu = () => {
    this._menu.show();
  };

  fetchCompetition = (id) => {
    this.setState({ refreshingDetails: true });
    getCompetition(id).then(data => {
      const competition = data.data ? data.data : null;
      this.setState({ refreshingDetails: false });
      if (competition !== null) {
        this.props.dispatch(selectPost(competition));
      }
    }).catch(e => {
      this.setState({ refreshingDetails: false });
      showError(e.status, this.props.t);
    });
  }

  deleteCompetitionCall () {
    this.setState({ loading: true });
    deleteCompetition(this.props.post.id).then(data => {
      this.props.dispatch(deletePostAction(this.props.post.id));
      this.props.dispatch(deleteTopicPostAction(this.props.post.id));
      this.setState({ loading: false });
      showMessage('SUCCESS.DELETED', this.props.t);
      this.navigateBack();
    }).catch(e => {
      this.setState({ loading: false });
      showError(e.status, this.props.t);
    });
  }

  deleteCommentCall (comment) {
    deleteComment(comment.id).then(sucess => {
      this.props.dispatch(deleteCommentAction(comment));
    });
  }

  refreshCompetition = (id) => {
    getCompetition(id).then(data => {
      this.props.dispatch(syncPost(data.data));
    }).catch(e => {
      showError(e.status, this.props.t);
    });
  };

  upVoteCall = (post) => {
    this.props.dispatch(upVotePost(post));
    upVote(post.id).then(() => {
      if (post.currentUsersVote === null) {
        this.refreshIdentity();
      }
    }).catch(e => {
      this.refreshCompetition(post.id);
      showError(e.status, this.props.t);
    });
  }

  downVoteCall = (post) => {
    this.props.dispatch(downVotePost(post));
    downVote(post.id).then(() => {
      if (post.currentUsersVote === null) {
        this.refreshIdentity();
      }
    }).catch(e => {
      this.refreshCompetition(post.id);
      showError(e.status, this.props.t);
    });
  }

  pinUnPinCompetition = (id) => {
    pinUnpinCompetition(id).then(() => {
      this.refreshCompetition(id);
    }).catch(e => {
      showError(e.status, this.props.t);
    });
  }

  blockAuthor = id => {
    blockAuthorCall(id).then(() => {
      this.setState({ page: 0 });
      this.fetchComments(this.props.post.id, true, true);
    }).catch(e => { showError(e.status, this.props.t); });
  }

  handlePressMenuButton = (buttonIndex, comment, editable) => {
    if (buttonIndex === 1 && editable) {
      this.props.dispatch(selectComment(comment));
      this.props.navigation.navigate('WriteComment');
    }
    if (buttonIndex === 2 && editable) {
      this.deleteCommentCall(comment);
    }
    if (buttonIndex === 3 && !editable) {
      this.handleReport(comment);
    }
    if (buttonIndex === 4 && !editable) {
      this.blockAuthor(comment);
    }
    if (buttonIndex === 5 && !editable) {
      this.startConverstaion(comment.author);
    }
  }

  handlePressCompetitionMenuButton = (buttonIndex, competition, isAuthor) => {
    switch (buttonIndex) {
      case 1:
        this.handlePressEditCompetition(competition);
        break;
      case 2:
        this.props.navigation.navigate('CompleteCompetitionScreen');
        break;
      case 3:
        this.props.navigation.navigate('CancelCompetitionScreen');
        break;
      case 4:
        this.deleteCompetitionCall(competition);
        break;
      case 5:
        this.handleReportCompetition(competition);
        break;
      case 6:
        this.props.navigation.navigate('UpdateCompetitionScreen');
        break;
      case 7:
        this.startConverstaion(competition.author);
        break;
      default:
        break;
    }
  }

  handlePressEditCompetition = post => {
    this.props.dispatch(setCompetition(post));
    if (post.competition.type === GENERAL_CHALLENGES) {
      this.props.navigation.navigate('WizzardStack', { screen: 'AddGoalScreen' });
    } else {
      this.props.navigation.navigate('WizzardStack', { screen: 'QuantitativeCompetitionOverviewScreen' });
    }
  }

  refreshIdentity = () => {
    getIdentity().then(data => {
      this.props.dispatch(updateIdentity(data.data.identity));
    });
  }

  upVoteCommentCall = comment => {
    this.props.dispatch(upVoteCommentAction(comment));
    upVoteComment(comment.id).then(data => {
    }).catch(e => { this.props.dispatch(downVoteCommentAction(comment)); showError(e.status, this.props.t); });
  }

  downVoteCommentCall = (comment) => {
    this.props.dispatch(downVoteCommentAction(comment));
    downVoteComment(comment.id).then(() => {
    }).catch(e => {
      this.props.dispatch(upVoteCommentAction(comment));
      showError(e.status, this.props.t);
    });
  }

  handleReply = comment => {
    this.props.dispatch(selectComment(comment));
    this.props.dispatch(reset('replyCommentForm'));
    this.props.navigation.navigate('ReplyComment');
  }

  renderItem = ({ item }) => <Comment
    currentUser={this.props.username}
    comment={item}
    handleUpVote={this.upVoteCommentCall}
    handleDownVote={this.downVoteCommentCall}
    handleReply={this.handleReply}
    handlePressMenuButton={this.handlePressMenuButton}
    level={item.level}
    {...item}
  />

  fetchComments = (id, reset, silent) => {
    this.setState({ loading: !silent && reset, loadingNewPage: true });
    getComments(id, this.state.page, this.state.commentsPerPage, this.state.sort).then(success => {
      this.props.dispatch(reset ? setComments(success.data) : addComments(success.data));
      this.setState({ loading: false, refreshing: false, loadingNewPage: false, lastPageLoadedAt: (new Date()).getTime() });
    }).catch(e => {
      this.setState({ loading: false, refreshing: false, loadingNewPage: false, lastPageLoadedAt: (new Date()).getTime() });
      showError(e.status, this.props.t);
    });
  }

  onEndReached = (info) => {
    const now = (new Date()).getTime();
    const lastPageLoadedDiff = now - (this.state.lastPageLoadedAt === null ? 0 : this.state.lastPageLoadedAt);
    if (lastPageLoadedDiff > 500 && this.state.loadingNewPage === false) {
      this.setState({ page: this.state.page + 1 }, () => this.fetchComments(this.props.post.id, false, false));
    }
  };

  handleSort = (sort) => {
    this.setState({ sort: sort, page: 0 }, () => this.fetchComments(this.props.post.id, true, false));
  }

  handleReport = (comment) => {
    reportCall(comment.id).then(success => showMessage('SUCCESS.USER_REPORTED', this.props.t), e => showError('ERROR.ERROR', this.props.t));
  }

  handleReportCompetition = (post) => {
    reportCompetitionCall(post.id).then(success => showMessage('SUCCESS.USER_REPORTED', this.props.t), e => showError('ERROR.ERROR', this.props.t));
  }

  onRefreshing = () => {
    this.setState({ page: 0, refreshing: true }, () => {
      this.fetchComments(this.props.post.id, true, false);
      this.refreshCompetition(this.props.post.id);
    });
  };

  doCompetitionView = (competition) => {
    viewCompetition(competition).then(success => { }, e => { showError(e.status, this.props.t); });
  }

  async componentDidMount () {
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      this.setState({ page: 1 }, () => this.fetchComments(this.props.post.id, true, false));
      this.doCompetitionView(this.props.post.competition);
      this.props.dispatch(syncPost({ ...this.props.post, status: 0 }));
    });
  }

  componentWillUnmount () {
  }

  startConverstaion = (user) => {
    createConversation(user).then((success) => {
      this.props.navigation.navigate('Chat', { screen: 'ChatScreen', params: { discussion: success } });
      this.props.dispatch(setCurrentDiscussionId(success.id));
    });
  }

  navigateBack = () => this.props.navigateBack(this.props.post.topic)

  renderMenuButton = () => this.state.loading
    ? <ActivityIndicator/>
    : (
      <TouchableOpacity style={{ padding: 10 }} onPress={this.showMenu} >
        <Icon name="md-more" />
      </TouchableOpacity>
    )

  keyExtractor = (item, index) => index.toString()

  handleNewComment = () => {
    this.props.dispatch(reset('newCommentForm'));
    this.props.navigation.navigate('WriteComment', { topic: this.props.route.params?.topic ?? null });
  }

  selectTopic = () => {
    this.props.dispatch(selectTopic(this.props.post.topic));
    this.props.dispatch(selectTopicScreen(true));
    this.props.navigation.navigate('Favourites', { screen: 'TopicDetailScreen' });
  };

  renderHeader = () => {
    const { post, t } = this.props;
    const competition = post.competition;
    const timeDiffObject = calculateTimeDiffAndUnit(moment(competition.targetDate).valueOf(), moment.now());

    return (
      <View style={{ padding: 0 }} transparent >
        <TouchableOpacity onPress={this.selectTopic}>
          <CardItem header>
            <Left>
              <Thumbnail
                small
                source={{
                  uri: getIconSrc(post && post.topic)
                }}
              />
              <Body>
                <Text>{post.topic ? post.topic.name : '-'}</Text>
                <Text note>{t('screens.threadScreen.postedBy')} {post.author.username}
                  {' | '}
                  {moment(post.createdAt).fromNow()}
                </Text>
              </Body>
            </Left>
          </CardItem>
        </TouchableOpacity>
        {competition.cancelledAt && competition.cancelReason
          ? <CardItem style={{ backgroundColor: '#A12121', flexDirection: 'column', borderRadius: 0 }}>
            <Text note style={{ color: '#ffffff', paddingTop: 10 }}>
              {
                t('screens.competitionThreadScreen.cancelledAt') +
                      moment(competition.cancelledAt).format('L') +
                      t('screens.competitionThreadScreen.cancelled')
              }
            </Text>
            <Text style={{ color: '#ffffff', paddingTop: 15, paddingBottom: 15, lineHeight: 30, fontStyle: 'italic' }} >
              {'"' + competition.cancelReason + '"'}
            </Text>
          </CardItem>
          : null}
        {competition.cancelledAt && !competition.cancelReason
          ? <CardItem style={{ backgroundColor: '#A12121', flexDirection: 'column', borderRadius: 0 }}>
            <Text note style={{ color: '#ffffff', paddingTop: 10 }}>
              {
                t('screens.competitionThreadScreen.cancelledByTimeLimit')
              }
            </Text></CardItem> : null}
        {competition.finishDate != null
          ? <CardItem style={{ backgroundColor: '#309B67', flexDirection: 'column', borderRadius: 0 }}>
            <Text note style={{ color: '#ffffff', paddingTop: 10 }}>
              {
                t('screens.competitionThreadScreen.completedAt') +
                      moment(competition.finishDate).format('L') + ' ' +
                      t('screens.competitionThreadScreen.completed')
              }
            </Text>
            <TouchableOpacity onPress={() => this.setState({ evidenceModal: true })} style={{ flexDirection: 'row' }}>
              <Text style={{ color: '#ffffff', paddingTop: 15, paddingBottom: 15, lineHeight: 30 }} >
                {t('screens.competitionThreadScreen.evidenceText')}
              </Text>
              <MaterialCommunityIcons style={inlineStyles.iconStyle} color={'white'} size={32} name="information">
              </MaterialCommunityIcons>
            </TouchableOpacity>
            <Modal
              transparent
              animationType={'fade'}
              visible={this.state.evidenceModal}
              onRequestClose={() => {
              }}>
              <View style={{
                width: '100%',
                height: '100%',
                backgroundColor: 'rgba(0,0,0,0.7)',
                flexDirection: 'column',
                alignItems: 'center',
                paddingTop: '50%'
              }}>
                <Card style={{ width: '80%', backgroundColor: 'white', alignSelf: 'center' }}>
                  <Text note style={{ paddingTop: 20, paddingBottom: 20, alignSelf: 'center' }} >
                    {t('screens.competitionThreadScreen.evidenceOf') +
                            (post.author
                              ? post.author.username
                              : null) +
                            ' ' + t('screens.competitionThreadScreen.evidenceAt') +
                            moment(competition.finishDate).format('L')}
                  </Text>
                  {competition.proveMedia && competition.proveMedia.path
                    ? <CardItem cardBody style={{ padding: 0 }}>
                      <Image
                        source={{
                          uri: getBannerSrc({ banner: competition.proveMedia })
                        }}
                        style={{ height: 200, width: null, flex: 1, resizeMode: 'cover' }}
                      />
                    </CardItem> : null}
                  {competition.proveText
                    ? <CardItem >
                      <Text style={{ alignSelf: 'center' }}>{competition.proveText}</Text>
                    </CardItem> : null}
                  <CardItem style={{ flexDirection: 'row-reverse', paddingTop: 20, paddingBottom: 20 }}>
                    <Button style={{ backgroundColor: 'transparent', elevation: 0 }} onPress={() => this.setState({ evidenceModal: false })}>
                      <Text note>{t('screens.competitionThreadScreen.modalClose')}</Text>
                    </Button>
                  </CardItem>
                </Card>
              </View>
            </Modal>
          </CardItem>
          : null}
        <CardItem cardBody style={{ padding: 0 }}>
          {
            post.banner && <Image
              source={{ uri: getBannerSrc(post) }}
              style={{ height: 200, width: null, flex: 1, resizeMode: 'cover' }}
            />
          }
        </CardItem>
        <CardItem>
          <Body>
            <H1 style={{ fontFamily: 'Montserrat' }}>{post.title}</H1>
            <Text style={{ color: '#808080', fontSize: 18, paddingBottom: 10, paddingTop: 10, fontFamily: 'Montserrat' }}>
              {t('screens.competitionThreadScreen.subtitle') + ' ' +
                      competition.timeAmount + ' ' +
                      t('timeUnits.' + competition.timeUnit) + ': '} {'\n'}{
                competition.goal}
              {competition.type === QUANTITATIVE_CHALLENGES
                ? ' von ' + replaceDotWithComma(competition.startValue) +
                      competition.unit + ' auf ' + replaceDotWithComma(competition.endValue) + competition.unit
                : null}
              {competition.type === QUANTITATIVE_CHALLENGES && (competition.startValue - competition.endValue > 0)
                ? t('screens.competitionThreadScreen.decrease') : null}
              {competition.type === QUANTITATIVE_CHALLENGES && (competition.startValue - competition.endValue < 0)
                ? t('screens.competitionThreadScreen.increase') : null}
            </Text>
            <Text style={{ paddingTop: 20, fontFamily: 'Montserrat' }}>
              {post.content}
            </Text>
          </Body>
        </CardItem>
        {competition !== null && competition.type === GENERAL_CHALLENGES
          ? <CompetitionFooter
            timeDiff={timeDiffObject.timeDiff}
            unit={timeDiffObject.unit}
          /> : null}
        <PostActions
          post={post}
          downVoteCall={this.downVoteCall}
          upVoteCall={this.upVoteCall}
          {...post}
        />
        {competition !== null && competition.type === QUANTITATIVE_CHALLENGES
          ? <QuantitativeCompetitionFooter
            competitionActionList={post.competitionActionList}
            timeDiffObject={competition.targetDate ? timeDiffObject : null}
            unit={competition.unit}
            startValue={competition.startValue}
            endValue={competition.endValue}
          /> : null}
        <CommentListHeader sort={this.state.sort} handleSort={this.handleSort} />
      </View>
    );
  }

  render () {
    const { navigation, post, moderatorTopics, userId, route, t } = this.props;
    const competition = post.competition;
    if (post === null || competition === null) return <Container></Container>;
    const isAuthor = post.author.id === userId;
    const userHasDeleteRole = moderatorTopics !== [] && moderatorTopics !== null ? !!moderatorTopics.find(t => t.topic.id === post.topic.id) : false;

    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={this.navigateBack}>
              <Icon name="ios-arrow-round-back" />
            </Button>
          </Left>

          <Right>
            <Menu
              style={{ marginTop: 30 }}
              ref={this.setMenuRef}
              onDismiss={this._closeMenu}
              button={this.renderMenuButton()}
            >
              {(isAuthor || userHasDeleteRole) && competition.finishDate === null && competition.cancelledAt === null &&
                 <MenuItem key={1} onPress={() => { this.handlePressCompetitionMenuButton(1, this.props.post, isAuthor); this.hideMenu(); }} >
                   <Text style={{ color: '#808080' }}>{t('screens.competitionThreadScreen.actions.edit')}</Text>
                 </MenuItem>
              }
              {
                (isAuthor || userHasDeleteRole) && competition.finishDate === null && competition.cancelledAt === null && new Date(competition.targetDate) < new Date() &&
                <MenuItem key={2} onPress={() => { this.handlePressCompetitionMenuButton(2, this.props.post, isAuthor); this.hideMenu(); }}>
                  <Text style={{ color: '#808080' }}>{t('screens.competitionThreadScreen.actions.complete')}</Text>
                </MenuItem>
              }
              {isAuthor && competition.type === QUANTITATIVE_CHALLENGES && competition.finishDate === null && competition.cancelledAt === null
                ? <MenuItem key={6} onPress={() => { this.handlePressCompetitionMenuButton(6, this.props.post, isAuthor); this.hideMenu(); }} >
                  <Text style={{ color: '#808080' }}>{t('screens.competitionThreadScreen.actions.update')}</Text>
                </MenuItem> : null}
              {isAuthor && competition.cancelledAt === null && competition.finishDate === null
                ? <MenuItem key={3} onPress={() => { this.handlePressCompetitionMenuButton(3, this.props.post, isAuthor); this.hideMenu(); }} >
                  <Text style={{ color: '#A12121' }}>{t('screens.competitionThreadScreen.actions.cancel')}</Text>
                </MenuItem> : null}
              {(isAuthor || userHasDeleteRole) && post.active
                ? <MenuItem key={4} onPress={() => {
                  this.handlePressCompetitionMenuButton(4, this.props.post, isAuthor || userHasDeleteRole);
                  this.hideMenu();
                }} >
                  <Text style={{ color: '#A12121' }}>{t('screens.competitionThreadScreen.actions.delete')}</Text>
                </MenuItem> : null}

              {!isAuthor ? <MenuItem key={5} onPress={() => { this.handlePressCompetitionMenuButton(5, this.props.post, isAuthor); this.hideMenu(); }} >
                <Text style={{ color: '#808080' }}>{t('screens.competitionThreadScreen.actions.report')}</Text>
              </MenuItem> : null}

              {!isAuthor ? <MenuItem key={7} onPress={() => { this.handlePressCompetitionMenuButton(7, this.props.post, isAuthor); this.hideMenu(); }} >
                <Text style={{ color: '#808080' }}>{t('screens.competitionThreadScreen.actions.startConversation')}</Text>
              </MenuItem> : null}
            </Menu>
          </Right>
        </Header>
        <View style={styles.divider}></View>
        <FlatList
          style={{ padding: 0 }}
          ListHeaderComponent={this.renderHeader}
          ref={(ref) => { this.flatListRef = ref; }}
          refreshControl={<RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={() => this.onRefreshing()}
          />}
          renderItem={this.renderItem}
          data={this.props.comments && !this.state.loading ? this.props.comments : null}
          onEndReached={this.onEndReached}
          keyExtractor={this.keyExtractor}
          onEndReachedThreshold={0.2}
          initialNumToRender={7}
          contentInset={contentInset}
          contentInsetAdjustmentBehavior="automatic"
          ListFooterComponent={<ListFooter loading={this.state.loadingNewPage}/>}
        />
        <Footer style={styles.threadFooter}>
          <Button
            iconRight
            transparent
            style={{ alignSelf: 'center', borderColor: 'grey', borderStyle: 'solid', borderWidth: 1, borderRadius: 5, height: 35 }}
            onPress={this.handleNewComment}
          >
            <Text style={{ color: '#ffffff', fontWeight: 'bold' }}>
              {t('screens.threadScreen.writeComment')}
            </Text>
          </Button>
        </Footer>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    moderatorTopics: state.identity && state.identity.moderatorTopics ? state.identity.moderatorTopics : null,
    userId: state.identity && state.identity.id ? state.identity.id : null,
    username: state.identity && state.identity.username ? state.identity.username : null,
    comments: state.comments.list,
    comment: state.comments.currentComment,
    post: state.posts.currentPost
  };
};

export default connect(mapStateToProps)(withTranslation()(ThreadScreen));
