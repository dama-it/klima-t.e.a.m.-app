// React imports
import Constants from 'expo-constants';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
// Native Base imports
import {
  Button,
  Icon,
  View
} from 'native-base';
import React, { Component } from 'react';
import { ActivityIndicator, Image } from 'react-native';
import { RNS3 } from 'react-native-upload-aws-s3';
import { getConfig } from '../utils/configUtils';
// Other imports
// Style imports
import styles from '../styles/styles';
import { getImageByPath } from '../utils/imageUtils';
import { withTranslation } from 'react-i18next';

class FileUploader extends Component {
  state = {
    image: null
  };

  getCameraRollPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        alert(this.props.t('screens.profileScreen.noCameraRollPermission'));
      }
    }
  };

  getCameraPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA);
      if (status !== 'granted') {
        alert(this.props.t('screens.profileScreen.noCameraPermission'));
      }
    }
  };

  setImage =(image) => {
    this.setState({ image: image });
  }

  render () {
    return (
      <>
        {this.state.image || this.props.initDocumentPath
          ? <View style={styles.imgUpload}>
            {
              this.state.image &&
                <Image
                  source={{ uri: this.state.image.uri }}
                  style={styles.uploadThumb}
                />
            }
            {
              !this.state.image && this.props.initDocumentPath &&
                <Image
                  source={{ uri: getImageByPath(this.props.initDocumentPath) }}
                  style={styles.uploadThumb}
                />
            }
            {this.state.uploading
              ? <View style={styles.imageUploading}>
                <ActivityIndicator color="#ffffff" />
              </View>
              : null}
          </View>
          : null}
        <View style={styles.imgUploadButtonsView}>
          <View>
            <Button transparent onPress={this._pickImage} style={styles.imgUploadButton}>
              <Icon name="ios-images" style={styles.imgUploadButtons}/>
            </Button>
          </View>
          <View>
            <Button transparent onPress={this._pickImageFromCamera} style={styles.imgUploadButton}>
              <Icon name="ios-camera" style={styles.imgUploadButtons}/>
            </Button>
          </View>
        </View>
      </>
    );
  }

  async uploadToS3 (file) {
    const options = {
      keyPrefix: '',
      bucket: getConfig('IMAGE_BUCKET_NAME'),
      region: getConfig('AWS_REGION'),
      accessKey: getConfig('AWS_ACCESS_KEY'),
      secretKey: getConfig('AWS_SECRET_KEY'),
      successActionStatus: 201
    };
    try {
      const response = await RNS3.put(file, options);
      if (response.status === 201) {
        this.props.setUploadedImagePath(response.body.postResponse.key);
        this.setState({ uploading: false });
      } else {
        this.setState({ uploading: false });
        this.setImage(null);
      }
    } catch (error) {
      this.setState({ uploading: false });
      this.setImage(null);
    }
  }

  uploadFile (result) {
    this.setState({ uploading: true });
    const localUri = result.uri;
    const filename = localUri.split('/').pop();
    const match = /\.(\w+)$/.exec(filename);
    const type = match ? `image/${match[1]}` : 'image';
    const formData = new FormData();
    formData.append('file', { uri: localUri, name: filename, type });
    formData.append('name', filename);
    const file = { uri: localUri, name: filename, type: match ? type : 'image/' + type };
    this.uploadToS3(file);
  }

  _pickImageFromCamera = async () => {
    const permission = await this.getCameraPermissionAsync();
    const aspect = this.props.aspect ? this.props.aspect : [4, 3];
    const result = await ImagePicker.launchCameraAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: aspect
    });

    if (!result.cancelled) {
      this.setImage(result, () => {
      });
      this.uploadFile(result);
    }
  };

  _pickImage = async () => {
    const permission = this.getCameraRollPermissionAsync();
    const aspect = this.props.aspect ? this.props.aspect : [4, 3];
    const result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: aspect
    });

    if (!result.cancelled) {
      this.setImage(result);
      this.uploadFile(result);
    }
  };
}

export default withTranslation()(FileUploader);
