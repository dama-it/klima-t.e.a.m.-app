import { Text, View } from 'native-base';
import React from 'react';
import { withTranslation } from 'react-i18next';

const styles = {
  valueContainer: {
    width: 90,
    height: 90,
    borderRadius: 45,
    borderColor: '#ffffff',
    borderStyle: 'solid',
    borderWidth: 4,
    justifyContent: 'center',
    alignItems: 'center'
  }
};

export const GoalReachedInfo = ({ t, timeDiffObject, goalObject }) =>
  <View style={{ backgroundColor: '#00A29B', alignItems: 'center', paddingTop: 10, paddingBottom: 10 }}>
    <Text style={{ color: '#ffffff', fontSize: 20, paddingBottom: 10 }}>{t('screens.CompetitionCompleteScreen.goalReached')}</Text>
    <View style={styles.valueContainer}>
      <Text style={{ color: '#ffffff', fontSize: 40 }}>{goalObject.value}</Text>
    </View>
    <View>
      <Text style={{ color: '#ffffff', fontSize: 20, paddingTop: 10 }}>
        {goalObject.unit} in {timeDiffObject.value + ' ' + t('timeUnits.' + timeDiffObject.unit)}
      </Text>
    </View>
  </View>;

export default withTranslation()(GoalReachedInfo);
