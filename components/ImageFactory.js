import Constants from 'expo-constants';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import React, { Component } from 'react';
import styles from '../styles/styles';
import { RNS3 } from 'react-native-upload-aws-s3';
import {
  ActionSheetIOS,
  View,
  Animated,
  Platform
} from 'react-native';
import { withTranslation } from 'react-i18next';
import { Feather, MaterialIcons } from '@expo/vector-icons';
import { getConfig } from '../utils/configUtils';
import BottomSheet from 'react-native-js-bottom-sheet';

class ImageFactory extends Component {
  constructor (props) {
    super(props);
    this.bottomSheet = new BottomSheet();
    this.state = {
      image: undefined,
      height: new Animated.Value(0),
      overflow: 'visible'
    };
  }

  getCameraRollPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        alert(this.props.t('screens.profileScreen.noCameraRollPermission'));
      }
    }
  };

  getCameraPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA);
      if (status !== 'granted') {
        alert(this.props.t('screens.profileScreen.noCameraPermission'));
      }
    }
  };

  async uploadToS3 (file) {
    const options = {
      keyPrefix: '',
      bucket: getConfig('IMAGE_BUCKET_NAME'),
      region: getConfig('AWS_REGION'),
      accessKey: getConfig('AWS_ACCESS_KEY'),
      secretKey: getConfig('AWS_SECRET_KEY'),
      successActionStatus: 201
    };
    try {
      const response = await RNS3.put(file, options);
      if (response.status === 201) {
        this.props.setUploadedImagePath(response.body.postResponse.key);
        this.setState({ uploading: false });
      } else {
        this.setState({ uploading: false, image: undefined });
      }
    } catch (error) {
      this.setState({ uploading: false, image: undefined });
    }
  }

  uploadFile (result) {
    this.setState({ uploading: true });
    const localUri = result.uri;
    const filename = localUri.split('/').pop();
    const match = /\.(\w+)$/.exec(filename);
    const type = match ? `image/${match[1]}` : 'image';
    const formData = new FormData();
    formData.append('file', { uri: localUri, name: filename, type });
    formData.append('name', filename);
    const file = { uri: localUri, name: filename, type: match ? type : 'image/' + type };
    this.uploadToS3(file);
  }

  _pickImageFromCamera = async () => {
    this.getCameraPermissionAsync().then(async () => {
      const aspect = this.props.aspect ? this.props.aspect : [4, 3];
      const result = await ImagePicker.launchCameraAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: aspect
      });
      if (!result.cancelled) {
        this._getImageFromStorage(result.uri);
        this.uploadFile(result);
      }
    });
  };

  _pickImage = async () => {
    this.getCameraRollPermissionAsync().then(async () => {
      const aspect = this.props.aspect ? this.props.aspect : [4, 3];
      const result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: aspect
      });

      if (!result.cancelled) {
        this._getImageFromStorage(result.uri);
        this.uploadFile(result);
      }
    });
  };

  _onPressImage = () => {
    const options = [
      this.props.t('screens.newPost.camera'),
      this.props.t('screens.newPost.gallery'),
      this.props.t('screens.topic.actions.cancel')
    ];
    ActionSheetIOS.showActionSheetWithOptions(
      {
        options,
        cancelButtonIndex: 2
      },
      (buttonIndex) => {
        if (buttonIndex === 0) {
          this._pickImageFromCamera();
        } else if (buttonIndex === 1) {
          this._pickImage();
        }
      }
    );
  };

  shouldComponentUpdate (nextProps, nextState) {
    return true;
  }

  _startAnimation = () => {
    Animated.sequence([
      Animated.timing(this.state.height, {
        toValue: 0,
        duration: 250,
        useNativeDriver: false
      }),
      Animated.timing(this.state.height, {
        toValue: 150,
        duration: 500,
        delay: 75,
        useNativeDriver: false
      })
    ]).start();
  };

  _renderOptions = () => {
    const options = [
      this.props.t('screens.newPost.camera'),
      this.props.t('screens.newPost.gallery'),
      this.props.t('screens.topic.actions.cancel')
    ];

    return [
      {
        title: options[0],
        onPress: () =>
          this._pickImageFromCamera() && this.bottomSheet.close(),
        icon: (
          <MaterialIcons name="photo-camera" size={24} style={styles.icon} />
        )
      },
      {
        title: options[1],
        onPress: () =>
          this._pickImage() && this.bottomSheet.close(),
        icon: (
          <MaterialIcons name="photo-library" size={24} color="black" />
        )
      }
    ];
  };

  _getImageFromStorage = (path) => {
    this.setState({ image: path, overflow: 'hidden' }, () =>
      this._startAnimation()
    );
  };

  render () {
    const locals = { error: '', hasError: '', config: { title: 'select img' }, help: '' };
    return (
      <View style={styles.outsideContainer}>
        <View
          style={[
            styles.topContainer,
            locals.hasError ? styles.borderColorOnError : {}
          ]}>
          <Animated.Image
            resizeMode="cover"
            useNativeDriver="false"
            source={{
              uri: this.state.image
            }}
            style={[
              styles.image,
              {
                height: this.state.height
              }
            ]}
            onStartShouldSetResponder={
              Platform.OS === 'ios'
                ? this._onPressImage
                : () => this.bottomSheet.open()
            }
          />
          <View
            style={[
              { overflow: this.state.overflow },
              styles.container,
              locals.hasError ? styles.backgroundColorOnError : {}
            ]}
            onStartShouldSetResponder={
              Platform.OS === 'ios'
                ? this._onPressImage
                : () => this.bottomSheet.open()
            }
          >
            <Feather name="camera" size={32} color="black" />
          </View>
        </View>
        {Platform.OS === 'android' ? (
          <BottomSheet
            ref={(ref) => {
              this.bottomSheet = ref;
            }}
            title={locals.config.title}
            options={this._renderOptions()}
            coverScreen={true}
          />
        ) : null}
      </View>
    );
  };
}

export default withTranslation()(ImageFactory);
