import React from 'react';
import { View } from 'native-base';
import { ActivityIndicator } from 'react-native';
import styles from '../styles/styles';

const ListFooter = ({ loading }) => (
  <>
    {loading && <ActivityIndicator size={0} color="#fff" />}
    <View style={styles.listFooter}></View>
  </>
);
export default ListFooter;
