import moment from 'moment';
import { Body, Card, CardItem, ListItem, Right, Text, Thumbnail, View } from 'native-base';
import React from 'react';
import { withTranslation } from 'react-i18next';
import { Modal, TouchableOpacity } from 'react-native';
import { getIconSrc } from '../utils/imageUtils';
import { calculateTimeSinceCreationObject } from '../utils/timeUtils';

const styles = {
  msgViewStyle: {
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0,0,0,0.7)',
    flexDirection: 'column',
    alignItems: 'center',
    paddingTop: '50%'
  },
  msgCardStyle: { width: '80%', backgroundColor: 'white', alignSelf: 'center' }
};

class MessageListItem extends React.PureComponent {
  state = {
    modal: false
  }

  readMessage = () => {
    if (!this.props.seen) {
      this.props.readMessage();
    }
    this.setState({ modal: true });
  }

  iconSrc = {
    uri: getIconSrc(this.props.post ? this.props.post.topic : null)
  }

  render () {
    const { t, title, body, createdAt, seen } = this.props;
    const timeSinceCreationObject = calculateTimeSinceCreationObject(createdAt, moment);
    return (
      <ListItem
        selected={!seen}
        onPress={this.readMessage}
        noIndent>
        <View style={{ width: 50 }}>
          <Thumbnail
            small
            source={this.iconSrc}
          >
          </Thumbnail>
        </View>
        <Body>
          <Text>{title}</Text>
          <Text note>{body}</Text>
        </Body>
        <Right>
          <Text note>
            {timeSinceCreationObject.timeAmount +
           t('timeUnits.short.' + timeSinceCreationObject.unit)}
          </Text>
        </Right>
        <Modal
          onRequestClose={() => {}}
          transparent
          animationType={'fade'}
          visible={this.state.modal}>
          <View style={styles.msgViewStyle}>
            <Card style={styles.msgCardStyle}>
              <CardItem>
                <Text>{title}</Text>
              </CardItem>
              <CardItem>
                <Text note>{body}</Text>
              </CardItem>
              <CardItem style={{ flexDirection: 'row-reverse', paddingTop: 20, paddingBottom: 20 }}>
                <TouchableOpacity onPress={() => this.setState({ modal: false })}>
                  <Text note>{t('screens.competitionThreadScreen.modalClose')}</Text>
                </TouchableOpacity>
              </CardItem>
            </Card>
          </View>
        </Modal>
      </ListItem>);
  };
};

export default withTranslation()(MessageListItem);
