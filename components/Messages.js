import { Toast } from 'native-base';

export const messageTypes = {
  DANGER: 'danger',
  SUCCESS: 'success',
  WARNING: 'warning'
};

export const messagePositions = {
  TOP: 'top',
  BOTTOM: 'bottom'
};

const defaultDuration = 2500;

export const message = (msg, type, duration, position) => {
  Toast.show({
    text: msg,
    type: type,
    duration: duration,
    position: position
  });
};

export const showError = (msg, t) => {
  message(t(msg), messageTypes.DANGER, defaultDuration, messagePositions.BOTTOM);
};

export const showWarning = (msg, t) => {
  message(t(msg), messageTypes.WARNING, defaultDuration, messagePositions.BOTTOM);
};

export const showMessage = (msg, t) => {
  message(t(msg), messageTypes.SUCCESS, defaultDuration, messagePositions.BOTTOM);
};

export const showMessageTop = (msg, t) => {
  message(t(msg), messageTypes.SUCCESS, defaultDuration, messagePositions.TOP);
};
