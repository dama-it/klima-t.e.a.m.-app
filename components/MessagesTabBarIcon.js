
import React from 'react';
import { connect } from 'react-redux';
import { View, Text } from 'native-base';
import { Ionicons } from '@expo/vector-icons';

const MessagesTabBarIcon = ({ newMessages, focused, nameFocused, tintColor, name }) => (
  <View>
    <Ionicons
      name={focused ? nameFocused : name}
      color={tintColor}
      size={24}
    />
    {
      newMessages
        ? <View style={{ position: 'absolute', right: -6, top: 0, backgroundColor: 'red', borderRadius: 9, width: 12, height: 12, justifyContent: 'center', alignItems: 'center' }}>
          <Text style={{ color: 'white' }}></Text>
        </View> : null
    }
  </View>
);
const mapStateToProps = (state) => {
  return {
    newMessages: state.messages.newMessages
  };
};
export default connect(mapStateToProps)(MessagesTabBarIcon);
