import { H3, View } from 'native-base';
import React from 'react';
import { withTranslation } from 'react-i18next';
import { Image } from 'react-native';
import styles from '../styles/styles';

const NoContent = ({ t }) =>
  <View style={styles.noContent}>
    <Image source={require('../../assets/no-posts.png')} style={styles.noContentImage} />
    <H3>{t('screens.topic.noContent')}</H3>
  </View>;

export default withTranslation()(NoContent);
