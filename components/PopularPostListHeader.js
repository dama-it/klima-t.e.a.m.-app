import 'moment/locale/de-at';
import { ActionSheet, Button, Icon, Text, View } from 'native-base';
import React from 'react';
import { withTranslation } from 'react-i18next';

const styles = {
  iconDown: { color: '#01A19A' }
};

const PopularPostListHeader = ({ t, handleSort, handleDisplayAs, ...props }) => {
  const buttonsPostView = [
    t('screens.homeDetailsScreen.cards'),
    t('screens.homeDetailsScreen.list'),
    t('screens.homeDetailsScreen.cancel')
  ];

  const cancelIndexPostView = 3;
  const showActionSheet = () =>
    ActionSheet.show(
      {
        options: buttonsPostView,
        cancelButtonIndex: cancelIndexPostView,
        title: t('screens.homeDetailsScreen.displayPostsAs')
      },
      buttonIndex => handleDisplayAs(buttonIndex)
    );

  return (

    <View>
      <View style={{ flex: 1, backgroundColor: '#eeeeee' }}>
        <Button
          iconRight
          transparent
          small
          onPress={showActionSheet}
        >
          <Text style={{ color: '#01A19A' }}>{t('screens.homeDetailsScreen.displayPostsAs')}</Text>
          <Icon
            style={styles.iconDown}
            name="ios-arrow-down"
          />
        </Button>
      </View>
    </View>);
};

export default withTranslation()(PopularPostListHeader);
