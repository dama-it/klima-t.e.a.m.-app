// React imports
import { CardItem, Icon, Left, Right, Text, View } from 'native-base';
import React from 'react';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { connect } from 'react-redux';
import { addFavPopularPost } from '../store/actions/popularPosts';
import { favouritePost } from '../store/actions/posts';
import { favouriteTopicPost } from '../store/actions/topics';
// Style imports
import styles from '../styles/styles';
import { setFavouritePost } from '../utils/restUtils';
import { showError } from './Messages';

const inlineStyles = {
  rowWrapper: {
    flexDirection: 'row'
  },
  container: {
    flex: 1
  },
  voteContainer: {
    flexDirection: 'row',
    paddingRight: 10
  },
  voteButtonContainer: {
    flexDirection: 'row',
    width: 25,
    justifyContent: 'center'
  },
  voteCounter: {
    color: '#808080',
    textAlign: 'center',
    paddingTop: 2
  },
  voteCountContainer: {
    width: 25
  },
  favActive: {
    ...styles.threadTools,
    ...styles.active
  },
  favInactive: {
    ...styles.threadTools
  },
  rightContainer: {
    flexDirection: 'row',
    alignItems: 'flex-end'
  },
  commentsCount: {
    ...styles.threadTools,
    marginLeft: 5,
    justifyContent: 'center',
    paddingBottom: 4
  },
  shareButton: {
    ...styles.threadTools,
    marginLeft: 10
  },
  pinContainer: {
    flex: 1,
    alignItems: 'flex-end'
  }

};

class PostActions extends React.PureComponent {
  state = {
    doingAction: false
  }

  handleUpVote = () => {
    if (this.props.currentUsersVote === false || this.props.currentUsersVote === null) {
      this.props.upVoteCall(this.props.post);
    }
  }

  handleDownVote = () => {
    if (this.props.currentUsersVote === true || this.props.currentUsersVote === null) {
      this.props.downVoteCall(this.props.post);
    };
  }

  setAsFavouritePost = () => {
    const { post, dispatch } = this.props;
    if (!this.state.doingAction) {
      this.setState({ doingAction: true });
      dispatch(favouritePost(post));
      dispatch(favouriteTopicPost(post));
      dispatch(addFavPopularPost(post));
      setFavouritePost(post.id).then(data => {
        this.setState({ doingAction: false });
      }).catch(e => {
        dispatch(favouritePost(post));
        dispatch(favouriteTopicPost(post));
        dispatch(addFavPopularPost(post));
        this.setState({ doingAction: false });
        showError(e.status, this.props.t);
      });
    }
  }

  render () {
    const { upVotesCount, commentsCount, isPinned, downVotesCount, isFavourite, currentUsersVote } = this.props;

    return (
      <CardItem>
        <Left>
          <View style={inlineStyles.rowWrapper}>
            <View style={inlineStyles.container}>
              <View style={inlineStyles.voteContainer}>
                <TouchableOpacity onPressIn={this.handleUpVote}
                  style={inlineStyles.voteButtonContainer}>
                  {currentUsersVote === true
                    ? (<Icon name="md-arrow-round-up" style={styles.active} />)
                    : (<Icon name="ios-arrow-round-up" style={styles.threadTools} />)}
                </TouchableOpacity>
                <View style={inlineStyles.voteCountContainer}>
                  <Text style={inlineStyles.voteCounter}>
                    {upVotesCount - downVotesCount}
                  </Text>
                </View>
                <TouchableOpacity onPressIn={this.handleDownVote} style={inlineStyles.voteButtonContainer}>
                  {currentUsersVote === false
                    ? (<Icon name="md-arrow-round-down" style={styles.active}/>)
                    : (<Icon name="ios-arrow-round-down" style={styles.threadTools} />)}
                </TouchableOpacity>
              </View>
            </View>
            <View style={{ flex: 1 }}>
              <TouchableOpacity onPressIn={this.setAsFavouritePost}>
                {isFavourite
                  ? <Icon name="ios-heart" style={inlineStyles.favActive} />
                  : <Icon name="ios-heart" style={inlineStyles.favInactive} />
                }
              </TouchableOpacity>
            </View>
          </View>
        </Left>
        <Right>
          <View style={inlineStyles.rightContainer}>
            <Icon name="md-mail" style={styles.threadTools} />
            <Text style={inlineStyles.commentsCount}>
              {commentsCount}
            </Text>
            <TouchableOpacity>
              <Icon name="md-share" style={inlineStyles.shareButton} />
            </TouchableOpacity>
            {isPinned
              ? <View style={inlineStyles.pinContainer}>
                <TouchableOpacity>
                  <Text>P</Text>
                </TouchableOpacity>
              </View>
              : null}
          </View>
        </Right>
      </CardItem>
    );
  }
}
export default connect()(PostActions);
