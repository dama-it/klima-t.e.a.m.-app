import React from 'react';
import { ActivityIndicator } from 'react-native';

export const PostListFooter = ({ posts, loading }) => {
  return posts && loading ? <ActivityIndicator size={30} color='#00A19A' /> : null;
};
