import 'moment/locale/de-at';
import { ActionSheet, Button, Icon, Text, View } from 'native-base';
import React from 'react';
import { withTranslation } from 'react-i18next';

const styles = {
  iconDown: { color: '#01A19A' }
};

const PostListHeader = ({ t, handleSort, handleDisplayAs, ...props }) => {
  const buttonsPostView = [
    t('screens.homeDetailsScreen.cards'),
    t('screens.homeDetailsScreen.list'),
    t('screens.homeDetailsScreen.cancel')
  ];
  const buttonsSortPosts = [t('screens.homeDetailsScreen.newPosts'),
    t('screens.homeDetailsScreen.hot'),
    t('screens.homeDetailsScreen.cancel')
  ];
  const cancelIndexPostView = 3;
  const cancelIndexSortPost = 3;
  const showSortActionSheet = () =>
    ActionSheet.show(
      {
        options: buttonsSortPosts,
        cancelButtonIndex: cancelIndexSortPost,
        title: t('screens.homeDetailsScreen.sortPosts')
      },
      buttonIndex => handleSort(buttonIndex)
    );

  const showDisplayAsActionScreen = () =>
    ActionSheet.show(
      {
        options: buttonsPostView,
        cancelButtonIndex: cancelIndexPostView,
        title: t('screens.homeDetailsScreen.displayPostsAs')
      },
      buttonIndex => handleDisplayAs(buttonIndex)
    );

  return (

    <View>
      <View
        style={{
          flexDirection: 'row',
          backgroundColor: '#eeeeee'
        }}
      >
        <View style={{ flex: 1 }}>
          <Button
            iconRight
            transparent
            small
            full
            style={{ justifyContent: 'center' }}
            onPress={showSortActionSheet}
          >
            <Text style={{ color: '#01A19A' }}>{t('screens.homeDetailsScreen.sortPosts')}</Text>
            <Icon
              style={styles.iconDown}
              name="ios-arrow-down"
            />
          </Button>
        </View>
        <View style={{ flex: 1 }}>
          <Button
            iconRight
            transparent
            small
            full
            style={{ justifyContent: 'center' }}
            onPress={showDisplayAsActionScreen}
          >
            <Text style={{ color: '#01A19A' }}>{t('screens.homeDetailsScreen.displayPostsAs')}</Text>
            <Icon
              style={styles.iconDown}
              name="ios-arrow-down"
            />
          </Button>
        </View>
      </View>
    </View>);
};

export default withTranslation()(PostListHeader);
