// React imports
import moment from 'moment';
import { Body, Card, CardItem, H1, Icon, Left, Right, Text, Thumbnail, View } from 'native-base';
import React from 'react';
// React Native imports
import { Image } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
// Other imports
import { withTranslation } from 'react-i18next';
import { getBannerSrc, getIconSrc } from '../utils/imageUtils';
import PostActions from './PostActions';
import styles from './../styles/styles';
import CreatedBy from './post/CreatedBy';
import PostCreatedBy from './post/PostCreatedBy';

const HidedPostMarker = ({ post }) => {
  return post.active ? null : <Icon style={{ color: '#01A19A' }} type="Ionicons" name="ios-eye-off" />;
};

class PostListItem extends React.PureComponent {
  selectPost = () => {
    this.props.selectPost(this.props.post);
  }

  selectTopic = () => {
    this.props.selectTopic(this.props.post.topic);
  }

  iconSrc = {
    uri: getIconSrc(this.props.post.topic)
  }

  bannerSrc = () => {
    return { uri: getBannerSrc(this.props.post) };
  }

  render () {
    const { t, post, selectPost, downVoteCall, upVoteCall, selectTopic, displayAsListItem } = this.props;

    const topic = post.topic ? post.topic : { name: 'kein Topic' };

    if (displayAsListItem) {
      return (
        <Card style={styles.cardListContainer}>
          <CardItem header style={{ paddingBottom: 0 }}>
            <Left>
              <TouchableOpacity onPress={this.selectTopic}>
                <Thumbnail
                  style={{ width: 28, height: 28 }}
                  small
                  source={this.iconSrc}
                />
              </TouchableOpacity>
              <Body>
                <PostCreatedBy post={post} selectTopic={this.selectTopic}/>
              </Body>
            </Left>
            <Right>
              <HidedPostMarker post={post} />
            </Right>
          </CardItem>
          <TouchableOpacity onPress={this.selectPost}>
            <View style={styles.postImageContainer}>
              <View style={styles.postTitleContainer}>
                <H1 onPress={this.selectPost} style={styles.selectPostListContent}>
                  {post.title}
                </H1>
              </View>
              {
                this.props.post.banner && <View style={styles.imageListViewParentContainer}>
                  <Image
                    source={this.bannerSrc()}
                    style={styles.imageListViewStyle}
                  />
                </View>
              }
            </View>
          </TouchableOpacity>
          <PostActions
            post={post}
            selectPost={selectPost}
            downVoteCall={downVoteCall}
            upVoteCall={upVoteCall}
            {...post}
          />
        </Card>
      );
    } else {
      return (
        <Card>
          <CardItem header>
            <Left>
              <TouchableOpacity onPress={this.selectTopic}>
                <Thumbnail
                  small
                  source={this.iconSrc}
                />
              </TouchableOpacity>
              <Body>
                <Text
                  onPress={this.selectTopic}
                >
                  {post.topic ? topic.name : null}
                </Text>
                <CreatedBy post={post}/>
              </Body>
            </Left>
            {
              !post.active && <Right>
                <HidedPostMarker post={post} />
              </Right>
            }
          </CardItem>
          {
            this.props.post.banner && <TouchableOpacity onPress={this.selectPost}>
              <CardItem cardBody>
                <Image
                  source={this.bannerSrc()}
                  style={styles.imageContainer}
                />
              </CardItem>
            </TouchableOpacity>
          }
          <CardItem>
            <Body>
              <H1 onPress={this.selectPost}>
                {post.title}
              </H1>
            </Body>
          </CardItem>
          <PostActions
            post={post}
            selectPost={selectPost}
            downVoteCall={downVoteCall}
            upVoteCall={upVoteCall}
            {...post}
          />
        </Card>
      );
    }
  }
}

export default withTranslation()(PostListItem);
