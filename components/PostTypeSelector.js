import React from 'react';
import { View, Text, Icon } from 'native-base';
import { MaterialIcons, MaterialCommunityIcons, Ionicons } from '@expo/vector-icons';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { setCompetitionTopic } from '../store/actions/competition';
import { withTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';

const PostTypeSelector = ({ selected, navigation, topic, dispatch, t }) => {
  const skipIntro = async () => {
    try {
      return await AsyncStorage.getItem('skipCompetitionInit');
    } catch (e) { }
  };

  const handleNewPost = () => {
    navigation.goBack();
    if (selected) {
      navigation.navigate('New', {
        topic: topic
      });
    } else {
      navigation.navigate('New', {
        topic: null
      });
    }
  };

  const handleNewCompetition = () => {
    navigation.goBack();
    if (selected) {
      dispatch(setCompetitionTopic(topic));
      skipIntro().then(val => {
        if (val === 'true') {
          navigation.navigate('WizzardStack', { screen: 'CompetitionTopicAndTypeSelectScreen', params: { topic: topic } });
        } else {
          navigation.navigate('WizzardStack', { screen: 'CompetitionIntroScreen', params: { topic: topic } });
        }
      });
    } else {
      skipIntro().then(val => {
        if (val === 'true') {
          navigation.navigate('WizzardStack', { screen: 'CompetitionTopicAndTypeSelectScreen', params: { topic: null } });
        } else {
          navigation.navigate('WizzardStack', { screen: 'CompetitionIntroScreen', params: { topic: null } });
        }
      });
    }
  };

  const navigateBack = () => navigation.goBack();

  return (
    <View style={{
      backgroundColor: 'rgba(0,0,0,0.8)',
      width: '100%',
      height: '40%',
      position: 'absolute',
      bottom: 0,
      justifyContent: 'space-evenly'

    }}>
      <Text style={{ alignSelf: 'center', color: '#fff' }}>{t('screens.postTypeSelector.somethingNew')}</Text>
      <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center', alignContent: 'center', flex: 0.5 }}>
        <TouchableOpacity style={{ flexDirection: 'column', alignContent: 'center', alignItems: 'center', justifyContent: 'center' }}
          onPress={handleNewPost}
        >
          <View style={{ width: 60, height: 60, borderRadius: 30, backgroundColor: '#fff', alignItems: 'center', justifyContent: 'center' }}>
            <MaterialIcons size={40} color={'#000'} name="playlist-add" />
          </View>
          <Text style={{ color: '#ffffff', paddingTop: 10 }}>{t('screens.postTypeSelector.newPost')}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={handleNewCompetition}
          style={{ flexDirection: 'column', alignContent: 'center', alignItems: 'center', justifyContent: 'center' }}
        >
          <View style={{ width: 60, height: 60, borderRadius: 30, backgroundColor: '#fff', alignItems: 'center', justifyContent: 'center' }}>
            <Ionicons size={40} color={'#000'} name="ios-trophy" />
          </View>
          <Text style={{ color: '#ffffff', paddingTop: 10 }}>{t('screens.postTypeSelector.newCompetition')}</Text>
        </TouchableOpacity>
      </View>
      <TouchableOpacity onPress={navigateBack} style={{ flexDirection: 'row', justifyContent: 'center' }}>
        <Icon style={{ color: '#ffffff' }} name="md-close" />
        <Text style={{ color: '#ffffff', paddingTop: 5, paddingLeft: 5 }}>{t('screens.postTypeSelector.cancel')}</Text>
      </TouchableOpacity>
    </View>);
};

const mapStateToProps = state => {
  return {
    topic: state.topic ? state.topic.currentTopic : null,
    selected: state.topic ? state.topic.selected : false
  };
};

export default connect(mapStateToProps)(withTranslation()(PostTypeSelector));
