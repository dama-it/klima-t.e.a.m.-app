import { Text, View, ListItem, Right, Left, Body } from 'native-base';
import React from 'react';
import { withTranslation } from 'react-i18next';
import moment from 'moment';
import { replaceDotWithComma } from '../utils/formatUtils';
import ListFooter from './ListFooter';

const styles = {
  competitionFooterTimeRemainingContainer: {
    width: 90,
    height: 90,
    borderRadius: 45,
    borderColor: '#ffffff',
    borderStyle: 'solid',
    borderWidth: 4,
    justifyContent: 'center',
    alignItems: 'center'
  }
};

export const QuantitativeCompetitionFooter = ({ t, timeDiffObject, startValue, endValue, unit, competitionActionList }) => <View>
  <View style={{ backgroundColor: '#EEEEEE', height: 20, borderTopWidth: 1, borderBottomWidth: 1, borderColor: '#DDDDDD' }}></View>
  <View style={{ backgroundColor: '#00A29B', alignItems: 'center', paddingTop: 10, paddingBottom: 10 }}>
    <View>
      <Text style={{ color: '#ffffff', fontSize: 20, paddingBottom: 15 }}>
        {t('screens.competitionThreadScreen.goal') + ':'}
      </Text>
    </View>
    <View style={styles.competitionFooterTimeRemainingContainer}>
      <Text style={{ color: '#ffffff', fontSize: 40 }}>{replaceDotWithComma(endValue)}</Text>
    </View>
    <View>
      <Text style={{ color: '#ffffff', fontSize: 20, paddingTop: 15 }}>{unit}</Text>
    </View>
    {timeDiffObject
      ? <View>
        <Text style={{ color: '#ffffff', fontSize: 20, paddingTop: 15 }}>
          {t('screens.competitionThreadScreen.still') + ' ' +
           timeDiffObject.timeDiff + ' ' +
            t('timeUnits.' + timeDiffObject.unit) + ' ' +
             t('screens.competitionThreadScreen.toReachYourGoal')}
        </Text>
      </View> : null}
  </View>
  <ListFooter/>
  {competitionActionList && competitionActionList.length > 0
    ? <View>
      <ListItem noBorder><Text>{t('screens.competitionThreadScreen.updates') + ':'}</Text></ListItem>
      {competitionActionList.map((competitionAction, key) =>
        <ListItem key={key}>
          <Left>
            <Text note>{moment(competitionAction.createdAt).format('L')}</Text>
          </Left>
          <Body>
            <Text>{replaceDotWithComma(getCurrentAmount(startValue, competitionActionList, key))}</Text>
          </Body>
          <Right>
            <Text note>{replaceDotWithComma(Math.round(competitionAction.relativeValue * 10) / 10)}</Text>
          </Right>
        </ListItem>)}
    </View>
    : null
  }
</View>;

const getCurrentAmount = (startValue, updates, key) => {
  let currentAmount = startValue;
  for (let i = 0; i <= key; i++) {
    currentAmount += updates[i].relativeValue;
  }
  return currentAmount;
};

export default withTranslation()(QuantitativeCompetitionFooter);
