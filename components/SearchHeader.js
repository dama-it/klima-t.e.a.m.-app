import 'moment/locale/de-at';
import { Button, Header, Icon, Input, View } from 'native-base';
import React, { useState } from 'react';
import { withTranslation } from 'react-i18next';
import { Image, Keyboard } from 'react-native';
import styles from '../styles/styles';
import { useNavigation } from '@react-navigation/native';

const SearchHeader = ({ t, handleSearch, additionalButton, logo, handleLoadingSpinner }) => {
  const navigation = useNavigation();
  const [searchFocused, setSearchFocused] = useState(false);
  const [value, setValue] = useState('');

  const onChange = (event) => {
    setValue(event.nativeEvent.text);
    handleSearch(event.nativeEvent.text);
  };

  const focus = () => {
    setSearchFocused(true);
  };

  const unfocus = () => {
    setSearchFocused(false);
    handleLoadingSpinner(false);
    Keyboard.dismiss();
    if (value && value !== '') {
      handleSearch('');
    };
  };

  const openDrawer = () => {
    navigation.openDrawer();
  };

  return (
    <Header style={styles.mainHeader}>
      <View style={styles.mainHeaderWrapper}>
        <View style={styles.mainHeaderMenuWrapper}>
          {
            !searchFocused
              ? <Button transparent onPress={openDrawer}>
                <Icon type="Feather" name="menu" style={styles.mainHeaderIcon} />
              </Button>
              : <Button transparent onPress={unfocus}>
                <Icon type="AntDesign" name="arrowleft" style={styles.iconDown} />
              </Button>
          }
        </View>
        {
          searchFocused
            ? <View style={styles.mainHeaderInput}>
              <Input
                value={value}
                onChange={onChange}
                placeholder={t('screens.homeScreen.search')}
                autoFocus
              />
            </View>
            : <View style={styles.mainHeaderLogoContainer}>
              {logo && (
                <Image
                  style={styles.mainHeaderLogo}
                  source={require('../../assets/logos/acheeva_logo-v.png')}
                />
              )}
            </View>
        }
        <View style={styles.mainHeaderMultiButtonMainWrapper}>
          <View style={styles.mainHeaderMultiButtonWrapper }>
            {
              !searchFocused &&
            <Button transparent onPress={focus}>
              <Icon name="ios-search" size={20} style={styles.mainHeaderIcon}/>
            </Button>
            }
            {
              additionalButton || null
            }
          </View>
        </View>
      </View>
    </Header>
  );
};

export default withTranslation()(SearchHeader);
