import { Body, Button, Header, Icon, Input, Item, Left, ListItem, Right, Container, Text, Thumbnail, View } from 'native-base';
import React, { Component } from 'react';
import { ActivityIndicator, Modal, RefreshControl } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';
import { addTopics, setTopics } from '../store/actions/topics';
import styles from '../styles/styles';
import { getIconSrc } from '../utils/imageUtils';
import { getTopics } from '../utils/restUtils';
import { showError } from './Messages';

class SelectTopicModal extends Component {
  state = {
    topicsLength: 0,
    topicsPerPage: 20,
    page: 0,
    topics: [],
    orderSettings: {
      column: 'id',
      order: 'asc'
    },
    filter: {},
    loadingNewPage: false,
    lastPageLoadedAt: null,
    refreshing: false,
    refreshingDetails: false,
    searchString: '',
    isSearching: false
  };

  async componentDidMount () {
    const reset = true;
    this.getTopics(reset);
  }

  getTopics = (reset) => {
    this.setState({ loadingNewPage: true });
    getTopics(this.state.page, this.state.topicsPerPage, this.state.orderSettings.column, this.state.orderSettings.order, this.state.filter).then(
      response => {
        this.props.dispatch(reset ? setTopics(response.data) : addTopics(response.data));
        this.setState({ refreshing: false, loadingNewPage: false, isSearching: false, lastPageLoadedAt: (new Date()).getTime() });
      }).catch(e => {
      showError('ERROR.ERROR', this.props.t);
    });
  };

  selectTopic = item => {
    this.props.onSelectTopic(item);
  };

  onEndReached = () => {
    this.setState({ page: this.state.page + 1 }, () => this.getTopics(false));
  };

  onRefreshing = () => {
    this.setState({ page: 0, refreshing: true }, () => this.getTopics(true));
  };

  handleSearch = e => {
    this.setState({ searchString: e.nativeEvent.text });
    if ((e.nativeEvent.text.length >= 1 && !this.state.loading)) {
      this.setState({ isSearching: true });
    }
    this.setState(
      { page: 0, filter: { fullText: e.nativeEvent.text } },
      () => this.getTopics(true)
    );
  }

  closeModal = () => {
    this.setState({ isSearching: false });
    this.props.closeModal();
  }

  renderItem = ({ item, index, separators }) => <ListItem avatar key={'topic_' + index}>
    <Left>
      <Thumbnail
        small
        source={{
          uri: getIconSrc(item)
        }}
      />
    </Left>
    <Body style={styles.noBorder}>
      <Text
        onPress={() => this.selectTopic(item)}
      >
        {item.name}
      </Text>
    </Body>
  </ListItem>

  keyExtractor = (item, index) => index.toString()

  renderFooter = () => this.state.loadingNewPage && this.props.topics.length > 0
    ? <ActivityIndicator size={0} style={{ color: '#fff' }}/>
    : null

  renderListEmpty = () => <ActivityIndicator size={0} style={{ color: '#fff', marginTop: '50%' }}/>

  render () {
    const navigation = this.props.navigation;
    const { t, topics } = this.props;
    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.props.visible}
      >
        <Container>
          <Header searchBar>
            <Left>
              <Button transparent onPress={this.closeModal}>
                <Icon name="md-close" />
              </Button>
            </Left>
            <Body>
              <Item>
                <Input
                  placeholder={t('screens.favourites.search')}
                  onChange={this.handleSearch}
                  value={this.state.searchString}
                />
              </Item>
            </Body>
            <Right>
              <Button transparent>
                {(this.state.searchString && this.state.searchString.length > 1)
                  ? <ActivityIndicator size="small" animating={this.state.isSearching}/>
                  : <Icon name="ios-search" size={20} onPress={() => this.ref._root.focus()}/>}
              </Button>
            </Right>
          </Header>
          <View style={styles.listContainer}>
            <FlatList
              style={styles.whiteBackground}
              navigation={navigation}
              data={topics || []}
              renderItem={this.renderItem}
              refreshControl={<RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.onRefreshing}
              />}
              onEndReached={this.onEndReached}
              onEndReachedThreshold={0.5}
              initialNumToRender={20}
              keyExtractor={this.keyExtractor}
              ListFooterComponent={this.renderFooter}
              ListEmptyComponent={this.renderListEmpty}
            />
          </View>
        </Container>
      </Modal>
    );
  }
}

const mapStateToProps = state => {
  return {
    topics: state && state.topic && state.topic.list && state.topic.list.length > 0 ? state.topic.list : [],
    counter: state.topic ? state.topic.counter : null
  };
};

export default connect(mapStateToProps)(withTranslation()(SelectTopicModal));
