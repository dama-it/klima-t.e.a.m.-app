import AsyncStorage from '@react-native-async-storage/async-storage';
import { useNavigation } from '@react-navigation/native';
import { CheckBox, Left, ListItem, Right, Text } from 'native-base';
import React, { useEffect, useState } from 'react';

const SkipCompetitionCreationIntroCheckbox = ({ title }) => {
  const [skipCheckbox, setSkipCheckbox] = useState(false);
  const navigation = useNavigation();

  const skipIntro = async () => {
    try {
      return await AsyncStorage.getItem('skipCompetitionInit');
    } catch (e) { }
  };

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      skipIntro().then(val => {
        setSkipCheckbox(val === 'true');
      });
    });
    return unsubscribe;
  }, [navigation]);

  const skipCheckboxClick = () => {
    const val = !skipCheckbox;
    setSkipCheckbox(val);
    saveSkipCheckboxState(val);
  };

  const saveSkipCheckboxState = async (val) => {
    AsyncStorage.setItem('skipCompetitionInit', JSON.stringify(val));
  };

  return <ListItem>
    <Left>
      <Text>{title}</Text>
    </Left>
    <Right>
      <CheckBox checked={skipCheckbox} onPress={skipCheckboxClick}/>
    </Right>
  </ListItem>;
};

export default SkipCompetitionCreationIntroCheckbox;
