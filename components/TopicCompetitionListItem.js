// React imports
import moment from 'moment';
import { Card, CardItem, Left, Text } from 'native-base';
import React from 'react';
// React Native imports
import { TouchableWithoutFeedback } from 'react-native';
// Other imports
import { withTranslation } from 'react-i18next';
import { GENERAL_CHALLENGES, QUANTITATIVE_CHALLENGES } from '../utils/competitionTypes';
import { replaceDotWithComma } from '../utils/formatUtils';
import { BadgeBar } from './BadgeBar';
import PostActions from './PostActions';
import styles from '../styles/styles';
import CreatedBy from './post/CreatedBy';

class TopicCompetitionListItem extends React.PureComponent {
  render () {
    const { t, post, selectPost, downVoteCall, upVoteCall, ...props } = this.props;
    const competition = post.competition;
    return (
      <Card >
        <CardItem cardBody>
          <Left>
            <CreatedBy post={post}/>
          </Left>
        </CardItem>

        <CardItem cardBody>
          <Left>
            <Text style={{ paddingLeft: 2, paddingTop: 2 }}>{post.title}</Text>
          </Left>
        </CardItem>
        <TouchableWithoutFeedback onPress={() => { selectPost(post); }}>
          <CardItem >
            <Text style={[{ paddingTop: 0 }, styles.content]} note>
              {t('screens.competitionThreadScreen.subtitle') + ' ' +
                     competition.timeAmount + ' ' +
                      t('timeUnits.' + competition.timeUnit) + ' ' +
                     competition.goal}
              {competition.type === GENERAL_CHALLENGES ? '.' : null}
              {competition.type === QUANTITATIVE_CHALLENGES
                ? ' von ' + replaceDotWithComma(competition.startValue) + competition.unit +
              ' auf ' + replaceDotWithComma(competition.endValue) + competition.unit
                : null}
              {competition.type === QUANTITATIVE_CHALLENGES && (competition.startValue - competition.endValue > 0)
                ? t('screens.competitionThreadScreen.decrease') : null}
              {competition.type === QUANTITATIVE_CHALLENGES && (competition.startValue - competition.endValue < 0)
                ? t('screens.competitionThreadScreen.increase') : null}
            </Text>
          </CardItem>
        </TouchableWithoutFeedback>

        <BadgeBar post={post}/>

        <PostActions
          post={post}
          selectPost={selectPost}
          downVoteCall={downVoteCall}
          upVoteCall={upVoteCall}
          {...post}
        />
      </Card>);
  }
};

export default withTranslation()(TopicCompetitionListItem);
