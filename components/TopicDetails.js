import { Accordion, Button, H1, Text, View } from 'native-base';
import React from 'react';
import { withTranslation } from 'react-i18next';
import { Image } from 'react-native';
import { syncTopic } from '../store/actions/topics';
import { getBannerSrc } from '../utils/imageUtils';
import { setFavouriteTopic } from '../utils/restUtils';
import { showError } from './Messages';
import moment from 'moment';
import styles from '../styles/styles';
import { connect } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { openChat } from '../utils/openTopicChat';

const TopicDetails = ({ topic, t, dispatch }) => {
  const navigation = useNavigation();

  const setFavourite = () => {
    setFavouriteTopic(topic.id).then((data) => {
      dispatch(syncTopic(data.data));
    }).catch(e => {
      showError('ERROR.ERROR', t);
    });
  };

  const joinChat = () => {
    openChat(topic.conversation, navigation, dispatch, t);
  };

  return (
    <>
      <Image source={{ uri: getBannerSrc(topic) }} style={styles.topicBanner} />
      <View style={styles.topicHeader}>
        <View style={styles.topicHeadingWrapper}>
          <H1 style={[styles.topicHeading, styles.content]}>{topic.name}</H1>
          <Text style={[styles.topicHeading, styles.content]}>
            {topic.registeredCount ? topic.registeredCount : 0}
            {' '}{topic && topic.registeredCount === 1 ? t('screens.topic.memberCount') : t('screens.topic.membersCount')}
            {' | '}{t('screens.topic.lastPost')}:{' '}
            {topic.latestPostDate ? moment(topic.latestPostDate, 'YYYY-MM-DD HH:mm:ss').format('L').toString() : null}
          </Text>
        </View>
        <Accordion
          dataArray={[{ title: t('screens.topic.description'), content: topic ? topic.description : t('screens.topic.noDescription') }]}
          icon="ios-arrow-up"
          expandedIcon="ios-arrow-down"
          iconStyle={{ color: '#01A19A' }}
          expandedIconStyle={{ color: '#01A19A' }}
          style={{ marginTop: 15, marginBottom: 10 }}
          contentStyle={{ backgroundColor: 'white' }}
          // expanded={topic && topic.description && topic.description.length > 250 ? null : 0}
        />
        <View style={styles.topicRowContainer}>
          <View style={styles.topicRowButtonWrapper}>
            {
              topic.conversation && <Button full style={styles.button} onPress={joinChat}>
                <Text style={styles.buttonText}>
                  {t('screens.topic.actions.chat')}
                </Text>
              </Button>
            }
          </View>
          <View style={styles.topicRowButtonWrapper}>
            <Button full style={styles.button} onPress={setFavourite}>
              <Text style={styles.buttonText}>
                {topic.isFavourite === true
                  ? t('screens.topic.unsubscribe')
                  : t('screens.topic.subscribe')}
              </Text>
            </Button>
          </View>
        </View>
      </View>
    </>
  );
};

const mapStateToProps = state => {
  return {
    topic: state.topic ? state.topic.currentTopic : null
  };
};

export default connect(mapStateToProps)(withTranslation()(TopicDetails));
