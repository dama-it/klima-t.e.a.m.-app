import React from 'react';
import { setCompetitionTopic } from '../store/actions/competition';
import { connect } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import BottomRightFab from './BottomRightFab';

const TopicFab = ({ topic, isCompetition, dispatch }) => {
  const navigation = useNavigation();
  const getIconName = isCompetition ? 'ios-trophy' : 'playlist-add';
  const getIconType = isCompetition ? 'Ionicons' : 'MaterialIcons';

  const addPost = () => {
    navigation.navigate('New', { topic: topic });
  };

  const addCompetition = () => {
    dispatch(setCompetitionTopic(topic));
    navigation.navigate('WizzardStack', { screen: 'CompetitionIntroScreen', params: { topic: topic } });
  };

  const press = () => {
    isCompetition
      ? addCompetition()
      : addPost();
  };

  return (
    <BottomRightFab onPress={press} iconName={getIconName} iconType={getIconType} />
  );
};

export default connect()(TopicFab);
