// React imports
import { Body, Button, Icon, Left, ListItem, Right, Text, Thumbnail } from 'native-base';
import React from 'react';
import { TouchableOpacity } from 'react-native-gesture-handler';
import styles from '../styles/styles';
import { getIconSrc } from '../utils/imageUtils';

class TopicListItem extends React.PureComponent {
    selectTopic = () => this.props.selectTopic(this.props.topic)
    setFavourite = () => this.props.setFavourite(this.props.topic)
    iconSrc = { uri: getIconSrc(this.props.item) }

    render () {
      const { index, isFavourite, name } = this.props;
      return (
        <ListItem avatar key={'topic_' + index}>
          <Left>
            <TouchableOpacity onPress={this.selectTopic}>
              <Thumbnail
                small
                source={this.iconSrc}
              />
            </TouchableOpacity>
          </Left>
          <Body style={styles.noBorder}>
            <Text
              onPress={this.selectTopic}
              style={styles.content}
            >
              {name}
            </Text>
          </Body>
          <Right style={styles.noBorder}>
            <Button
              transparent
              onPress={this.setFavourite}
            >
              <Icon
                name="ios-star"
                style={isFavourite ? styles.active : styles.inactive}
              />
            </Button>
          </Right>
        </ListItem>);
    }
}

export default TopicListItem;
