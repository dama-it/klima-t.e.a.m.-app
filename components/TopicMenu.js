import { ActionSheet, Button, Icon } from 'native-base';
import React from 'react';
import { withTranslation } from 'react-i18next';
import { setFavouriteTopic } from '../utils/restUtils';
import { showError } from './Messages';
import styles from '../styles/styles';
import { connect } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { syncTopic } from '../store/actions/topics';
import { openChat } from '../utils/openTopicChat';

var cancelThreadOptions = 1;

const TopicMenu = ({ t, topic, dispatch }) => {
  const navigation = useNavigation();

  const getOptions = () => {
    var options = [];
    options.push(topic.isFavourite ? t('screens.topic.unsubscribe') : t('screens.topic.subscribe'));
    options.push(t('screens.topic.actions.cancel'));
    if (topic.conversation) {
      options.push(t('screens.topic.actions.chat'));
    }
    return options;
  };

  const showActionSheet = () => {
    ActionSheet.show(
      {
        options: getOptions(),
        cancelButtonIndex: cancelThreadOptions,
        title: t('screens.topic.options')
      },
      buttonIndex => {
        performAction(buttonIndex);
      }
    );
  };

  const performAction = (index) => {
    if (index === 0) {
      setFavourite(topic.id);
    } else if (index === 2 && topic.conversation) {
      openChat(topic.conversation, navigation, dispatch, t);
    }
  };

  const setFavourite = id => {
    setFavouriteTopic(id).then((data) => {
      dispatch(syncTopic(data.data));
    }).catch(e => {
      showError('ERROR.ERROR', t);
    });
  };

  return (
    <Button transparent onPress={showActionSheet}>
      <Icon name="md-more" style={styles.mainHeaderIcon}/>
    </Button>
  );
};

const mapStateToProps = state => {
  return {
    topic: state.topic ? state.topic.currentTopic : null
  };
};

export default connect(mapStateToProps)(withTranslation()(TopicMenu));
