// React imports
import moment from 'moment';
import { Body, Card, CardItem, H1, Left, Right, Text, Thumbnail } from 'native-base';
import React from 'react';
// Other imports
import { withTranslation } from 'react-i18next';
// React Native imports
import { Image } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { getBannerSrc, getIconSrc } from '../utils/imageUtils';
import PostActions from './PostActions';
import styles from '../styles/styles';
import CreatedBy from './post/CreatedBy';

class TopicPostListItem extends React.PureComponent {
  selectPost = () => this.props.selectPost(this.props.post)

  bannerSrc = () => {
    return {
      uri: getBannerSrc(this.props.post)
    };
  }

  iconSrc = () => {
    return {
      uri: getIconSrc(this.props.post && this.props.post.topic)
    };
  }

  render () {
    const { t, post, selectPost, downVoteCall, upVoteCall, selectTopic, displayAsListItem } = this.props;

    if (displayAsListItem) {
      return (
        <Card transparent style={styles.listItemStyle}>
          <CardItem>
            <Left>
              <CreatedBy post={post}/>
            </Left>
          </CardItem>
          <TouchableOpacity onPress={this.selectPost}>
            <CardItem>
              <Left>
                <Text>{post.title}</Text>
              </Left>
              <Right>
                {
                  this.props.post.banner && <Thumbnail
                    square
                    large
                    source={this.bannerSrc()}
                  />
                }
              </Right>
            </CardItem>
          </TouchableOpacity>
          <PostActions
            post={post}
            selectPost={selectPost}
            downVoteCall={downVoteCall}
            upVoteCall={upVoteCall}
            {...post}
          />
        </Card>
      );
    } else {
      return (
        <Card>
          <CardItem header>
            <Left>
              <TouchableOpacity onPress={selectTopic}>
                <Thumbnail
                  small
                  source={this.iconSrc()}
                />
              </TouchableOpacity>
              <Body>
                <Text style={styles.content}>
                  {post.topic.name}
                </Text>
                <CreatedBy post={post}/>
              </Body>
            </Left>
          </CardItem>
          {
            this.props.post.banner && <TouchableOpacity onPress={() => selectPost(post)}>
              <CardItem cardBody>
                <Image
                  source={this.bannerSrc()}
                  style={{ height: 200, width: null, flex: 1, resizeMode: 'contain' }}
                />
              </CardItem>
            </TouchableOpacity>
          }
          <CardItem>
            <Body>
              <H1 onPress={this.selectPost} style={styles.content}>
                {post.title}
              </H1>
            </Body>
          </CardItem>

          <PostActions
            post={post}
            selectPost={selectPost}
            downVoteCall={downVoteCall}
            upVoteCall={upVoteCall}
            {...post}
          />
        </Card>);
    }
  }
}

export default withTranslation()(TopicPostListItem);
