import React from 'react';
import { View, H3 } from 'native-base';
import { withTranslation } from 'react-i18next';
import { Image } from 'react-native';
import styles from '../styles/styles';

const TrophyAndXp = ({ t, xp }) => <View style={styles.trophyWrapper}>
  <View>
    <View style={styles.trophyItem}>
      <Image
        source={require('../../assets/medal-icon.png')}
        style={styles.trophyIcon}
      />
      <H3>{t('screens.CompetitionCompleteScreen.newTrophy')}</H3>
    </View>
    <View style={styles.trophyItem}>
      <Image
        source={require('../../assets/gift-icon.png')}
        style={styles.trophyIcon}
      />
      <H3>+{xp} XP</H3>
    </View>
  </View>
</View>;

export default withTranslation()(TrophyAndXp);
