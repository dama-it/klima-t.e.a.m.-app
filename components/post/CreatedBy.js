import React from 'react';
import moment from 'moment';
import { Text } from 'native-base';
import { withTranslation } from 'react-i18next';
import { useNavigation } from '@react-navigation/native';
import styles from '../../styles/styles';
const CreatedBy = ({ t, post }) => {
  const navigation = useNavigation();

  const openProfile = (user) => {
    if (user) {
      navigation.navigate('Profile', { userId: user.id });
    }
  };

  return (
    <>
      <Text note>
        {t('screens.postList.postedBy')}
        {' '}
        <Text
          onPress={() => openProfile(post.author)}
          note
          style={styles.postAuthor}
        >
          {post.author ? post.author.username : null}
        </Text>
        {' | '}
        {moment(post.createdAt).fromNow()}
      </Text>
    </>
  );
};

export default withTranslation()(CreatedBy);
