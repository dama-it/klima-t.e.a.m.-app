import React from 'react';
import { Text, View } from 'native-base';
import styles from '../../styles/styles';
import CreatedBy from './CreatedBy';

const PostCreatedBy = (props) => {
  const { post, selectTopic } = props;

  return (
    <View style={styles.postedByContainer}>
      <Text note
        onPress={selectTopic}
      >
        {post.topic ? post.topic.name : null}
        {' | '}
      </Text>
      <CreatedBy post={post}/>
    </View>
  );
};

export default PostCreatedBy;
