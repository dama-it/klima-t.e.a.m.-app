import React from 'react';
import { FlatList } from 'react-native-gesture-handler';
import { ActivityIndicator, RefreshControl } from 'react-native';
import { View } from 'native-base';
import styles from '../../styles/styles';
import { blockAuthorCall, deleteComment, getUserComments, reportCall } from '../../utils/restUtils';
import { connect } from 'react-redux';
import CommentHeader from '../CommentHeader';
import Comment from '../Comment';
import { addProfileComments, deleteCommentAction, downVoteCommentAction, selectComment, setProfileComments, upVoteCommentAction } from '../../store/actions/comments';
import { showError, showMessage } from '../Messages';
import { downVoteComment, upVoteComment } from '../../utils/voteRestUtils';
import { selectPost } from '../../store/actions/posts';
import { reset } from 'redux-form';

class CommentFlatList extends React.Component {
    state = {
      loading: true,
      refreshing: false,
      page: 1,
      comments: [],
      lastPageLoadedAt: null,
      endReached: false
    };

    componentDidMount () {
      this.props.dispatch(setProfileComments([]));
      this.getUserComments(true);
    }

    getUserComments = (reset) => {
      this.setState({ loading: true });
      getUserComments(this.state.page, 10, this.props.user.id).then(success => {
        if (success.data.length === 0) this.setState({ endReached: true });
        this.props.dispatch(reset ? setProfileComments(success.data) : addProfileComments(success.data));
        this.setState({ refreshing: false, loading: false, lastPageLoadedAt: (new Date()).getTime() });
      }).catch(e => {
        this.setState({ refreshing: false, loading: false, lastPageLoadedAt: (new Date()).getTime() });
      });
    }

    upVoteCommentClick = comment => {
      this.props.dispatch(upVoteCommentAction(comment));
      upVoteComment(comment.id).then(data => {
      }).catch(e => {
        this.props.dispatch(downVoteCommentAction(comment));
        showError(e.status, this.props.t);
      });
    }

    downVoteCommentClick = comment => {
      this.props.dispatch(downVoteCommentAction(comment));
      downVoteComment(comment.id).then(() => {
      }).catch(e => {
        this.props.dispatch(upVoteCommentAction(comment));
        showError(e.status, this.props.t);
      });
    }

    handleReply = comment => {
      this.props.dispatch(selectComment(comment));
      this.props.dispatch(selectPost({ id: comment.postId }));
      this.props.dispatch(reset('replyCommentForm'));
      this.props.navigation.navigate('ReplyComment');
    }

    handleReport = (comment) => {
      reportCall(comment.id).then(success => showMessage('SUCCESS.USER_REPORTED', this.props.t), e => showError('ERROR.ERROR', this.props.t));
    }

    deleteCommentCall (comment) {
      deleteComment(comment.id).then(sucess => {
        this.props.dispatch(deleteCommentAction(comment));
      });
    }

    handlePressMenuButton = (buttonIndex, comment, editable) => {
      if (buttonIndex === 1 && editable) {
        this.props.dispatch(selectComment(comment));
        this.props.dispatch(selectPost({ id: comment.postId }));
        this.props.dispatch(reset('newCommentForm'));
        this.props.navigation.navigate('WriteComment');
      }
      if (buttonIndex === 2 && editable) {
        this.deleteCommentCall(comment);
      }
      if (buttonIndex === 3 && !editable) {
        // should only display myComments
        // this.handleReport(comment);
      }
      if (buttonIndex === 4 && !editable) {
        // should only display myComments
        // this.blockAuthor(comment);
      }
    }

    blockAuthor = id => {
      blockAuthorCall(id).then(() => {
        this.setState({ page: 0 });
        this.getUserComments(true);
      }).catch(e => {
        showError(e.status, this.props.t);
      });
    }

    endReached = () => {
      const now = (new Date()).getTime();
      const lastPageLoadedDiff = now - (this.state.lastPageLoadedAt === null ? 0 : this.state.lastPageLoadedAt);
      if (lastPageLoadedDiff > 500 && this.state.loading === false && !this.state.endReached) {
        this.setState({ loading: true, page: this.state.page + 1 }, () => this.getUserComments(false));
      }
    };

    onRefreshingMyComments = () => {
      this.setState({ page: 0, refreshing: true }, () => this.getUserComments(true));
    };

    renderCommentItem = ({ item }) =>
      <>
        <CommentHeader
          author={this.props.user.userFullName}
          postTitle={item.postTitle} />
        <Comment
          currentUser={this.props.identity.username}
          comment={item}
          handleUpVote={this.upVoteCommentClick}
          handleDownVote={this.downVoteCommentClick}
          handleReply={this.handleReply}
          handlePressMenuButton={this.handlePressMenuButton}
          indentation={item.level}
          {...item}
        />
      </>

    render () {
      return (<FlatList
        style={styles.listBackground}
        ListHeaderComponent={
          <View style={{ height: 10 }}></View>
        }
        data={this.props.comments}
        renderItem={this.renderCommentItem}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this.onRefreshingMyComments}
          />
        }
        onEndReached={this.endReached}
        keyExtractor={(item, index) => index.toString()}
        onEndReachedThreshold={0.2}
        initialNumToRender={7}
        ListFooterComponent={this.state.loading && this.props.comments && this.props.comments.length > 0 ? <ActivityIndicator /> : null}
      />
      );
    }
}
const mapStateToProps = state => {
  return {
    identity: state.identity,
    comments: state && state.comments && state.comments.profileList && state.comments.profileList.length > 0 ? state.comments.profileList : []
  };
};
export default connect(mapStateToProps)(CommentFlatList);
