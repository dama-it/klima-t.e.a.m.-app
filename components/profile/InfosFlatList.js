import React from 'react';
import { RefreshControl } from 'react-native';
import { getSpecialAchievements } from '../../utils/restUtils';
import { connect } from 'react-redux';
import { showError } from '../Messages';
import { ScrollView } from 'react-native-gesture-handler';
import { Text, List } from 'native-base';
import { withTranslation } from 'react-i18next';
import Achievement from '../Achievement';
import moment from 'moment';
import { addProfileAchievements, setProfileAchievements } from '../../store/actions/achievements';

class InfosFlatList extends React.Component {
  state = {
    refreshing: false,
    loading: true,
    achievementsPage: 0
  };

  componentDidMount () {
    this.props.dispatch(setProfileAchievements([]));
    this.fetchMyAchievements(true);
  }

  fetchMyAchievements = (reset) => {
    getSpecialAchievements(this.state.achievementsPage, 5, this.props.user.id).then(
      success => {
        const ordered = success.data.sort(function (a, b) {
          return new Date(b.createdAt) - new Date(a.createdAt);
        });
        this.props.dispatch(reset ? setProfileAchievements(ordered) : addProfileAchievements(ordered));
        this.setState({ refreshing: false, loading: false });
      }
    ).catch(e => {
      this.setState({ refreshing: false, loading: false });
      showError(e.status, this.props.t);
    });
  };

  onEndReachedAchievements = () => {
    if (this.state.loading === false && this.state.total > this.props.achievements.length) {
      this.setState({ loading: true, achievementsPage: this.state.achievementsPage + 1 }, () => this.fetchMyAchievements(false));
    }
  };

  onRefreshingInfo = () => {
    this.setState({ achievementsPage: 0, refreshing: true }, () => this.fetchMyAchievements(true));
    this.props.refreshUser();
  };

  render () {
    const { t, user, itsMe } = this.props;
    var clubMemberInvitationCount = 0;
    var clubMemberInquiriesCount = 0;
    return (
      <ScrollView refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this.onRefreshingInfo}/>}>
        <List style={{ paddingTop: 10, paddingLeft: 10 }}>
          <Text style={{ color: '#00A19A', fontWeight: '400' }}>
            {t('screens.profileScreen.memberSince') + ':'}
          </Text>
          <Text>{moment(user.createdAt).format('L')}</Text>
          <Text style={{ paddingTop: 10, color: '#00A19A', fontWeight: '400' }}>
            {t('screens.profileScreen.badgesAndTrophies') + ':'}
          </Text>
          {this.props.achievements.map(item => <Achievement {...item}/>)}
        </List>
      </ScrollView>);
  }
}

const mapStateToProps = state => {
  return {
    identity: state.identity,
    achievements: state && state.achievements && state.achievements.profileList && state.achievements.profileList.length > 0 ? state.achievements.profileList : []
  };
};
export default connect(mapStateToProps)(withTranslation()(InfosFlatList));
