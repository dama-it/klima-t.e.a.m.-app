import React from 'react';
import { withTranslation } from 'react-i18next';
import { ActivityIndicator, RefreshControl, VirtualizedList } from 'react-native';
import { connect } from 'react-redux';
import { addProfilePosts, downVotePost, selectPost, setProfilePosts, syncPost, upVotePost } from '../../store/actions/posts';
import styles from '../../styles/styles';
import { getPost, getUserPosts } from '../../utils/restUtils';
import { downVote, upVote } from '../../utils/voteRestUtils';
import { showError } from '../Messages';
import NoContent from '../NoContent';
import ProfilePostListItem from './ProfilePostListItem';

const postPerPage = 10;

class ProfilePostAndCompetitionList extends React.Component {
  state = {
    page: 1,
    refreshing: false,
    loading: true,
    endReached: false
  };

  componentDidMount () {
    this.props.dispatch(setProfilePosts([]));
    this.fetchPosts(true);
  }

  fetchPosts = (reset) => {
    this.setState({ loading: true });
    getUserPosts(this.state.page, postPerPage, this.props.user.id, true).then(
      success => {
        this.setState({ refreshing: false, loading: false, endReached: success.data.length === 0 });
        this.props.dispatch(reset ? setProfilePosts(success.data) : addProfilePosts(success.data));
      }
    ).catch(e => {
      this.setState({ refreshing: false, loading: false });
      showError(e.status, this.props.t);
    });
  };

  refreshPost = (id) => {
    getPost(id).then(data => {
      this.props.dispatch(syncPost(data.data));
    }).catch(e => {
      showError(e.status, this.props.t);
    });
  };

  upVoteCall = (post) => {
    this.props.dispatch(upVotePost(post));
    upVote(post.id).then(() => {
    }).catch(e => {
      this.refreshPost(post.id);
      showError(e.status, this.props.t);
    });
  }

  downVoteCall = (post) => {
    this.props.dispatch(downVotePost(post));
    downVote(post.id).then(() => {
    }).catch(e => {
      this.refreshPost(post.id);
    });
  }

  selectPost = (post) => {
    this.props.dispatch(selectPost(post));
    this.props.navigation.navigate('Thread');
  }

  onRefreshingPosts = () => {
    this.props.dispatch(setProfilePosts([]));
    this.setState({ page: 0, refreshing: true }, () => this.fetchPosts(true));
  };

  onEndReachedPosts = () => {
    if (this.state.loading === false && !this.state.endReached) {
      this.setState({ loading: true, page: this.state.page + 1 }, () => this.fetchPosts(false));
    }
  };

  renderItem = ({ item }) => <ProfilePostListItem
    selectPost={this.selectPost}
    upVoteCall={this.upVoteCall}
    downVoteCall={this.downVoteCall}
    post={item}
    {...item} />

  listFooter = () => this.state.loading && this.props.posts && this.props.posts.length > 0 && <ActivityIndicator />;

  getItem = (data, index) => data[index]

  getItemCount = data => data.length;

  keyExtractor = (item, index) => index.toString();

  render () {
    return <VirtualizedList
      getItem={this.getItem}
      getItemCount={this.getItemCount}
      style={styles.listBackground}
      keyExtractor={this.keyExtractor}
      data={this.props.posts}
      renderItem={this.renderItem}
      onEndReachedThreshold={0.5}
      initialNumToRender={5}
      refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this.onRefreshingPosts}/>}
      onEndReached={this.onEndReachedPosts}
      ListFooterComponent={this.listFooter}
      ListEmptyComponent={<NoContent/>}
    />;
  }
};

const mapStateToProps = state => {
  return {
    posts: state && state.posts && state.posts.profileList && state.posts.profileList.length > 0 ? state.posts.profileList : []
  };
};

export default connect(mapStateToProps)(withTranslation()(ProfilePostAndCompetitionList));
