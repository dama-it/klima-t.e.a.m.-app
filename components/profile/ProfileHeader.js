import { MaterialIcons } from '@expo/vector-icons';
import { List, ListItem, Text, Thumbnail, View } from 'native-base';
import React from 'react';
import { withTranslation } from 'react-i18next';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { connect } from 'react-redux';
import { getUserImageSrc } from '../../utils/imageUtils';

const ProfileHeader = ({ t, user, showXps, identity }) => {
  const userIsBlocked = user && identity.blockedMessagingUsers && identity.blockedMessagingUsers.filter(u => u.id === user.id).length > 0;
  return (
    <View>
      <List>
        <ListItem avatar noBorder style={{ paddingLeft: 30, paddingBottom: 10 }}>
          <Thumbnail source={{ uri: getUserImageSrc(user ? user.userImagePath : null) }}/>
          <View style={{ flexDirection: 'column' }}>
            <Text style={{ paddingLeft: 10 }}>
              {user ? user.userFullName : null}
              {' '}
              {userIsBlocked ? <MaterialIcons size={14} name="block" /> : null}
            </Text>
            {
              user && <TouchableOpacity onPress={showXps}>
                <Text note style={{ paddingLeft: 10 }}>
                  { user.xp + t('screens.profileScreen.XP') + ' | ' + ((user && user.status) || t('ranks.novice'))}
                </Text>
              </TouchableOpacity>
            }
          </View>
        </ListItem>
      </List>
    </View>
  );
};
const mapStateToProps = state => {
  return {
    identity: state.identity
  };
};
export default connect(mapStateToProps)(withTranslation()(ProfileHeader));
