// React imports
import { CardItem, Icon, Left, Right, Text, View } from 'native-base';
import React from 'react';
import { TouchableOpacity } from 'react-native-gesture-handler';
// Style imports
import styles from '../../styles/styles';

class ProfilePostActions extends React.PureComponent {
  handleUpVote = () => {
    if (this.props.post.currentUsersVote === false || this.props.post.currentUsersVote === null) {
      this.props.upVoteCall(this.props.post);
    }
  }

  handleDownVote = () => {
    if (this.props.post.currentUsersVote === true || this.props.post.currentUsersVote === null) {
      this.props.downVoteCall(this.props.post);
    }
  }

  setAsFavourite = () => {
    this.props.setAsFavourite(this.props.post);
  }

  render () {
    const { post, setAsFavourite, downVoteCall, upVoteCall } = this.props;
    return (
      <CardItem style={{
        paddingTop: 0
      }}>
        <Left>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flexDirection: 'row', paddingRight: 15 }}>
              <TouchableOpacity onPress={this.handleUpVote} style={{ flexDirection: 'row', width: 25, justifyContent: 'center' }}>
                {post.currentUsersVote === true
                  ? (<Icon name="md-arrow-round-up" style={styles.active} />)
                  : (<Icon name="ios-arrow-round-up" style={styles.threadTools} />)}
              </TouchableOpacity>
              <View style={{ width: 25 }}>
                <Text style={{ color: '#808080', textAlign: 'center', paddingTop: 2 }}>
                  {post.upVotesCount - post.downVotesCount}
                </Text>
              </View>
              <TouchableOpacity onPress={this.handleDownVote} style={{ flexDirection: 'row', width: 25, justifyContent: 'center' }}>
                {post.currentUsersVote === false
                  ? (<Icon name="md-arrow-round-down" style={styles.active } />)
                  : (<Icon name="ios-arrow-round-down" style={styles.threadTools} />)}
              </TouchableOpacity>
            </View>
            <View >
              <TouchableOpacity onPress={this.setAsFavourite}>
                <Icon name="ios-heart" style={{ ...styles.threadTools, ...(post.isFavourite ? styles.active : styles.threadTools) }} />
              </TouchableOpacity>
            </View>
          </View>
        </Left>
        <Right>

          <View style={{ flexDirection: 'row', alignItems: 'flex-end' }}>

            <Icon name="md-mail" style={styles.threadTools} />
            <Text style={{ ...styles.threadTools, marginLeft: 5, justifyContent: 'center', paddingBottom: 4 }}>
              {post.commentsCount}
            </Text>
            <TouchableOpacity>
              <Icon name="md-share" style={{ ...styles.threadTools, marginLeft: 10 }} />
            </TouchableOpacity>
            {post.isPinned
              ? <View style={{ flex: 1, alignItems: 'flex-end' }}>
                <TouchableOpacity>
                  <Text>P</Text>
                </TouchableOpacity>
              </View> : null}

          </View>

        </Right>
      </CardItem>
    );
  }
}
export default ProfilePostActions;
