import React from 'react';
import { withTranslation } from 'react-i18next';
import ProfileCompetitionList from './ProfileCompetitionList';
import ProfilePostAndCompetitionListHeader from './ProfilePostAndCompetitionListHeader';
import ProfilePostList from './ProfilePostList';

class ProfilePostAndCompetitionList extends React.Component {
  state = {
    isCompetitionView: false
  };

  setPostView = () => {
    this.setState({ isCompetitionView: false });
  };

  setCompetitionView = () => {
    this.setState({ isCompetitionView: true });
  };

  render () {
    return <>
      <ProfilePostAndCompetitionListHeader
        isCompetitionView={this.state.isCompetitionView}
        setPostView={this.setPostView}
        setCompetitionView={this.setCompetitionView}
      />
      {
        this.state.isCompetitionView
          ? <ProfileCompetitionList
            user={this.props.user}
            navigation={this.props.navigation}
          />
          : <ProfilePostList
            user={this.props.user}
            navigation={this.props.navigation}
          />
      }
    </>;
  }
};

export default withTranslation()(ProfilePostAndCompetitionList);
