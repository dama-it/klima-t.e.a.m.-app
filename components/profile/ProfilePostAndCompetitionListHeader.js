import { Button, Segment, Text } from 'native-base';
import React from 'react';
import { withTranslation } from 'react-i18next';
import styles from '../../styles/styles';

const TopicListHeader = ({ t, isCompetitionView, setPostView, setCompetitionView }) => {
  return (
    <>
      <Segment style={styles.listHeaderSegment}>
        <Button first onPress={setPostView} active={!isCompetitionView} style={styles.listHeaderSegmentButton}>
          <Text style={ !isCompetitionView ? styles.listHeaderSegmentButtonTextActive : styles.listHeaderSegmentButtonTextInActive }>
            {t('screens.topic.posts')}
          </Text>
        </Button>
        <Button last onPress={setCompetitionView} active={isCompetitionView} style={styles.listHeaderSegmentButton}>
          <Text style={isCompetitionView ? styles.listHeaderSegmentButtonTextActive : styles.listHeaderSegmentButtonTextInActive }>
            {t('screens.topic.challenges')}
          </Text>
        </Button>
      </Segment>
    </>
  );
};

export default withTranslation()(TopicListHeader);
