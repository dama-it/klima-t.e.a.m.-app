// React imports
import { Card, CardItem, Left, Right, H1, Thumbnail, Body, Icon, View } from 'native-base';
import React from 'react';
import { TouchableOpacity } from 'react-native-gesture-handler';
import styles from '../../styles/styles';
import { getBannerSrc, getIconSrc } from '../../utils/imageUtils';
import PostActions from '../PostActions';
import PostCreatedBy from '../post/PostCreatedBy';
import { Image } from 'react-native';
import { selectTopicScreen, selectTopic as selectTopicAction } from '../../store/actions/topics';
import { connect } from 'react-redux';
import { useNavigation } from '@react-navigation/core';

const HidenPostMarker = ({ post }) => {
  return post.active ? null : <Icon style={{ color: '#01A19A' }} type="Ionicons" name="ios-eye-off" />;
};

const ProfilePostListItem = ({ dispatch, post, selectPost, downVoteCall, upVoteCall }) => {
  const navigation = useNavigation();

  const iconSrc = () => {
    return { uri: getIconSrc(post.topic) };
  };

  const bannerSrc = () => {
    return { uri: getBannerSrc(post) };
  };

  const selectTopic = () => {
    dispatch(selectTopicAction(post.topic));
    dispatch(selectTopicScreen(true));
    navigation.navigate('Favourites', { screen: 'TopicDetailScreen' });
  };

  return (
    <Card style={styles.cardListContainer}>
      <CardItem header style={{ paddingBottom: 0 }}>
        <Left>
          <TouchableOpacity onPress={selectTopic}>
            <Thumbnail
              style={{ width: 28, height: 28 }}
              small
              source={iconSrc()}
            />
          </TouchableOpacity>
          <Body>
            <PostCreatedBy post={post} selectTopic={selectTopic}/>
          </Body>
        </Left>
        <Right>
          <HidenPostMarker post={post} />
        </Right>
      </CardItem>
      <TouchableOpacity onPress={() => selectPost(post)}>
        <View style={styles.postImageContainer}>
          <View style={styles.postTitleContainer}>
            <H1 onPress={() => selectPost(post)} style={styles.selectPostListContent}>
              {post.title ? post.title : (post.competition ? post.competition.goal : '')}
            </H1>
          </View>
          {
            post.banner && <View style={styles.imageListViewParentContainer}>
              <Image
                source={bannerSrc}
                style={styles.imageListViewStyle}
              />
            </View>
          }
        </View>
      </TouchableOpacity>
      <PostActions
        post={post}
        selectPost={() => selectPost(post)}
        downVoteCall={downVoteCall}
        upVoteCall={upVoteCall}
        {...post}
      />
    </Card>);
};

export default connect()(ProfilePostListItem);
