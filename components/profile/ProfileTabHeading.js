import React from 'react';
import { Text } from 'react-native';
import TabHeading from '../../themes/native-base-theme/components/TabHeading';

const ProfileTabHeading = (props) =>
  <TabHeading
    style={{
      backgroundColor: 'red',
      borderBottomColor: '#00A19A'
    }}
  >
    <Text>{props.title}</Text>
  </TabHeading>;

export default ProfileTabHeading;
