// React imports
import React from 'react';

// Native Base imports
import { CheckBox, Text, Row, List, Col } from 'native-base';

// Other imports
// import { useTranslation } from 'react-i18next';

import styles from '../../styles/styles';

// const { t } = useTranslation();

export const renderCheckBox = ({
  input,
  checked,
  label,
  meta: { touched, error },
  children,
  touch,
  ...custom
}) => (
  <List>
    <Row style={styles.checkBoxRow}>
      <Col style={styles.checkBoxCol} >
        <CheckBox
          checked={input.value === true}
          onPress={() => {
            input.value ? (input.value = false) : (input.value = true);
            input.onChange(input.value);
            touch();
          }}
          {...custom}
          color="#135182"
        />
      </Col>
      <Col style={styles.checkBoxLabel}>
        <Text>{label}</Text>
      </Col>
    </Row>
    <Row>
      <Text style={styles.errorLabel}>{error && touched ? (error) : null}</Text>
    </Row>
  </List>
);
