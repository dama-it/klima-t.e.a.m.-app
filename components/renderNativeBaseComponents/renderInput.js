import React from 'react';
import { Input, Item, Icon, Text, Label } from 'native-base';
import styles from '../../styles/styles';

export const renderInput = ({
  input: { onChange, ...restInput },
  meta: { touched, invalid, error },
  disabled,
  label,
  multiline
}) => {
  return (
    <>
      <Item success={!invalid && touched} error={invalid && touched} floatingLabel style={styles.registerInput} >
        <Label>{label}</Label>
        <Input
          onChangeText={onChange}
          disabled={disabled}
          {...restInput}
          multiline={multiline}
        />
        {touched ? error ? <Icon name="close-circle" /> : <Icon name="checkmark-circle" /> : null}
      </Item>
      <Text style={styles.errorLabel}>{invalid && touched ? (error) : null}</Text>
    </>
  );
};
