import React from 'react';
import { Input, Item, Icon, Text, Label } from 'native-base';
import styles from '../../styles/styles';

export const renderPasswordInput = ({
  input: { onChange, ...restInput },
  meta: { touched, invalid, error },
  label
}) => {
  return (
    <>
      <Item success={!invalid && touched} error={invalid && touched} style={styles.registerInput} floatingLabel >
        <Label>{label}</Label>
        <Input secureTextEntry onChangeText={onChange} {...restInput} />
        {touched ? error ? <Icon name="close-circle" /> : <Icon name="checkmark-circle" /> : null}
      </Item>
      <Text style={styles.errorLabel}>{invalid && touched ? (error) : null}</Text>
    </>
  );
};
