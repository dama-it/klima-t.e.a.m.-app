import { Navigation } from "react-native-navigation";

import LoginScreen from "./screens/LoginScreen";
import HomeScreen from "./screens/HomeScreen";
import OsnovniPodatki from './screens/OsnovniPodatki';
import Materiali from './screens/Materiali';
import Slike from './screens/Slike';
import Opombe from './screens/Opombe';
import Podpis from './screens/Podpis';
import SplashScreen from './screens/SplashScreen';
import PodpisIzvajalca from './screens/PodpisIzvajalca';
import Soglasje from './screens/Soglasje';
import CustomerAnnotations from './screens/CustomerAnnotations';
import additionalInfo from './screens/additionalInfo';


Navigation.registerComponent('LoginScreen', () => LoginScreen);
Navigation.registerComponent('HomeScreen', () => HomeScreen);
Navigation.registerComponent('SettingsScreen', () => SettingsScreen);
Navigation.registerComponent('OsnovniPodatki', () => OsnovniPodatki);
Navigation.registerComponent('Materiali', () => Materiali);
Navigation.registerComponent('RegisterScreen', () => RegisterScreen);
Navigation.registerComponent('EditProfileScreen', () => EditProfileScreen);
Navigation.registerComponent('ChangePasswordScreen', () => ChangePasswordScreen);
Navigation.registerComponent('Slike', () => Slike);
Navigation.registerComponent('Opombe', () => Opombe);
Navigation.registerComponent('ForgotPasswordScreenThree', () => ForgotPasswordScreenThree);
Navigation.registerComponent('SearchUserScreen', () => SearchUserScreen);
Navigation.registerComponent('CalibrationPicker', () => CalibrationPicker);
Navigation.registerComponent('Podpis', () => Podpis);
Navigation.registerComponent('SplashScreen', () => SplashScreen);
Navigation.registerComponent('UserRegistrationDataScreen', () => UserRegistrationDataScreen);
Navigation.registerComponent('PlayScreen', () => PlayScreen);
Navigation.registerComponent('CommentScreen', () => CommentScreen);
Navigation.registerComponent('FollowersScreen', () => FollowersScreen);
Navigation.registerComponent('ProfileScreenFollower', () => ProfileScreenFollower);
Navigation.registerComponent('MembershipSelectorScreen', () => MembershipSelectorScreen);
Navigation.registerComponent('PodpisIzvajalca', () => PodpisIzvajalca);
Navigation.registerComponent('Soglasje', () => Soglasje);
Navigation.registerComponent('CustomerAnnotations', () => CustomerAnnotations);
Navigation.registerComponent('additionalInfo', () => additionalInfo);



Navigation.events().registerAppLaunchedListener(async () => {
  Navigation.setRoot({
    root: {

      stack: {
        children: [
          {
            component: {
              name: 'SplashScreen',
              options: {
                topBar: {
                  visible: false,
                }
              }
            }
          }
        ]
      }
    }
  });
});


