import { Ionicons } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { useNavigation } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import { connect } from 'react-redux';
import HomeScreen from '../screens/HomeScreen';
import ProfileScreen from '../screens/ProfileScreen';
const styles = { tabBarIcon: { backgroundColor: 'transparent', color: '#333333' } };

const tabBarIcon = (name, nameFocused, style) => ({ color, focused }) => (
  <Ionicons
    name={focused ? nameFocused : name}
    color={color}
    size={24}
  />
);

const MainAppNavigator = createStackNavigator();

export function AppNavigator ({ loggedIn }) {
  return (
    <MainAppNavigator.Navigator
      initialRoute="Login"
      headerMode="none">
      {loggedIn
        ? <MainAppNavigator.Screen
          name="Home"
          component={AppDrawer}
        />
        : <>
          <MainAppNavigator.Screen
            name="Login"
            component={LoginScreen} />
          <MainAppNavigator.Screen
            name="Register"
            component={RegisterScreen}
            noHeader
          />
          <MainAppNavigator.Screen
            name="ConfirmRegistration"
            component={ConfirmRegistrationScreen}
          />
          <MainAppNavigator.Screen
            name="ForgotPassword"
            component={ForgotPasswordScreen}
          />
          <MainAppNavigator.Screen
            name="PasswordReseted"
            component={PasswordResetedScreen}
          />
        </>
      }
    </MainAppNavigator.Navigator>
  );
}

const Drawer = createDrawerNavigator();

function AppDrawer () {
  return (
    <Drawer.Navigator >
      <Drawer.Screen name="Home" component={HomeScreen} />
      <Drawer.Screen name="Profile" component={ProfileScreen} />
    </Drawer.Navigator>
  );
}

const Tab = createBottomTabNavigator();

function HomeBottomTabNavigator () {
  const navigation = useNavigation();
  return (
    <>
      <Tab.Navigator
        initialRouteName="Home"
        backBehavior="history"
        tabBarOptions={{
          showLabel: false,
          activeTintColor: '#01A19A',
          inactiveTintColor: '#333333',
          style: { backgroundColor: 'rgba(255,255,255,1.0)', opacity: 1.0, position: 'absolute', elevation: 0 }
        }}
      >
        <Tab.Screen
          options={{
            tabBarLabel: 'Home',
            tabBarIcon: tabBarIcon('md-home', 'md-home')
          }}
          name="Home"
          component={HomeScreen}
        />
        <Tab.Screen
          options={{
            tabBarLabel: 'Profile',
            tabBarIcon: tabBarIcon('ios-star', 'ios-star')
          }}
          name="Profile"
          component={TopicStack}
        />
      </Tab.Navigator>
    </>
  );
}


const mapStateToProps = state => {
  return (
    { loggedIn: state.identity && state.identity.username && state.identity.username !== '' }
  );
};

export default connect(mapStateToProps)(AppNavigator);
