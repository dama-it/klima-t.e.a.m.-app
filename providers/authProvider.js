import {
  AUTH_LOGIN,
  AUTH_LOGOUT,
  AUTH_ERROR,
  AUTH_CHECK,
  AUTH_GET_PERMISSIONS
} from './types';
import qs from 'qs';
import { AsyncStorage } from 'react-native';
import { HOST, API } from '../config/config';


const invalidateData = () => {
  AsyncStorage.removeItem('id');
  AsyncStorage.removeItem('userFullName');
  AsyncStorage.removeItem('token');
  AsyncStorage.removeItem('username');
  AsyncStorage.removeItem('userEmail');
  AsyncStorage.removeItem('userFirstName');
  AsyncStorage.removeItem('userLastName');
}

export default (type, params) => {


  // called when the user attempts to log in
  if (type === AUTH_LOGIN) {
    console.log("AUTH_LOGIN");
    const { username, password } = params;
    const request = new Request(HOST + API + '/login', {
      method: 'POST',
      // formData: {"username": username, "password": password},
      // body: JSON.stringify({ username, password }),
      body: qs.stringify({
        username: username,
        password: password
      }),
      headers: new Headers({
        'Content-Type': 'application/x-www-form-urlencoded'
      })
    });
    return fetch(request).then(response => {
      
      console.log("Response:",response.status);
      if (response.status < 200 || response.status >= 300) {
        console.log(response.statusText);
        throw new Error(response.statusText);
      }
      // get user data
      
      console.log("LOGGED IN");
      const token = response.headers.get('authorization');
      AsyncStorage.setItem('token', token || '');
      console.log(token);

      const identityRequest = new Request(HOST + API + '/identity', {
        method: 'GET',
        headers: new Headers({
          'Content-Type': 'application/json',
          authorization: token
        })
      });

      return fetch(identityRequest)
        .then(response => response.json())
        .then(json => {
          console.log("Identity");
          if (response.status < 200 || response.status >= 300) {
            console.log(response.statusText);
            AsyncStorage.removeItem('token');
            throw new Error(response.statusText);
          }
          if (json.identity) {
            console.log(json.identity.username);
            AsyncStorage.setItem('id', json.identity.id ? json.identity.id.toString() : '0');
            AsyncStorage.setItem('username', json.identity.username ? json.identity.username : '');
            AsyncStorage.setItem('userFullName', json.identity.userFullName ? json.identity.userFullName : '');
            
          }
          console.log("OK!");
          return Promise.resolve({
            userData: {
              id: json.identity.id,
              username: json.identity.username,
            }
          });
        });
    });
  }
  // called when the user clicks on the logout button
  if (type === AUTH_LOGOUT) {
    invalidateData();
    return Promise.resolve();
  }
  // called when the API returns an error
  if (type === AUTH_ERROR) {
    const { status } = params;
    if (status === 401 || status === 403) {
      invalidateData();
      return Promise.reject(Error('ERROR.AUTH_ERROR'));
    }
    return Promise.resolve();
  }
  // called when the user navigates to a new location
  if (type === AUTH_CHECK) {
    return AsyncStorage.getItem('token') ? Promise.resolve() : Promise.reject(Error('ERROR.AUTH_ERROR'));
  }


  return Promise.reject(Error('ERROR.AUTH_ERROR'));
};
