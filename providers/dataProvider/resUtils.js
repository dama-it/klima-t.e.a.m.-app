import restClient from './rest';
import { CREATE, DELETE, FAVOURITE, GET_ALL, GET_LIST, GET_ONE, REGISTER, UPDATE, DOWNLOAD } from '../types';

export const getPosts = (page, perPage, field, order, filter) => {
  return new Promise((resolve, reject) => {
    console.log("tu sem");
    restClient(GET_LIST, 'posts', {
      pagination: { page: page + 1, perPage: perPage },
      sort: { field: field, order: order }, 
      filter: filter
     
    })
      .then(data => {
        console.log(data);
        if (data.status && data.code !== 200) {
          reject(data);
        }
        resolve(data);
      })
      .catch(e => {
        reject(e);
        console.log(e);
      });
  });
};
