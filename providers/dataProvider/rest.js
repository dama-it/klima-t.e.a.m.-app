import springJsonServer from './springJsonServer';
import { HOST, API } from '../../config/config';

const restClient = springJsonServer(HOST + API);

export default (type, resource, params) =>
  new Promise(resolve =>
    setTimeout(() => resolve(restClient(type, resource, params)), 500)
  );


