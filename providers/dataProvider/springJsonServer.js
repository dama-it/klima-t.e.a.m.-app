import { stringify } from 'query-string';
import { fetchJson, flattenObject } from './utils/fetch';
import { AsyncStorage } from 'react-native';
import {
  GET_LIST,
  GET_ONE,
  GET_MANY,
  GET_MANY_REFERENCE,
  CREATE,
  UPDATE,
  DELETE,
  DELETE_MANY,
  AUTH_LOGOUT,
  REGISTER,
  GET_ALL
} from '../types';
// import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import store from '../../store/configureStore';
import { updateError } from '../../store/actions';
import authProvider from '../../providers/authProvider';

/**
 * Maps admin-on-rest queries to a json-server powered REST API
 *
 * @see https://github.com/typicode/json-server
 * @example
 * GET_LIST     => GET http://my.api.url/posts?sort=title,asc&page=0&size=20
 * GET_ONE      => GET http://my.api.url/posts/123
 * GET_MANY     => GET http://my.api.url/posts/123, GET http://my.api.url/posts/456, GET http://my.api.url/posts/789
 * UPDATE       => PUT http://my.api.url/posts/123
 * CREATE       => POST http://my.api.url/posts/123
 * DELETE       => DELETE http://my.api.url/posts/123
 */

export default (apiUrl, httpClient = fetchJson, dispatch) => {
  /**
   * @param {String} type One of the constants appearing at the top if this file, e.g. 'UPDATE'
   * @param {String} resource Name of the resource to fetch, e.g. 'posts'
   * @param {Object} params The REST request params, depending on the type
   * @returns {Object} { url, options } The HTTP request parameters
   */
  const convertRESTRequestToHTTP = async (type, resource, params) => {
    const token = await AsyncStorage.getItem('token');
    let url = '';
    const options = {
      headers: new Headers({
        Accept: 'application/json',
        authorization: token
      })
    };

    switch (type) {
      case GET_LIST: {
        const { page, perPage } = params.pagination;
        const { field, order } = params.sort;
        const query = {
          ...flattenObject(params.filter),
          sort: field + ',' + order.toLowerCase(),
          page: page - 1,
          size: perPage
        };
        options.method = 'GET';
        url = `${apiUrl}/${resource}?${stringify(query)}`;
        break;
      }
      case GET_ALL: {
        const query = {
          ...flattenObject(params.filter)
        };
        options.method = 'GET';
        url = `${apiUrl}/${resource}?${stringify(query)}`;

        break;
      }
      case GET_ONE:
        if (typeof params !== 'undefined') {
          url = `${apiUrl}/${resource}/${params.id}`;
        } else {
          // hack to get stuff like /api/dashboard to work
          url = `${apiUrl}/${resource}`;
        }
        break;
      case GET_MANY: {
        url = `${apiUrl}/${resource}/many`;
        options.method = 'POST';
        options.body = JSON.stringify(params.ids);
        break;
      }
      case GET_MANY_REFERENCE: {
        const { page, perPage } = params.pagination;
        const { field, order } = params.sort;
        const query = {
          ...flattenObject(params.filter),
          [params.target]: params.id,
          sort: field + ',' + order.toLowerCase(),
          page: page - 1,
          size: perPage
        };
        url = `${apiUrl}/${resource}?${stringify(query)}`;
        break;
      }
      case UPDATE:
        url = `${apiUrl}/${resource}/${params.id}`;
        options.method = 'PUT';
        options.body = JSON.stringify(params.data);
        break;
      case CREATE:
        url = `${apiUrl}/${resource}`;
        options.method = 'POST';
        options.body = JSON.stringify(params.data);
        break;
      case REGISTER:
        url = `${apiUrl}/${resource}`;
        options.method = 'POST';
        options.body = JSON.stringify(params.data);
        options.headers = null;
        break;
      case DELETE_MANY:
        url = `${apiUrl}/${resource}`;
        options.body = JSON.stringify(params.ids);
        options.method = 'DELETE';
        break;
      case DELETE:
        url = `${apiUrl}/${resource}/${params.id}`;
        options.body = JSON.stringify(params.data);
        options.method = 'DELETE';
        break;
      case 'UPLOAD':
        url = `${apiUrl}/${resource}`;
        options.body = params;
        options.headers.append('content-type', 'multipart/form-data');
        options.method = 'POST';
        break;
      case 'FAVOURITE':
        url = `${apiUrl}/${resource}/${params.id}`;
        options.method = 'POST';
        options.body = JSON.stringify(params.data);
        break;

      case 'DOWNLOAD':
        url = `${apiUrl}/${resource}/${params.id}`;
        options.headers = new Headers({
          authorization: token
        });
        options.method = 'GET';
        break;
      default:
        throw new Error(`Unsupported fetch action type ${type}`);
    }
    
    console.log(url);
    return { url, options };
  };

  /**
   * @param {Object} response HTTP response from fetch()
   * @param {String} type One of the constants appearing at the top if this file, e.g. 'UPDATE'
   * @param {String} resource Name of the resource to fetch, e.g. 'posts'
   * @param {Object} params The REST request params, depending on the type
   * @returns {Object} REST response
   */
  const convertHTTPResponseToREST = (response, type, resource, params) => {
    const token = response.headers.get('authorization');
    AsyncStorage.setItem('token', token || '');

    const { json } = response;

    switch (type) {
      case GET_LIST:
      case GET_MANY_REFERENCE:
        if (typeof json.totalElements === 'undefined') {
          throw new Error('Missing totalElements from json response');
        }
        return {
          data: json.content,
          total: json.totalElements
        };
      case CREATE:
        return { data: { ...json } };
      case 'DOWNLOAD':
        return response;
      default:
        return { data: json };
    }
  };

  /**
   * @param {string} type Request type, e.g GET_LIST
   * @param {string} resource Resource name, e.g. "posts"
   * @param {Object} payload Request parameters. Depends on the request type
   * @returns {Promise} the Promise for a REST response
   */
  return async (type, resource, params) => {
    const { url, options } = await convertRESTRequestToHTTP(
      type,
      resource,
      params
    );

    if (type === 'DOWNLOAD') {
      return fetch(url, options).then(response =>
        convertHTTPResponseToREST(response, type, resource, params)
      );
    }

    return httpClient(url, options)
      .then(response => {
        store.dispatch(updateError({ code: 200, status: 'OK' }));
        return convertHTTPResponseToREST(response, type, resource, params);
      })
      .catch(error => {
        // const { t } = useTranslation();
        store.dispatch(
          updateError({ code: 400, status: error.message })
        );

        if (error.message === 'ERROR.USER_DOES_NOT_EXIST' ||
          error.message === 'ERROR.ACCESS_DENIED' ||
          error.message === 'ERROR.SIGNATURE_INVALID' ||
          error.message === 'ERROR.TOKEN_INVALID' ||
          error.message === 'ERROR.SIGNATURE_EXPIRED' ||
          error.message === 'ERROR.TOKEN_EXPIRED'

        ) {
          authProvider(AUTH_LOGOUT, this.state);
          Navigation.push(params.componentId, {
            component: {
              name: 'LoginScreen',
              options: {
                topBar: {
                  visible: false,
                  title: {
                    text: 'LoginScreen',

                    }
                  }
                }
              }
            }
          );
        }
        // dispatch(updateError({ status: I18n.t(error.message), code: 400 }));
        // return { status: t(error.message), code: 400 };
        return { status: (error.message), code: 400 };
      });
  };
};
