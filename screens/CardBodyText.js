import React from 'react';
import StyleSheet from '../Styles/Style';
import PropTypes from 'prop-types';
import { propTypes } from 'redux-form';
import {
    Text,
} from 'react-native';
import { TouchableHighlight, View } from 'react-native';


class CardBodyText extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            long: false,
            value: 0,
            pressed: true

        };
    }
    render() {
        return (
            <>
                {((this.props.text).length > 55) ?
                    <>
                        {((this.state.pressed)) ?
                            <>
                                <View style={StyleSheet.commentTextWrap}>
                                    <Text>{(((this.props.text).substring(0, 55 - 3)) + ' ')}</Text>
                                    <TouchableHighlight
                                        style={StyleSheet.commentButton}
                                        activeOpacity={1}
                                        underlayColor="#fff"
                                        onPress={() => {
                                            this.setState({ pressed: false })
                                        }}>
                                        <Text style={StyleSheet.commentText}>more...</Text>
                                    </TouchableHighlight>
                                </View>
                            </>
                            :
                            <Text>{this.props.text}</Text>}
                    </>
                    :
                    <Text>{this.props.text}</Text>}
            </>
        );
    }
}

CardBodyText.propTypes = {
    ...propTypes,
    text: PropTypes.string.isRequired,
};
export default CardBodyText;