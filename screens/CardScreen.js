import { Card, CardTitle, CardContent, CardAction, CardButton, CardImage, ScrollView } from 'react-native-cards';
import React from 'react';
import StyleSheet from '../Styles/Style';
import PropTypes from 'prop-types';
import { propTypes } from 'redux-form';
import {
  AsyncStorage,
  View,
  Text,
  Picker,
  Image,
  TouchableHighlight,
} from 'react-native';
import CardBodyText from './CardBodyText';
import { Navigation } from "react-native-navigation";
import Dialog from "react-native-dialog";

class Cardscreen extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      likes: [],
      value: 0,
      liked: false,
      comments: [],
      comment: "",
      userName: "",
      monter:""
    };
  }

  setParam = (a) => {
    this.setState({ monter: a });
    
  };

  showDialog = () => {
    this.setState({ dialogOpen: true });
  };

  handleCancel = () => {
    this.setState({ dialogOpen: false });
  };

  async componentDidMount() {
    let username = await AsyncStorage.getItem('userFullName');
    this.setState({ username });
  }
  saveThisComment() {
    let commentsTmp = this.state.comments;
    console.log(commentsTmp);
    commentsTmp.push({ user: this.state.username, text: this.state.comment });
    this.setState({ comments: commentsTmp })
    console.log(this.state.comments);
  }
  setThisComment(e) {
    this.setState({ comment: e });
  }
  increaseValue() {
    console.log(this.state.liked)
    if (!this.state.liked) {
      this.setState({ liked: !this.state.liked })
      newValue = this.state.value + 1;
      this.setState({ value: newValue })
    }
    else {
      this.setState({ liked: !this.state.liked })
      newValue = this.state.value - 1;
      this.setState({ value: newValue })
    }
  }
  render() {
    return (
      <>
        <View style={StyleSheet.cardMargin}>
          <Card>
            <View style={StyleSheet.cardBodyWrap}>
              <Text style={StyleSheet.headerText}>DAMA IT d.o.o.</Text>
              <Text style={{ color: "green", marginLeft: 10, marginTop: 1 }}>Odprto</Text>
              <TouchableHighlight
                activeOpacity={1}
                underlayColor="#ffffff"
                onPress={() => Navigation.push(this.props.props.componentId, {
                  component: {
                    name: 'OsnovniPodatki',
                    options: {
                      topBar: {
                        visible: false,
                        title: {
                          text: 'OsnovniPodatki',

                        }
                      }
                    }
                  }
                })}>
                <Text style={StyleSheet.viewMoreCommentsText}>ODPRI</Text>
              </TouchableHighlight>
            </View>
            <View style={{ marginLeft: 10 }}>
              <Text style={{ color: "grey" }}>Jezdarska ", 2000 Maribor</Text>
            </View>
            <View style={{ marginLeft: 10 }}>
              <Text style={{ color: "grey" }}>22.03.2022</Text>
            </View>
            <View style={{ marginLeft: 10, marginBottom: 10 }}>
              <CardBodyText props={this.props} text={this.props.cardInfo.body} />
            </View>
            <View style={{ marginLeft: 10, marginBottom: 10 }}>
              <View style={{ flexDirection: "row",  marginBottom: 20 }}>
                <TouchableHighlight
                  onPress={this.showDialog}>
                  <Text style={{ color:"#ff9933" }}>ZAMENJAJ MONTERJA</Text>
                </TouchableHighlight>
                <Dialog.Container visible={this.state.dialogOpen} style={{ width: "90%", height: "80%" }}>
                  <Dialog.Title>Spremeni datum</Dialog.Title>
                  <Dialog.Description>
                    <Picker
                      selectedValue={this.monter}
                      style={{ width: 170 }}
                      onValueChange={(a) => this.setParam(a)}
                    >
                      <Picker.Item label="Monter1" value="Monter1" />
                      <Picker.Item label="Monter2" value="Monter1" />
                      <Picker.Item label="Monter3" value="Monter1" />
                      <Picker.Item label="Monter4" value="Monter1" />
                      <Picker.Item label="Monter5" value="Monter1" />
                      <Picker.Item label="Monter6" value="Monter1" />
                    </Picker>
                  </Dialog.Description>
                  <Dialog.Button label="Zapri" onPress={this.handleCancel} />

                </Dialog.Container>
              </View>

            </View>
            <CardAction
              separator={true}
              inColumn={false}>
            </CardAction>
          </Card>
        </View>
      </>
    );
  }
}
Cardscreen.propTypes = {
  ...propTypes,
  cardInfo: PropTypes.object.isRequired,
};
export default Cardscreen;