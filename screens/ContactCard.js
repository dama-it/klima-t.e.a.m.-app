import { Card, CardTitle, CardContent, CardAction, CardButton, CardImage, ScrollView } from 'react-native-cards';
import React from 'react';
import StyleSheet from '../Styles/Style';

const ContactCard = (parent) => {
  return (


    <Card style={StyleSheet.contactCard}>
      <CardTitle
        subtitle="Mojca Novak"
      />
      <CardButton style={StyleSheet.scrollViewUserAddButton}
        onPress={() => { }}
        title="ADD"
        color="#8600b3"
      />
    </Card>


  );
}

export default ContactCard