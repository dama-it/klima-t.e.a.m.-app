import React from 'react';
import {
  View,
  Image,
  TouchableHighlight,
  Text

} from 'react-native';
import StyleSheet from '../Styles/Style';
import { Navigation } from "react-native-navigation";


const DamaButton = (parent) => {
  return (
    <>
      <View style={StyleSheet.damaButtonWrap}>
        <TouchableHighlight style={StyleSheet.damaButton}
          onPress={() => Navigation.push(parent.props.componentId, {
            component: {
              name: parent.link,
              options: {
                topBar: {
                  visible: false,
                  title: {
                    text: parent.link,
                  }
                }
              }
            }
          })}>
          <Text style={StyleSheet.damaButtonText}>{parent.title}</Text>
        </TouchableHighlight>
      </View>

    </>
  );
};

export default DamaButton


