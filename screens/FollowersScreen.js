import { Card, CardTitle, CardContent, CardAction, CardButton, CardImage, ScrollView } from 'react-native-cards';
import React from 'react';
import StyleSheet from '../Styles/Style';
import PropTypes from 'prop-types';
import { propTypes } from 'redux-form';
import { CommentsScreen } from "./CommentsScreen";
import {
  SafeAreaView,
  AsyncStorage,
  View,
  Text,
  StatusBar,
  Image,
  TextInput,
  type,
  Pressable,
  onPress,
  title,
  Button,
  TouchableHighlight,
  useEffect,
  FlatList

} from 'react-native';
import CardBodyText from './CardBodyText';
import Header from './Header';
import Footer from './Footer';
import CardScreen from './CardScreen';
import { Navigation } from "react-native-navigation";
import axios from 'axios';


class CommentScreen extends React.Component {


  state = {
    likes: [],
    value: 0,
    liked: false,
    comments: [],
    comment: "",
    userName: "",

  };

  constructor(props) {
    super(props)
  }

  async componentDidMount() {
    let username = await AsyncStorage.getItem('userFullName');
    this.setState({ username });
  }

  saveThisComment() {
    let commentsTmp = this.state.comments;
    console.log(commentsTmp);
    commentsTmp.push({ user: this.state.username, text: this.state.comment, id: this.state.comments.length + 1 });
    this.setState({ comments: commentsTmp })
    console.log(this.state.comments);
  }
  setThisComment(e) {
    this.setState({ comment: e });
  }
  increaseValue() {
    console.log(this.state.liked)
    if (!this.state.liked) {
      this.setState({ liked: !this.state.liked })
      newValue = this.state.value + 1;
      this.setState({ value: newValue })
    }
    else {
      this.setState({ liked: !this.state.liked })
      newValue = this.state.value - 1;
      this.setState({ value: newValue })
    }
  }

  render() {
    return (
      <>
        <View style={StyleSheet.background}>
          <Header props={this.props} title={"logo"} location={"HomeScreen"} />
          <View style={StyleSheet.searchFollowersWrap}>
            <TextInput
              style={StyleSheet.commentInput}
              onChangeText={(e) => this.setThisComment(e)}
              defaultValue={this.state.comments}
              placeholder="Look for followers"
            />
            <TouchableHighlight
              style={StyleSheet.commentButton}
              activeOpacity={1}
              underlayColor="#fff"
              onPress={() => {
                this.saveThisComment()
              }}>
              <Text style={StyleSheet.commentText}>Search</Text>
            </TouchableHighlight>
          </View>
            <View style={StyleSheet.commentOutputWrap}>
              <TouchableHighlight
                activeOpacity={1}
                underlayColor="#ffffff"
                onPress={() => Navigation.push(this.props.componentId, {
                  component: {
                    name: 'ProfileScreenFollower',
                    options: {
                      topBar: {
                        visible: false,
                        title: {
                          text: 'ProfileScreenFollower',

                        }
                      }
                    }
                  }
                })}>
                <View style={StyleSheet.commentOutputWrap1}>
                  <Image
                    style={StyleSheet.profilePictureComment}
                    source={require('./src/profilepicture.png')}
                  />
                  <View>
                    <View style={StyleSheet.commentOutputWrapText} >
                      <Text style={StyleSheet.followerWrap}>Mojca Novak</Text>
                    </View>
                  </View>
                </View>
              </TouchableHighlight>
            </View>
            <View style={StyleSheet.commentOutputWrap}>
              <TouchableHighlight
                activeOpacity={1}
                underlayColor="#ffffff"
                onPress={() => Navigation.push(this.props.componentId, {
                  component: {
                    name: 'ProfileScreenFollower',
                    options: {
                      topBar: {
                        visible: false,
                        title: {
                          text: 'ProfileScreenFollower',

                        }
                      }
                    }
                  }
                })}>
                <View style={StyleSheet.commentOutputWrap1}>
                  <Image
                    style={StyleSheet.profilePictureComment}
                    source={require('./src/profilepicture.png')}
                  />
                  <View>
                    <View style={StyleSheet.commentOutputWrapText} >
                      <Text style={StyleSheet.followerWrap}>Mojca Novak</Text>
                    </View>
                  </View>
                </View>
              </TouchableHighlight>
            </View>
            <View style={StyleSheet.commentOutputWrap}>
              <TouchableHighlight
                activeOpacity={1}
                underlayColor="#ffffff"
                onPress={() => Navigation.push(this.props.componentId, {
                  component: {
                    name: 'ProfileScreenFollower',
                    options: {
                      topBar: {
                        visible: false,
                        title: {
                          text: 'ProfileScreenFollower',

                        }
                      }
                    }
                  }
                })}>
                <View style={StyleSheet.commentOutputWrap1}>
                  <Image
                    style={StyleSheet.profilePictureComment}
                    source={require('./src/profilepicture.png')}
                  />
                  <View>
                    <View style={StyleSheet.commentOutputWrapText} >
                      <Text style={StyleSheet.followerWrap}>Mojca Novak</Text>
                    </View>
                  </View>
                </View>
              </TouchableHighlight>
            </View>
            <View style={StyleSheet.commentOutputWrap}>
              <TouchableHighlight
                activeOpacity={1}
                underlayColor="#ffffff"
                onPress={() => Navigation.push(this.props.componentId, {
                  component: {
                    name: 'ProfileScreenFollower',
                    options: {
                      topBar: {
                        visible: false,
                        title: {
                          text: 'ProfileScreenFollower',

                        }
                      }
                    }
                  }
                })}>
                <View style={StyleSheet.commentOutputWrap1}>
                  <Image
                    style={StyleSheet.profilePictureComment}
                    source={require('./src/profilepicture.png')}
                  />
                  <View>
                    <View style={StyleSheet.commentOutputWrapText} >
                      <Text style={StyleSheet.followerWrap}>Mojca Novak</Text>
                    </View>
                  </View>
                </View>
              </TouchableHighlight>
            </View>
            <View style={StyleSheet.commentOutputWrap}>
              <TouchableHighlight
                activeOpacity={1}
                underlayColor="#ffffff"
                onPress={() => Navigation.push(this.props.componentId, {
                  component: {
                    name: 'ProfileScreenFollower',
                    options: {
                      topBar: {
                        visible: false,
                        title: {
                          text: 'ProfileScreenFollower',

                        }
                      }
                    }
                  }
                })}>
                <View style={StyleSheet.commentOutputWrap1}>
                  <Image
                    style={StyleSheet.profilePictureComment}
                    source={require('./src/profilepicture.png')}
                  />
                  <View>
                    <View style={StyleSheet.commentOutputWrapText} >
                      <Text style={StyleSheet.followerWrap}>Mojca Novak</Text>
                    </View>
                  </View>
                </View>
              </TouchableHighlight>
            </View>
            


          <Footer props={this.props} type={"basic"} location={"HomeScreen"} />
        </View>
      </>
    );

  }
}
export default CommentScreen;



