import React from 'react';
import {
    View,
    TouchableHighlight,
    Text
} from 'react-native';
import StyleSheet from "../Styles/Style";
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faPlus, faAngleRight, faCheck } from '@fortawesome/free-solid-svg-icons'
import { Navigation } from "react-native-navigation";
import { Snackbar } from 'react-native-paper';
const Footer = (parent) => {

    const [isValidate, setValidate] = React.useState(false);
    const [visible, setVisible] = React.useState(false);
    const onDismissSnackBar = () => setVisible(false);
    
    const validateBasicData = () => {
        {
            (parent.counterOrderDate == undefined || parent.counterOrderDate == ""  || parent.counterCustomer == undefined || parent.counterCustomer == "" || parent.counterMontageDate == undefined || parent.counterMontageDate == "" || parent.counterMontageHour == undefined || parent.counterMontageHour == "" || parent.counterAddress == undefined || parent.counterAddress == "" ) ? (
                console.log("TO", parent.counterCustomer),
                setVisible(true)
            ) : (console.log("TO", parent.counterCustomer),
                Navigation.push(parent.props.componentId, {
                    component: {
                        name: 'additionalInfo',
                        options: {
                            topBar: {
                                visible: false,
                                title: {
                                    text: 'additionalInfo',
                                }
                            }
                        }
                    }
                }))
        }
    }


    return (
        <>
            <View style={StyleSheet.footer}>
                <Text style={StyleSheet.footerText}>Klima T.E.A.M.</Text>
                {(parent.location == "home") ? (
                    <View style={StyleSheet.iconWrap}>
                        <View style={StyleSheet.footerCenterIcon}>
                            <TouchableHighlight style={StyleSheet.icon}
                                activeOpacity={1}
                                underlayColor="#ffffff"
                                onPress={() => Navigation.push(parent.props.componentId, {
                                    component: {
                                        name: 'OsnovniPodatki',
                                        options: {
                                            topBar: {
                                                visible: false,
                                                title: {
                                                    text: 'OsnovniPodatki',
                                                }
                                            }
                                        }
                                    }
                                })}>
                                <FontAwesomeIcon icon={faPlus} size='30px' style={{ alignSelf: 'center', marginTop: 15 }} />
                            </TouchableHighlight>
                        </View>
                    </View>
                ) : (
                    (parent.location == "OsnovniPodatki") ? (
                        <View style={StyleSheet.iconWrap}>
                            <View style={StyleSheet.footerCenterIcon}>
                                <TouchableHighlight style={StyleSheet.icon}
                                    activeOpacity={1}
                                    underlayColor="#ffffff"
                                    onPress={() => validateBasicData()}>
                                    <FontAwesomeIcon icon={faAngleRight} size='40px' style={{ alignSelf: 'center', marginTop: 10 }} />
                                </TouchableHighlight>
                            </View>
                        </View>
                    ) : (
                        (parent.location == "Materiali") ? (
                            <View style={StyleSheet.iconWrap}>
                                <View style={StyleSheet.footerCenterIcon}>
                                    <TouchableHighlight style={StyleSheet.icon}
                                        activeOpacity={1}
                                        underlayColor="#ffffff"
                                        onPress={() => Navigation.push(parent.props.componentId, {
                                            component: {
                                                name: 'Opombe',
                                                options: {
                                                    topBar: {
                                                        visible: false,
                                                        title: {
                                                            text: 'Opombe',
                                                        }
                                                    }
                                                }
                                            }
                                        })}>
                                        <FontAwesomeIcon icon={faAngleRight} size='40px' style={{ alignSelf: 'center', marginTop: 10 }} />
                                    </TouchableHighlight>
                                </View>
                            </View>
                        ) : (
                            (parent.location == "Opombe") ? (
                                <View style={StyleSheet.iconWrap}>
                                    <View style={StyleSheet.footerCenterIcon}>
                                        <TouchableHighlight style={StyleSheet.icon}
                                            activeOpacity={1}
                                            underlayColor="#ffffff"
                                            onPress={() => Navigation.push(parent.props.componentId, {
                                                component: {
                                                    name: 'Slike',
                                                    options: {
                                                        topBar: {
                                                            visible: false,
                                                            title: {
                                                                text: 'Slike',
                                                            }
                                                        }
                                                    }
                                                }
                                            })}>
                                            <FontAwesomeIcon icon={faAngleRight} size='40px' style={{ alignSelf: 'center', marginTop: 10 }} />
                                        </TouchableHighlight>
                                    </View>
                                </View>
                            ) : (
                                (parent.location == "Slike") ? (
                                    (parent.counter > -1) ? (
                                        <View style={StyleSheet.iconWrap}>
                                            <View style={StyleSheet.footerCenterIcon}>
                                                <TouchableHighlight style={StyleSheet.icon}
                                                    activeOpacity={1}
                                                    underlayColor="#ffffff"
                                                    onPress={() => Navigation.push(parent.props.componentId, {
                                                        component: {
                                                            name: 'Soglasje',
                                                            options: {
                                                                topBar: {
                                                                    visible: false,
                                                                    title: {
                                                                        text: 'Soglasje',
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    })}>
                                                    <FontAwesomeIcon icon={faAngleRight} size='40px' style={{ alignSelf: 'center', marginTop: 10 }} />
                                                </TouchableHighlight>
                                            </View>
                                        </View>
                                    ) : (null)
                                ) : (
                                    (parent.location == "Podpis") ? (
                                        <View style={StyleSheet.iconWrap}>
                                            <View style={StyleSheet.footerCenterIcon}>
                                                <TouchableHighlight style={StyleSheet.icon}
                                                    activeOpacity={1}
                                                    underlayColor="#ffffff"
                                                    onPress={() => Navigation.push(parent.props.componentId, {
                                                        component: {
                                                            name: 'PodpisIzvajalca',
                                                            options: {
                                                                topBar: {
                                                                    visible: false,
                                                                    title: {
                                                                        text: 'PodpisIzvajalca',
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    })}>
                                                    <FontAwesomeIcon icon={faAngleRight} size='40px' style={{ alignSelf: 'center', marginTop: 10 }} />
                                                </TouchableHighlight>
                                            </View>
                                        </View>
                                    ) : (
                                        (parent.location == "additionalInfo") ? (
                                        <View style={StyleSheet.iconWrap}>
                                            <View style={StyleSheet.footerCenterIcon}>
                                                <TouchableHighlight style={StyleSheet.icon}
                                                    activeOpacity={1}
                                                    underlayColor="#ffffff"
                                                    onPress={() => Navigation.push(parent.props.componentId, {
                                                        component: {
                                                            name: 'Materiali',
                                                            options: {
                                                                topBar: {
                                                                    visible: false,
                                                                    title: {
                                                                        text: 'Materiali',
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    })}>
                                                    <FontAwesomeIcon icon={faAngleRight} size='40px' style={{ alignSelf: 'center', marginTop: 10 }} />
                                                </TouchableHighlight>
                                            </View>
                                        </View>
                                    ) : (
                                        (parent.location == "Soglasje") ? (
                                        null
                                    ) : (
                                        <View style={StyleSheet.iconWrap}>
                                            <View style={StyleSheet.footerCenterIcon}>
                                                <TouchableHighlight style={StyleSheet.icon}
                                                    activeOpacity={1}
                                                    underlayColor="#ffffff"
                                                    onPress={() => Navigation.push(parent.props.componentId, {
                                                        component: {
                                                            name: 'Podpis',
                                                            options: {
                                                                topBar: {
                                                                    visible: false,
                                                                    title: {
                                                                        text: 'Podpis',
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    })}>
                                                    <FontAwesomeIcon icon={faAngleRight} size='40px' style={{ alignSelf: 'center', marginTop: 10 }} />
                                                </TouchableHighlight>
                                            </View>
                                        </View>
                                    )
                                    )
                                    )
                                )
                            )
                        )
                    )
                )}

                <Snackbar
                    style={{ alignSelf: 'center', marginBottom: 100 }}
                    visible={visible}
                    onDismiss={onDismissSnackBar}
                    action={{
                        label: 'Dismiss',
                        onPress: () => {
                            setVisible(false);
                        },
                    }}>
                    Vnesi vsa polja!
                </Snackbar>
            </View>

        </>


    );
};

export default Footer