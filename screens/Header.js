import React from 'react';
import {
  View,
  Image,
  TouchableHighlight,
  Text

} from 'react-native';
import StyleSheet from "../Styles/Style";
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faAngleLeft, faSearch } from '@fortawesome/free-solid-svg-icons'
import { Navigation } from "react-native-navigation";
import { useState } from "react";
import Dialog from "react-native-dialog";

import { Searchbar } from 'react-native-paper';

const Header = (parent) => {

  const [visible, setVisible] = useState(false);

  const showDialog = () => {
    setVisible(true);
  };

  const handleCancel = () => {
    setVisible(false);
  };

  const handleDelete = () => {
    // The user has pressed the "Delete" button, so here you can do your own logic.
    // ...Your logic
    setVisible(false);
  };


  const [searchQuery, setSearchQuery] = React.useState('');

  const onChangeSearch = query => setSearchQuery(query);

  return (
    <>
      <View style={StyleSheet.row}>
        <View style={StyleSheet.header}>
          {(parent.title == "Naročila") ? (
            <Text style={StyleSheet.headerTitleLeft}>{parent.title}</Text>
          ) : (
            (parent.title == "Slike" || "Materiali" || "Podpis" || "Opombe" || "Osnovni Podatki") ? (
              <>
                {(parent.location == "OsnovniPodatki") ? (
                  <TouchableHighlight style={StyleSheet.headerIconLeft}
                    activeOpacity={1}
                    underlayColor="#ffffff"
                    onPress={() => Navigation.push(parent.props.componentId, {
                      component: {
                        name: 'HomeScreen',
                        options: {
                          topBar: {
                            visible: false,
                            title: {
                              text: 'HomeScreen',
                            }
                          }
                        }
                      }
                    })}>
                    <FontAwesomeIcon icon={faAngleLeft} size='40px' style={{ alignSelf: 'center', marginTop: 10, color: "white" }} />
                  </TouchableHighlight>


                ) : (
                  (parent.location == "Materiali") ? (

                    <TouchableHighlight style={StyleSheet.headerIconLeft}
                      activeOpacity={1}
                      underlayColor="#ffffff"
                      onPress={() => Navigation.push(parent.props.componentId, {
                        component: {
                          name: 'additionalInfo',
                          options: {
                            topBar: {
                              visible: false,
                              title: {
                                text: 'additionalInfo',
                              }
                            }
                          }
                        }
                      })}>
                      <FontAwesomeIcon icon={faAngleLeft} size='40px' style={{ alignSelf: 'center', marginTop: 10, color: "white" }} />
                    </TouchableHighlight>

                  ) : (
                    (parent.location == "Opombe") ? (

                      <TouchableHighlight style={StyleSheet.headerIconLeft}
                        activeOpacity={1}
                        underlayColor="#ffffff"
                        onPress={() => Navigation.push(parent.props.componentId, {
                          component: {
                            name: 'Materiali',
                            options: {
                              topBar: {
                                visible: false,
                                title: {
                                  text: 'Materiali',
                                }
                              }
                            }
                          }
                        })}>
                        <FontAwesomeIcon icon={faAngleLeft} size='40px' style={{ alignSelf: 'center', marginTop: 10, color: "white" }} />
                      </TouchableHighlight>

                    ) : (
                      (parent.location == "Slike") ? (

                        <TouchableHighlight style={StyleSheet.headerIconLeft}
                          activeOpacity={1}
                          underlayColor="#ffffff"
                          onPress={() => Navigation.push(parent.props.componentId, {
                            component: {
                              name: 'Opombe',
                              options: {
                                topBar: {
                                  visible: false,
                                  title: {
                                    text: 'Opombe',
                                  }
                                }
                              }
                            }
                          })}>
                          <FontAwesomeIcon icon={faAngleLeft} size='40px' style={{ alignSelf: 'center', marginTop: 10, color: "white" }} />
                        </TouchableHighlight>

                      ) : (
                        (parent.location == "Podpis") ? (

                          <TouchableHighlight style={StyleSheet.headerIconLeft}
                            activeOpacity={1}
                            underlayColor="#ffffff"
                            onPress={() => Navigation.push(parent.props.componentId, {
                              component: {
                                name: 'Soglasje',
                                options: {
                                  topBar: {
                                    visible: false,
                                    title: {
                                      text: 'Soglasje',
                                    }
                                  }
                                }
                              }
                            })}>
                            <FontAwesomeIcon icon={faAngleLeft} size='40px' style={{ alignSelf: 'center', marginTop: 10, color: "white" }} />
                          </TouchableHighlight>

                        ) : (
                          (parent.location == "PodpisIzvajalca") ? (

                            <TouchableHighlight style={StyleSheet.headerIconLeft}
                              activeOpacity={1}
                              underlayColor="#ffffff"
                              onPress={() => Navigation.push(parent.props.componentId, {
                                component: {
                                  name: 'Podpis',
                                  options: {
                                    topBar: {
                                      visible: false,
                                      title: {
                                        text: 'Podpis',
                                      }
                                    }
                                  }
                                }
                              })}>
                              <FontAwesomeIcon icon={faAngleLeft} size='40px' style={{ alignSelf: 'center', marginTop: 10, color: "white" }} />
                            </TouchableHighlight>

                          ) : (
                            (parent.location == "additionalInfo") ? (

                              <TouchableHighlight style={StyleSheet.headerIconLeft}
                                activeOpacity={1}
                                underlayColor="#ffffff"
                                onPress={() => Navigation.push(parent.props.componentId, {
                                  component: {
                                    name: 'OsnovniPodatki',
                                    options: {
                                      topBar: {
                                        visible: false,
                                        title: {
                                          text: 'OsnovniPodatki',
                                        }
                                      }
                                    }
                                  }
                                })}>
                                <FontAwesomeIcon icon={faAngleLeft} size='40px' style={{ alignSelf: 'center', marginTop: 10, color: "white" }} />
                              </TouchableHighlight>

                            ) : (
                              (parent.location == "Soglasje") ? (

                                <TouchableHighlight style={StyleSheet.headerIconLeft}
                                  activeOpacity={1}
                                  underlayColor="#ffffff"
                                  onPress={() => Navigation.push(parent.props.componentId, {
                                    component: {
                                      name: 'Slike',
                                      options: {
                                        topBar: {
                                          visible: false,
                                          title: {
                                            text: 'Slike',
                                          }
                                        }
                                      }
                                    }
                                  })}>
                                  <FontAwesomeIcon icon={faAngleLeft} size='40px' style={{ alignSelf: 'center', marginTop: 10, color: "white" }} />
                                </TouchableHighlight>

                              ) : (
                                null
                              )
                            )
                          )
                        )
                      )
                    )
                  )
                )}
                <Text style={StyleSheet.headerTitle}>{parent.title}</Text>
              </>
            ) : (
              null
            )
          )

          }
        </View>

        {(parent.location == "HomeScreen") ? (
          <View style={{
            position: "absolute",
            right: 20,

          }}>
            <TouchableHighlight
              onPress={showDialog}>
              <FontAwesomeIcon icon={faSearch} size='20px' style={{ alignSelf: 'center', marginTop: 15, color: "white" }} />
            </TouchableHighlight>
            <Dialog.Container visible={visible}>
              <Dialog.Title>Išči naročila</Dialog.Title>
              <Dialog.Description>
                <Searchbar
                  placeholder="Search"
                  onChangeText={onChangeSearch}
                  value={searchQuery}
                />
              </Dialog.Description>
              <Dialog.Button label="Prekini iskanje" onPress={handleCancel} />

            </Dialog.Container>
          </View>
        ) : (
          <></>
        )
        }

      </View>
    </>
  );
};

export default Header

