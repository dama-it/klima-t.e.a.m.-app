import React from 'react';
import {
  View,
  AsyncStorage,
  Text,
  FlatList
} from 'react-native';
import StyleSheet from '../Styles/Style';
import Header from './Header';
import Footer from './Footer';
import CardScreen from './CardScreen';

import axios from 'axios';


class HomeScreen extends React.Component {

  state = {
    posts: [],
    size: 3
  };

  getPosts = (token) => {
    axios({
      url: 'http://75.119.145.14:8090/api/posts?page=0&size=' + this.state.size,
      method: 'get',
      headers: {
        Authorization: token,
      },
    }).then(response => {
      if (response.data) {
        this.setState({ posts: response.data.content });
        this.setState({ size: this.state.size + 12 })
      }
    }).catch((error) => {

      console.log(error.message);
    });
  }

  async componentDidMount() {
    let token = await AsyncStorage.getItem('token');
    this.getPosts(token);
  }

  async getPostWithToken() {
    let token = await AsyncStorage.getItem('token');
    this.getPosts(token);
  }

  constructor(props) {
    super(props);
  }

  renderItem = ({ item }) => (
    <CardScreen props={this.props} cardInfo={item}/>
  );

  fetchResult = () => (
    this.getPostWithToken()
  );

  render() {
    return (
      <>
        <View style={StyleSheet.background}>
          <Header props={this.props} title={"Naročila"} location={"HomeScreen"} />
          {(this.state.posts.length > 0) ? (
            <>
              <FlatList
                data={this.state.posts}
                renderItem={this.renderItem}
                keyExtractor={item => item.id}
                onEndReached={this.fetchResult}
                onEndReachedThreshold={0.1}
              />
            </>
          ) : (
            <Text>Iščem</Text>
          )
          }
          <Footer props={this.props} type={"basic"} location={"home"} />
        </View>
      </>
    );

  }
}
export default HomeScreen;



