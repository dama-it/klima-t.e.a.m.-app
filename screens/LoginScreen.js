import React from 'react';
import {
  View,
  Text,
  Image,
  TextInput,
  TouchableHighlight,
  ImageBackground
} from 'react-native';
import { Navigation } from "react-native-navigation";
import StyleSheet from '../Styles/Style';
import Logo from './src/Logo';
import authProvider from '../providers/authProvider';
import { getIdentity } from '../utils/restUtils';
import { Snackbar } from 'react-native-paper';


const image = { uri: "https://reactjs.org/logo-og.png" };

const loginSuccess = (r, props) => {
  getIdentity().then(data => {
    console.log("Identity:", data.username);
    console.log(props.componentId);
    Navigation.push(props.componentId, {
      component: {
        name: 'HomeScreen',
        options: {
          topBar: {
            visible: false,
            title: {
              text: 'HomeScreen',

            }
          }
        }
      }
    });
  }).catch(e => {
    console.log(e);
  });
};

const LoginScreen = (props) => {
  const [username, onChangeUsername] = React.useState("appuser");
  const [password, onChangePassword] = React.useState("Test123");
  const [visible, setVisible] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const onDismissSnackBar = () => setVisible(false);

  const doLogin = (u, p, props) => {
    setLoading(true);
    authProvider("AUTH_LOGIN", { username: u, password: p }).then(response => {
      loginSuccess(response, props);
      setTimeout(function () { setLoading(false); }, 2000);
    }).catch(e => {
      setVisible(true);
      setLoading(false);
    });
  };

  return (
    <View style={StyleSheet.background}>
    <ImageBackground source={require('./src/loginBackground.jpeg')} resizeMode="cover" style={{width:"100%", height:"100%"}}>
      <View >
      <Logo />
      </View>

      <View style={StyleSheet.inputWrap}>
        <TextInput
          style={StyleSheet.input}
          onChangeText={onChangeUsername}
          value={username}
          placeholder="Email"
        />
        <TextInput
          style={StyleSheet.input}
          onChangeText={onChangePassword}
          value={password}
          placeholder="Password"
        />
      </View>
      <View style={StyleSheet.WrapLogin1}>
        {(loading) ? (
          <Image source={require('./src/loading.gif')} style={StyleSheet.loading} />
        ) : (
          <View style={StyleSheet.damaButtonWrap}>
            <TouchableHighlight style={StyleSheet.damaButton}
              onPress={() => doLogin(username, password, props)}>
              <Text style={StyleSheet.damaButtonText}>{"VSTOPI"}</Text>
            </TouchableHighlight>
          </View>
        )
        }
      </View>
      <Snackbar
        visible={visible}
        onDismiss={onDismissSnackBar}
        action={{
          label: 'Dismiss',
          onPress: () => {
            setVisible(false);
          },
        }}>
       Prijava neuspešna!
      </Snackbar>
      </ImageBackground>
    </View>
  );
};

export default LoginScreen