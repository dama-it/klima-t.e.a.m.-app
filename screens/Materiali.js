import React from 'react';
import {
  SafeAreaView,
  Picker,
  View,
  Text,
  StatusBar,
  Image,
  TextInput,
  type,
  Pressable,
  onPress,
  title,
  AsyncStorage,
  Button,
  FlatList,
  TouchableHighlight
} from 'react-native';
import { Navigation } from "react-native-navigation";
import StyleSheet from '../Styles/Style';
import Header from './Header';
import Footer from './Footer';
import { DataTable } from 'react-native-paper';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faPlus, faTimes } from '@fortawesome/free-solid-svg-icons'


class Materiali extends React.Component {

  state = {
    posts: [],
    size: 3,
    selectedValue: ["Material 1"],
    selectedTextValue: ["0"],
    inputList: [{ firstName: "", lastName: "" }],
    rows: [],
    currentWork: [],
    allWorks: [],
  };

  setSelectedValue(itemValue) {
    this.setState({ selectedValue: itemValue });
    let currentWork = this.state.currentWork;
    currentWork["selectedValue"] = itemValue;
    this.setState(currentWork);
    console.log(this.state.currentWork)
  }

  setTextValue(textValue) {
    this.setState({ selectedTextValue: textValue });
    let currentWork = this.state.currentWork;
    currentWork["selectedTextValue"] = textValue;
    this.setState(currentWork);
    console.log(this.state.currentWork)
    
  }

  setRows() {
    let rows = this.state.rows;
    rows.push({material:this.state.selectedValue + "     " + this.state.selectedTextValue });
    this.setState({ rows });
  }

  removeRows = (i) =>{
    let rows = this.state.rows;
    rows.splice(i,1);
    this.setState({ rows });
  }

  render() {

    return (
      <>
        <View style={StyleSheet.background}>
          <Header props={this.props} title="Materiali" location={"Materiali"} />
          <View >
          
          {this.state.rows.map((item,index)=>(
            <DataTable>
              <DataTable.Row style={{ marginTop:15 }}>
              <Text style={{marginLeft:7}}>{item.material}</Text>
                <TouchableHighlight style={{ position:"absolute", right:7}}
                  onPress={() => this.removeRows(index)}>
                  <FontAwesomeIcon icon={faTimes} size='20px' style={{ color:'#0083d5' }} />
                </TouchableHighlight>
              </DataTable.Row>
              
            </DataTable>
            ))}
            <DataTable>
              <DataTable.Row >
                <Picker
                  selectedValue={this.selectedValue}
                  style={{ height: 50, width: 140 }}
                  onValueChange={(itemValue) => this.setSelectedValue(itemValue)}
                >
                  <Picker.Item label="Material 1" value="Material 1" />
                  <Picker.Item label="Material 2" value="Material 2" />
                  <Picker.Item label="Material 3" value="Material 3" />
                  <Picker.Item label="Material 4" value="Material 4" />
                  <Picker.Item label="Material 5" value="Material 5" />
                </Picker>
                <TextInput
                  onChangeText={(textValue) => this.setTextValue(textValue)}
                  value={this.SelectedTextValue}
                  placeholder="Količina"
                  style={{width: 100}}
                />
                <TouchableHighlight style={{ position:"absolute", right:7, marginTop: 15 }}
                  onPress={() => this.setRows()}>
                  <FontAwesomeIcon icon={faPlus} size='20px' style={{ color:'#ff9933' }}/>
                </TouchableHighlight>
              </DataTable.Row>
              
            </DataTable>
          </View>
          <Footer props={this.props} type={"basic"} location={"Materiali"} />
        </View>

      </>);
  }
};



export default Materiali;

