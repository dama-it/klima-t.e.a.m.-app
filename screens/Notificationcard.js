import { Card, CardTitle, CardContent, CardAction, CardButton, CardImage, ScrollView } from 'react-native-cards';
import React from 'react';
import StyleSheet from '../Styles/Style';
import PropTypes from 'prop-types';
import { propTypes } from 'redux-form';
import { CommentsScreen } from "./CommentsScreen";
import {
  SafeAreaView,
  AsyncStorage,
  View,
  Text,
  StatusBar,
  Image,
  TextInput,
  type,
  Pressable,
  onPress,
  title,
  Button,
  TouchableHighlight,
  useEffect
} from 'react-native';
import CardBodyText from './CardBodyText';
import { Navigation } from "react-native-navigation";
import NotificationsScreen from './NotificationsScreen';



class NotificationCard extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      likes: [],
      value: 0,
      liked: false,
      comments: [],
      comment: "",
      userName: "",

    };
  }
  async componentDidMount() {
    let username = await AsyncStorage.getItem('userFullName');
    this.setState({ username });
  }
  saveThisComment() {
    let commentsTmp = this.state.comments;
    console.log(commentsTmp);
    commentsTmp.push({ user: this.state.username, text: this.state.comment });
    this.setState({ comments: commentsTmp })
    console.log(this.state.comments);
  }
  setThisComment(e) {
    this.setState({ comment: e });
  }
  increaseValue() {
    console.log(this.state.liked)
    if (!this.state.liked) {
      this.setState({ liked: !this.state.liked })
      newValue = this.state.value + 1;
      this.setState({ value: newValue })
    }
    else {
      this.setState({ liked: !this.state.liked })
      newValue = this.state.value - 1;
      this.setState({ value: newValue })
    }
  }

  render() {
    return (
      <>
        <View>
          <Card>
            <View style={StyleSheet.notificationsCardWrap}>
              <View style={StyleSheet.cardHeaderWrap}>
                <Image
                  style={StyleSheet.profilePictureCard}
                  source={require('./src/profilepicture.png')}
                />
                <View>
                  <View style={StyleSheet.notificationParcelWrap}>
                    <Text style={StyleSheet.headerText}>{this.props.cardInfo.creator.firstName + " " + this.props.cardInfo.creator.lastName}</Text>
                    <Text style={StyleSheet.headerText1}>commented</Text>
                    <View style={StyleSheet.LikeNotification}>
                      <TouchableHighlight
                        activeOpacity={1}
                        underlayColor="#ffffff"
                        onPress={() => {
                          this.increaseValue()
                        }}>
                        <Image style={StyleSheet.likeIcon}
                          source={require('./src/Likeicon.png')}
                        />
                      </TouchableHighlight>
                    
                    <Text style={StyleSheet.likeIconText}>{this.state.value}</Text>
                    </View>
                  </View>
                  <Text style={StyleSheet.headerText1}>Bravo, keep it up!</Text>
                </View>


              </View>


            </View>
            <CardAction
              separator={true}
              inColumn={false}>

            </CardAction>

          </Card>

        </View>
      </>
    );
  }
}
NotificationCard.propTypes = {
  ...propTypes,
  cardInfo: PropTypes.object.isRequired,
};
export default NotificationCard;