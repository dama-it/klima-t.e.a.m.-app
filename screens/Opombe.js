import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  TextInput,
  type,
  Pressable,
  onPress,
  title,
  AsyncStorage,
  Button,
  FlatList,
  TouchableHighlight
} from 'react-native';
import { Navigation } from "react-native-navigation";
import StyleSheet from '../Styles/Style';
import Header from './Header';
import Footer from './Footer';
import CardScreen from './CardScreen';
import DamaButton from './DamaButton';

import axios from 'axios';
import localStorage from 'react-native-sync-localstorage'

localStorage.getAllFromLocalStorage()
  .then(() => {
  })
  .catch(err => {
    console.warn(err)
  })


class Opombe extends React.Component {

  state = {
    posts: [],
    size: 3,
    text: "",
    text1: "",
    currentWork: [],
    allWorks: [],
  };


  setText(textValue) {
    let currentWork = this.state.currentWork;
    currentWork["textValue"] = textValue;
    this.setState(currentWork);
    this.setState({ text: textValue });
    localStorage.setItem('currentWork', currentWork)
    console.log(localStorage.getItem('currentWork'))

    /*  localStorage.setItem('orderDate', this.state.basicData.orderDate) */
    /*  console.log(localStorage.getItem('orderDate')) */
  }

  saveData() {
    this.setState({ text1: this.state.text });
  }

  UselessTextInput = (props) => {
    return (
      <TextInput
        {...props} // Inherit any props passed to it; e.g., multiline, numberOfLines below
        editable
        maxLength={40}
      />
    );
  }


  render() {
    return (
      <>
        <View style={StyleSheet.background}>

          <Header props={this.props} title="Dodaten opis" location={"Opombe"} />
          <View style={StyleSheet.inputWrap}>
            <TextInput
              multiline
              numberOfLines={4}
              style={StyleSheet.input}
              onChangeText={(textValue) => this.setText(textValue)}
              value={this.text}
              placeholder="Vpiši dodaten opis"
            />
            <View style={{ alignItems: 'center' }}>
            </View>

          </View>
          <Footer props={this.props} type={"basic"} location={"Opombe"} />
        </View>

      </>);
  }
};



export default Opombe;