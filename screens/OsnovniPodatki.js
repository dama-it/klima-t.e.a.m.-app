import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  type,
  Pressable,
  onPress,
  title,
  AsyncStorage,
  Button,
  FlatList,
  TouchableHighlight,
  TextInput
} from 'react-native';
import { Navigation } from "react-native-navigation";
import StyleSheet from '../Styles/Style';
import Header from './Header';
import Footer from './Footer';
import localStorage from 'react-native-sync-localstorage'
/* import DatePicker from "./src/DatePicker" */
import PropTypes from 'prop-types';
import { propTypes } from 'redux-form';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faAngleLeft, faCalendar, faSearch } from '@fortawesome/free-solid-svg-icons'
import DatePicker from 'react-native-modern-datepicker';
import Dialog from "react-native-dialog";
import { typeOf } from 'react-is';

localStorage.getAllFromLocalStorage()
  .then(() => {
  })
  .catch(err => {
    console.warn(err)
  })

class OsnovniPodatki extends React.Component {

  state = {
    posts: [],
    size: 3,
    currentWork: [],
    allWorks: [],
    basicData: [{ orderDate: "", customer: "", montageDate: "", montageHour: "", address: "" }],
    dialogOpen: false,
    param: "",

  };

  showDialog = () => {
    this.setState({ dialogOpen: true });
    console.log("TUKAJONN", this.state.dialogOpen)
  };

  handleCancel = () => {
    this.setState({ dialogOpen: false });
    this.setOrderDateValue();
    console.log("TUKAJOFF", this.state.dialogOpen)
  };

  setParam(OrderDateValue) {
    this.setState({ param: OrderDateValue });
  }
  setOrderDateValue(OrderDateValue) {
    let currentWork = this.state.currentWork;
    currentWork["orderDate"] = OrderDateValue;
    this.setState(currentWork);
    console.log(this.state.currentWork)
    localStorage.setItem('orderDate', this.state.currentWork.orderDate)
    console.log(localStorage.getItem('orderDate'))
  }

  setCustomerValue(customerValue) {
    let currentWork = this.state.currentWork;
    currentWork["customer"] = customerValue;
    this.setState(currentWork);
    console.log(this.state.currentWork)
    localStorage.setItem('customer', this.state.basicData.customer)
    console.log(this.state.customer)

  }

  setMontageDateValue(montageDateValue) {
    let currentWork = this.state.currentWork;
    currentWork["montageDate"] = montageDateValue;
    this.setState(currentWork);
    console.log(this.state.currentWork)


    localStorage.setItem('montageDate', this.state.basicData.montageDate)
    console.log(this.state.montageDate)
  }

  setMontageHourValue(montageHourValue) {
    let currentWork = this.state.currentWork;
    currentWork["montageHour"] = montageHourValue;
    this.setState(currentWork);
    console.log(this.state.currentWork)


    localStorage.setItem('montageHour', this.state.basicData.montageHour)
    console.log(this.state.montageHour)
  }

  setAddressValue(addressValue) {
    let currentWork = this.state.currentWork;
    currentWork["address"] = addressValue;
    this.setState(currentWork);
    console.log(this.state.currentWork)
    localStorage.setItem('address', this.state.basicData.address)
    console.log(this.state.address)
  }





  render() {
    return (
      <>
        <View style={StyleSheet.background}>
          <Header props={this.props} title="Osnovni Podatki" location={"OsnovniPodatki"} />
          <View style={StyleSheet.inputWrap}>
            <Text style={StyleSheet.IdText}>ID              #1543564</Text>

            <View style={{ flexDirection: "row", marginLeft: 20, marginBottom: 20 }}>
              <TouchableHighlight
                onPress={this.showDialog}>
                <FontAwesomeIcon icon={faCalendar} size='20px' />
              </TouchableHighlight>
              <Dialog.Container visible={this.state.dialogOpen} style={{ width: "90%", height: "80%" }}>
                <Dialog.Title>Spremeni datum</Dialog.Title>
                <Dialog.Description>
                  <DatePicker
                    style={{ width: 300, height: "100%" }}
                    value={this.OrderDateValue}
                  //onSelectedChange={(OrderDateValue) => this.setParam(OrderDateValue)}
                  />
                </Dialog.Description>
                <Dialog.Button label="Zapri" onPress={this.handleCancel} />

              </Dialog.Container>
              <Text style={{ marginLeft: 20 }}>Ura in Datum naročila:</Text>
            </View>

            <View style={{ flexDirection: "row", marginLeft: 20, marginBottom: 20 }}>
              <TouchableHighlight
                onPress={this.showDialog}>
                <FontAwesomeIcon icon={faCalendar} size='20px' />
              </TouchableHighlight>
              <Dialog.Container visible={this.state.dialogOpen} style={{ width: "90%", height: "80%" }}>
                <Dialog.Title>Spremeni datum</Dialog.Title>
                <Dialog.Description>
                  <DatePicker
                    style={{ width: 300, height: "100%" }}
                    value={this.OrderDateValue}
                  //onSelectedChange={(OrderDateValue) => this.setParam(OrderDateValue)}
                  />
                </Dialog.Description>
                <Dialog.Button label="Zapri" onPress={this.handleCancel} />

              </Dialog.Container>
              <Text style={{ marginLeft: 20 }}>Ura in Datum montaže:</Text>
            </View>


            <TextInput
              style={StyleSheet.input}
              onChangeText={(OrderDateValue) => this.setOrderDateValue(OrderDateValue)}
              value={this.orderDate}
              placeholder="Datum naročila"
            />

            <TextInput
              style={StyleSheet.input}
              onChangeText={(customerValue) => this.setCustomerValue(customerValue)}
              value={this.customer}
              placeholder="Stranka/podjetje"
            />
            <TextInput
              style={StyleSheet.input}
              onChangeText={(montageDateValue) => this.setMontageDateValue(montageDateValue)}
              value={this.montageDate}
              placeholder="Datum montaže"
            />
            <TextInput
              style={StyleSheet.input}
              onChangeText={(montageHourValue) => this.setMontageHourValue(montageHourValue)}
              value={this.montageHour}
              placeholder="Ura montaže"
            />
            <TextInput
              style={StyleSheet.input}
              onChangeText={(addressValue) => this.setAddressValue(addressValue)}
              value={this.addressValue}
              placeholder="Naslov stranke"
            />
          </View>
          <View style={{ alignItems: 'center' }}>
            <View style={{ marginLeft: 10, marginBottom: 10 }}>
              <TouchableHighlight
                activeOpacity={1}
                underlayColor="#ffffff"
                onPress={() => Navigation.push(this.props.componentId, {
                  component: {
                    name: 'Opombe',
                    options: {
                      topBar: {
                        visible: false,
                        title: {
                          text: 'Opombe',

                        }
                      }
                    }
                  }
                })}>
                <Text style={{ color: "red" }} >ODPOVED MONTAŽE</Text>
              </TouchableHighlight>
            </View>
          </View>
          <Footer props={this.props} type={"basic"} location={"OsnovniPodatki"} counter={this.state.currentWork.length} counterOrderDate={this.state.currentWork.orderDate} counterCustomer={this.state.currentWork.customer} counterMontageDate={this.state.currentWork.montageDate} counterMontageHour={this.state.currentWork.montageHour} counterAddress={this.state.currentWork.address} />
        </View>

      </>);
  }
};


OsnovniPodatki.propTypes = {
  ...propTypes,
  date: PropTypes.string.isRequired,
};

export default OsnovniPodatki;