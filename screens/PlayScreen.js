import React, { createRef } from 'react';
import {
  View,
  Text,
  TouchableHighlight,
  SafeAreaView,
} from 'react-native';
import StyleSheet from '../Styles/Style';
import Header from './Header';
import Footer from './Footer';
import SignatureCapture from 'react-native-signature-capture';


const App = () => {
  const sign = createRef();

  const saveSign = () => {
    sign.current.saveImage();
  };

  const resetSign = () => {
    sign.current.resetImage();
  };

  const _onSaveEvent = (result) => {
    //result.encoded - for the base64 encoded png
    //result.pathName - for the file path name
    alert('Signature Captured Successfully');
    console.log(result.encoded);
  };

  const _onDragEvent = () => {
    // This callback will be called when the user enters signature
    console.log('dragged');
  };


  return (
    <View style={StyleSheet.background}>
      <Header props={this.props} title="Podpis" location={""} />



      <SignatureCapture
        style={{ width: "100%", height: "70%" }}
        ref={sign}
        onSaveEvent={_onSaveEvent}
        onDragEvent={_onDragEvent}
        showNativeButtons={false}
        showTitleLabel={false}
        viewMode={'portrait'}
      />
      <View style={{ flexDirection: 'row' }}>
        <TouchableHighlight
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            height: 50,
            backgroundColor: '#eeeeee',
            margin: 10,
          }}
          onPress={() => {
            saveSign();
          }}>
          <Text>Save</Text>
        </TouchableHighlight>
        <TouchableHighlight
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            height: 50,
            backgroundColor: '#eeeeee',
            margin: 10,
          }}
          onPress={() => {
            resetSign();
          }}>
          <Text>Reset</Text>
        </TouchableHighlight>
      </View>


      <Footer props={this.props} type={"basic"} location={"ProfileScreen"} />
    </View>
  );
};
export default App;











