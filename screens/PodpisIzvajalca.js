import React, { createRef } from 'react';
import {
  View,
  Text,
  TouchableHighlight,
  SafeAreaView,
} from 'react-native';
import StyleSheet from '../Styles/Style';
import Header from './Header';
import Footer from './Footer';
import SignatureCapture from 'react-native-signature-capture';


class PodpisIzvajalca extends React.Component {
   sign = createRef();

  saveSign = () => {
    this.sign.current.saveImage();
  };

   resetSign = () => {
    this.sign.current.resetImage();
  };

   _onSaveEvent = (result) => {
    //result.encoded - for the base64 encoded png
    //result.pathName - for the file path name
    alert('Signature Captured Successfully');
    console.log(result.encoded);
  };

   _onDragEvent = () => {
    // This callback will be called when the user enters signature
    console.log('dragged');
  };

  render() {

  return (
    <View style={StyleSheet.background}>
      <Header props={this.props} title="Podpis izvajalca" location={"PodpisIzvajalca"} />



      <SignatureCapture
        style={{ width: "100%", height: "70%" }}
        ref={this.sign}
        onSaveEvent={this._onSaveEvent}
        onDragEvent={this._onDragEvent}
        showNativeButtons={false}
        showTitleLabel={false}
        viewMode={'portrait'}
      />
      <View style={{ flexDirection: 'row' }}>
        <TouchableHighlight
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            height: 50,
            backgroundColor: '#ff9933',
            margin: 10,
            borderRadius:5
          }}
          onPress={() => {
            this.saveSign();
          }}>
          <Text style={StyleSheet.buttonText}>Shrani</Text>
        </TouchableHighlight>
        <TouchableHighlight
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            height: 50,
            backgroundColor: '#ff9933',
            margin: 10,
            borderRadius:5
          }}
          onPress={() => {
            this.resetSign();
          }}>
          <Text style={StyleSheet.buttonText}>Ponastavi</Text>
        </TouchableHighlight>
      </View>


      <Footer props={this.props} type={"basic"} location={"Podpis"} />
    </View>
  );
}
};
export default PodpisIzvajalca;
