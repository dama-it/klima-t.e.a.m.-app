import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  TextInput,
  type,
  Pressable,
  onPress,
  title,
  Button,
  TouchableHighlight,
} from 'react-native';
import StyleSheet from '../Styles/Style';
import DamaButton from './DamaButton';
import { useState } from 'react'
import DatePicker from 'react-native-date-picker'
import { Navigation } from "react-native-navigation";
import { Picker } from '@react-native-picker/picker';
import CountryPicker from 'react-native-country-picker-modal'
import BouncyCheckbox from "react-native-bouncy-checkbox"
import Speedometer from 'react-native-speedometer-chart';






const PressureCalibrationScreen = (props) => {
  const [height, onChangeHeight] = React.useState("");
  const [weight, onChangeWeight] = React.useState("");
  const [date, setDate] = useState(new Date())
  const [selectedGender, setSelectedGender] = useState("Gender");
  const [country, setCountry] = useState(null)




  return (
    <>
      <View style={StyleSheet.background}>
        <SafeAreaView style={StyleSheet.speedometer} >
          <Speedometer
            value={60}
            totalValue={100}
            size={250}
            outerColor="#d3d3d3"
            internalColor="#8600b3"
            showText
            text="50.00"
            textStyle={{ color: 'black' }}
            showLabels
            labelStyle={{ color: 'blue' }}
            labelFormatter={number => `${number}%`}
            showPercent
            percentStyle={{ color: "#8600b3" }}
          />
        </SafeAreaView>
        <Text style={StyleSheet.speedometerText}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt?</Text>
        <View style={StyleSheet.calibrationButtonWrap}>
          <View style={StyleSheet.calibrationButton}>
            <DamaButton title={"Yes"} link={"CalibrationPicker"} props={props}  ></DamaButton>
          </View>
          <View style={StyleSheet.calibrationButton}>
            <DamaButton title={"No"} link={"LoginScreen"} props={props}  ></DamaButton>
          </View>
        </View>
      </View>
    </>
  );
};

export default PressureCalibrationScreen