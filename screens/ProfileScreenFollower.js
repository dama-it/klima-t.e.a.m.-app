import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  Image,
  AsyncStorage,
  FlatList,
  TouchableHighlight
} from 'react-native';
import { Navigation } from "react-native-navigation";
import StyleSheet from '../Styles/Style';
import Header from './Header';
import Footer from './Footer';
import CardScreen from './CardScreen';
import axios from 'axios';

class ProfileScreenFollower extends React.Component {

  state = {
    posts: [],
    size: 3,
    identityappuser: [],
    membershipName:"",
    identityappuserall: [],
  };



  getidentity = (token) => {
    axios({
      url: 'https://portal.lailaa.si/app/identity',
      method: 'get',
      headers: {
        Authorization: token,
      },
    }).then(response => {
      if (response.data) {
        this.setState({ identityappuser: response.data.identity.membershipList });
        this.setState({ identityappuserall: response.data.identity });
        console.log(this.state.identityappuserall)
      }
    }).catch((error) => {
      console.log(error.message);
    });
  }

  getPosts = (token) => {
    axios({
      url: 'http://75.119.145.14:8090/api/posts?page=0&size=' + this.state.size,
      method: 'get',
      headers: {
        Authorization: token,
      },
    }).then(response => {
      if (response.data) {
        this.setState({ posts: response.data.content });
        this.setState({ size: this.state.size + 6 })
      }
    }).catch((error) => {
      console.log(error.message);
    });
  }

  renderItem = ({ item }) => (
    <CardScreen props={this.props} cardInfo={item} />
  );

  fetchResult = () => (
    this.getPostWithToken()
  );

  async componentDidMount() {
    let token = await AsyncStorage.getItem('token');
    this.getPosts(token);
    this.getidentity(token);
    
  }

  async getPostWithToken() {
    let token = await AsyncStorage.getItem('token');
    this.getPosts(token);
  }


  renderNameItem = () => {

    return (
      <>
        {this.state.identityappuser.map((element) => {
          return (   
            <Text style={StyleSheet.subscriptionName}>{element.name}</Text>
            )
        })}
      </>
    );
  }

  render() {
    return (
      <>
        <View style={StyleSheet.background}>
          <Header props={this.props} title={this.state.identityappuserall.userFirstName +" " + this.state.identityappuserall.userLastName} location={"ProfileScreen"} />
          <ScrollView>
            <View style={StyleSheet.profileWrap}>
              <Image
                style={StyleSheet.profilePicture}
                source={require('./src/profilepicture.png')}
              />
              <View style={StyleSheet.statsWrap}>
                <View style={StyleSheet.statsWrapOne}>
                  <Text style={StyleSheet.statsNumber}>{this.state.identityappuserall.points}</Text>
                  <TouchableHighlight
                    activeOpacity={1}
                    underlayColor="#ffffff"
                    onPress={() => Navigation.push(this.props.componentId, {
                      component: {
                        name: 'PlayScreen',
                        options: {
                          topBar: {
                            visible: false,
                            title: {
                              text: 'PlayScreen',

                            }
                          }
                        }
                      }
                    })}>
                    <Text style={StyleSheet.statsText}>Sessions</Text>
                  </TouchableHighlight>
                </View>
                <View style={StyleSheet.statsWrapOne}>
                  <Text style={StyleSheet.statsNumber}>34</Text>
                  <TouchableHighlight
                    activeOpacity={1}
                    underlayColor="#ffffff"
                    onPress={() => Navigation.push(this.props.componentId, {
                      component: {
                        name: 'FollowersScreen',
                        options: {
                          topBar: {
                            visible: false,
                            title: {
                              text: 'FollowersScreen',

                            }
                          }
                        }
                      }
                    })}>
                    <Text style={StyleSheet.statsText}>Followers</Text>
                  </TouchableHighlight>
                </View>
                <View style={StyleSheet.statsWrapOne}>
                  <Text style={StyleSheet.statsNumber}>55</Text>
                  <TouchableHighlight
                    activeOpacity={1}
                    underlayColor="#ffffff"
                    onPress={() => Navigation.push(this.props.componentId, {
                      component: {
                        name: 'PlayScreen',
                        options: {
                          topBar: {
                            visible: false,
                            title: {
                              text: 'PlayScreen',

                            }
                          }
                        }
                      }
                    })}>
                    <Text style={StyleSheet.statsText}>Following</Text>
                  </TouchableHighlight>
                </View>
              </View>
              <Text style={StyleSheet.userDescriptionText}>Sem Mojca Novak in se rada masiram!</Text>
            </View>
            <TouchableHighlight
              activeOpacity={1}
              underlayColor="#ffffff"
              onPress={() => Navigation.push(this.props.componentId, {
                component: {
                  name: 'MembershipSelectorScreen',
                  options: {
                    topBar: {
                      visible: false,
                      title: {
                        text: 'MembershipSelectorScreen',

                      }
                    }
                  }
                }
              })}>
              <View>{this.renderNameItem()}</View>
            </TouchableHighlight>
            <SafeAreaView>
              {(this.state.posts.length > 0) ? (
                <>
                  <FlatList
                    data={this.state.posts}
                    renderItem={this.renderItem}
                    keyExtractor={item => item.id}
                    onEndReached={this.fetchResult}
                    onEndReachedThreshold={0.1}
                  />
                </>
              ) : (
                <Text>No posts yet!</Text>
              )
              }
            </SafeAreaView>
          </ScrollView>
          <Footer props={this.props} type={"basic"} location={"ProfileScreen"} />
        </View>

      </>);
  }
};
export default ProfileScreenFollower;