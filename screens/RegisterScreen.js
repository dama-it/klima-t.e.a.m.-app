import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  TextInput,
  type,
  Pressable,
  onPress,
  title,
  Button
} from 'react-native';
import { Navigation } from "react-native-navigation";
import StyleSheet from '../Styles/Style';
import DamaButton from './DamaButton';
import Logo from './src/Logo';
import BcryptReactNative from 'bcrypt-react-native';

const RegisterScreen = (props) => {

  const [username, onChangeUsername] = React.useState("");
  const [passwordRegister, onChangePasswordRegister] = React.useState("Lolek");
  const [passwordRegisterConfirm, onChangePasswordRegisterConfirm] = React.useState("");


  

  return (
    <>
      <View style={StyleSheet.background}>
        <View style={StyleSheet.logo}>
          <Logo />
        </View>
        <SafeAreaView>
          <TextInput
            style={StyleSheet.input}
            onChangeText={onChangeUsername}
            value={username}
            placeholder="Username"
          />
          <TextInput
            style={StyleSheet.input}
            onChangeText={onChangePasswordRegister}
            value={passwordRegister}
            placeholder="Password"
          />
          <TextInput
            style={StyleSheet.input}
            onChangeText={onChangePasswordRegisterConfirm}
            value={passwordRegisterConfirm}
            placeholder="Confirm password"
            
          />
        </SafeAreaView>
        <View style={StyleSheet.buttonResetPassword}>
          <DamaButton title={"Submit"} link={"UserRegistrationDataScreen"} props={props}></DamaButton>
        </View>
      </View>
    </>
  );

};

export default RegisterScreen;