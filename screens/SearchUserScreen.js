import React from 'react';
import {
  SafeAreaView,
  View,
  Button,
  ScrollView,
  Text,
  TouchableHighlight,

} from 'react-native';
import { Navigation } from "react-native-navigation";
import StyleSheet from '../Styles/Style';
import Header from './Header';
import Footer from './Footer';
import CardScreen from './CardScreen';
import SearchUser from './src/SearchUser';
import ProfileBlack from './src/ProfileBlack';
import IconFB from './src/IconFB';
import Contacts from './src/Contacts';
import ContactCard from './ContactCard';






const SearchUserScreen = (props) => {

  return (
    <>
      <View style={StyleSheet.background}>
        <Header props={props} title={"Find and invite"} location={"SearchUserScreen"} />
        <View style={StyleSheet.searchUserMenuWrap}>
          <View style={StyleSheet.searchUserIconWrap}>
            <TouchableHighlight style={StyleSheet.searchUserIcon}
              activeOpacity={1}
              underlayColor="#ffffff"
              onPress={() => Navigation.push(props.componentId, {
                component: {
                  name: 'SearchUserScreen',
                  options: {
                    topBar: {
                      visible: false,
                      title: {
                        text: 'SearchUserScreen',

                      }
                    }
                  }
                }
              })}>
              <ProfileBlack style={StyleSheet.icon} />
            </TouchableHighlight>
            <Text style={StyleSheet.searchUserIcon}>Sugested</Text>
          </View>
          <View style={StyleSheet.searchUserIconWrap}>
            <TouchableHighlight style={StyleSheet.searchUserIcon}
              activeOpacity={1}
              underlayColor="#ffffff"
              onPress={() => Navigation.push(props.componentId, {
                component: {
                  name: 'SearchUserScreen',
                  options: {
                    topBar: {
                      visible: false,
                      title: {
                        text: 'SearchUserScreen',

                      }
                    }
                  }
                }
              })}>
              <IconFB style={StyleSheet.icon} />
            </TouchableHighlight>
            <Text style={StyleSheet.searchUserIcon}>Facebook</Text>
          </View>
          <View style={StyleSheet.searchUserIconWrap}>
            <TouchableHighlight style={StyleSheet.searchUserIcon}
              activeOpacity={1}
              underlayColor="#ffffff"
              onPress={() => Navigation.push(props.componentId, {
                component: {
                  name: 'SearchUserScreen',
                  options: {
                    topBar: {
                      visible: false,
                      title: {
                        text: 'SearchUserScreen',

                      }
                    }
                  }
                }
              })}>
              <Contacts style={StyleSheet.icon} />
            </TouchableHighlight>
            <Text style={StyleSheet.searchUserIcon}>Contacts</Text>
          </View>
        </View>
        <View >
          <ScrollView style={StyleSheet.scrollViewUser}>
            <ContactCard props={props} />
            <ContactCard props={props} />
            <ContactCard props={props} />
            <ContactCard props={props} />
            <ContactCard props={props} />
            <ContactCard props={props} />
            <ContactCard props={props} />
            <ContactCard props={props} />
            <ContactCard props={props} />
            <ContactCard props={props} />
            <ContactCard props={props} />
            <ContactCard props={props} />
            <ContactCard props={props} />
            <ContactCard props={props} />
            <ContactCard props={props} />
            <ContactCard props={props} />
            <ContactCard props={props} />
            <ContactCard props={props} />
            <ContactCard props={props} />
            <ContactCard props={props} />
            <ContactCard props={props} />
            <ContactCard props={props} />
            <ContactCard props={props} />
            <ContactCard props={props} />
          </ScrollView>
        </View>



        <Footer props={props} type={"basic"} />
      </View>
    </>
  );
};
export default SearchUserScreen;