import React from 'react';

import { StyleSheet, Text, View, TouchableOpacity, Button, Image, TouchableHighlight, ScrollView } from 'react-native';
import { DataTable } from 'react-native-paper';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faPlus, faTimes } from '@fortawesome/free-solid-svg-icons'
import * as ImagePicker from "react-native-image-picker"
import Header from './Header';
import Footer from './Footer';


export default class App extends React.Component {

  constructor(props) {

    super(props);

    this.state = {
      fileUri: "",
      resourcePath: {},
      rows: [],
      photos: [],
      selectedValue: [],
    };

  }



  selectFile = () => {

    var options = {
      title: 'Select Image',
      customButtons: [
        {
          name: 'customOptionKey',
          title: 'Choose file from Custom Option'
        },
      ],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.showImagePicker(options, res => {
      console.log('Response = ', res);
      if (res.didCancel) {
        console.log('User cancelled image picker');
      } else if (res.error) {
        console.log('ImagePicker Error: ', res.error);
      } else if (res.customButton) {
        console.log('User tapped custom button: ', res.customButton);
        alert(res.customButton);
      } else {
        let source = res;
        this.setState({
          resourcePath: source,
        });
      }
    });
  };


  // Launch Camera

  cameraLaunch = () => {
    let options = {
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.launchCamera(options, (res) => {
      console.log('Response = ', res);
      if (res.didCancel) {
        console.log('User cancelled image picker');
      } else if (res.error) {
        console.log('ImagePicker Error: ', res.error);
      } else if (res.customButton) {
        console.log('User tapped custom button: ', res.customButton);
        alert(res.customButton);
      } else {
        const source = { uri: res.uri };
        console.log("TEST", res.assets[0].uri);
        this.setState({
          filePath: res,
          fileData: res.data,
          fileUri: res.assets[0].uri,
        });

        let photos = this.state.photos;
        photos["photo"] = this.state.fileUri;
        this.setState(photos);
        let rows = this.state.rows;
        rows.push({ material: this.state.fileUri });
        this.setState({ rows });

      }

    });



  }



  imageGalleryLaunch = () => {

    let options = {

      storageOptions: {

        skipBackup: true,

        path: 'images',

      },

    };



    ImagePicker.launchImageLibrary(options, (res) => {

      console.log('Response = ', res);



      if (res.didCancel) {

        console.log('User cancelled image picker');

      } else if (res.error) {

        console.log('ImagePicker Error: ', res.error);

      } else if (res.customButton) {

        console.log('User tapped custom button: ', res.customButton);

        alert(res.customButton);

      } else {



        const source = { uri: res.uri };

        console.log('response', JSON.stringify(res));

        this.setState({

          filePath: res,

          fileData: res.data,

          fileUri: res.assets[0].uri,

          photos: res.assets[0].uri,


        });

        let photos = this.state.photos;
        photos["photo"] = this.state.fileUri;
        this.setState(photos);
        let rows = this.state.rows;
        rows.push({ material: this.state.fileUri });
        this.setState({ rows });

      }

    });

  }
  removeRows = (i) => {
    let rows = this.state.rows;
    rows.splice(i, 1);
    this.setState({ rows });
  }


  combine() {
    this.cameraLaunch();
    this.setRows();
  }

  renderItem = ({ item }) => (
    <CardScreen props={this.props} cardInfo={item} />
  );


  render() {

    return (
      <>
        <View style={StyleSheet.background}>
          <Header props={this.props} title="Slike" location={"Slike"} />
          <ScrollView style={{ marginTop: 20, width: "100%", height: "60%" }} >
            {this.state.rows.map((row, index) => (
              <>

                <View style={{ marginTop: 20, marginLeft: 20, flexDirection: "row" }}>
                  <Image
                    source={{ uri: row.material }}
                    style={{ width: 100, height: 100 }}
                  />

                  

                  <TouchableHighlight style={{ marginTop: 35, marginLeft: "65%" }}
                    onPress={() => this.removeRows(index)}>
                    <FontAwesomeIcon icon={faTimes} size='20px' style={{ color: '#0083d5' }} />
                  </TouchableHighlight>
                </View>


              </>
            ))}
          </ScrollView>



          <View style={{ height: "100%", alignSelf: "center", position: "absolute", top: 560 }}>
            <Text style={{ alignSelf: "center", marginBottom: 10 }} >Najmanj 5 slik</Text>
            <Text style={{ alignSelf: "center", marginBottom: 10 }}>Število slik: {this.state.rows.length}</Text>
            <TouchableOpacity onPress={this.cameraLaunch} style={styles.button}  >

              <Text style={styles.buttonText}>Slikaj</Text>

            </TouchableOpacity>



            <TouchableOpacity onPress={this.imageGalleryLaunch} style={styles.button}  >

              <Text style={styles.buttonText}>Odpri galerijo</Text>

            </TouchableOpacity>
          </View>
        </View>

        <Footer props={this.props} type={"basic"} location={"Slike"} counter={this.state.rows.length} />
      </>
    );

  }

}



const styles = StyleSheet.create({

  container: {


    padding: 30,

    alignItems: 'center',

    justifyContent: 'center',

    backgroundColor: '#fff'

  },

  button: {

    width: 200,

    height: 40,

    backgroundColor: '#ff9933',

    alignItems: 'center',

    justifyContent: 'center',

    borderRadius: 4,

    marginBottom: 12,



  },

  buttonText: {

    color: 'white',
    textTransform: 'uppercase',
    fontSize: 13,
    fontWeight: "bold",

  }

});