import React from 'react';
import {
  View,
  Text,
  TouchableHighlight,
  TouchableOpacity
} from 'react-native';
import { Navigation } from "react-native-navigation";
import StyleSheet from '../Styles/Style';
import Header from './Header';
import Footer from './Footer';
import { DataTable } from 'react-native-paper';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faPlus, faTimes } from '@fortawesome/free-solid-svg-icons'
import * as ImagePicker from "react-native-image-picker"





class Photos extends React.Component {

  state = {
    posts: [],
    size: 3,
    selectedValue: ["Material 1"],
    selectedTextValue: ["0"],
    inputList: [{ firstName: "", lastName: "" }],
    rows: [],
    currentWork: [],
    allWorks: [],
    photos:[],
  };

  setSelectedValue(itemValue) {
    this.setState({ selectedValue: itemValue });
    let currentWork = this.state.currentWork;
    currentWork["selectedValue"] = itemValue;
    this.setState(currentWork);
    console.log(this.state.currentWork)
  }

  setTextValue(textValue) {
    this.setState({ selectedTextValue: textValue });
    let currentWork = this.state.currentWork;
    currentWork["selectedTextValue"] = textValue;
    this.setState(currentWork);
    console.log(this.state.currentWork)

  }

  setRows() {
    let rows = this.state.rows;
    rows.push({ material: this.state.selectedValue + "     " + this.state.selectedTextValue });
    this.setState({ rows });
  }

  removeRows = (i) => {
    let rows = this.state.rows;
    rows.splice(i, 1);
    this.setState({ rows });
  }

  cameraLaunch = () => {

    
    

    let options = {

      storageOptions: {

        skipBackup: true,

        path: 'images',

      },

    };

    ImagePicker.launchCamera(options, (res) => {

      console.log('Response = ', res);



      if (res.didCancel) {

        console.log('User cancelled image picker');

      } else if (res.error) {

        console.log('ImagePicker Error: ', res.error);

      } else if (res.customButton) {

        console.log('User tapped custom button: ', res.customButton);

        alert(res.customButton);

      } else {

        const source = { uri: res.uri };

        console.log("TEST", res.assets[0].uri);

        this.setState({

          filePath: res,

          fileData: res.data,

          fileUri: res.assets[0].uri,


        });

      }

    });

    let photos = this.state.photos;
    photos["photo"] = this.state.fileUri;
    this.setState(photos);
    let rows = this.state.rows;
    rows.push({material: this.state.photos });
    this.setState({ rows });

  }


  

  render() {

    return (
      <>
        <View style={StyleSheet.background}>
          <Header props={this.props} title="Materiali" location={"Materiali"} />
          <View >

            {this.state.rows.map((item, index) => (
              <>
              <Image
                  source={{ uri: item.material}}
                  style={{ width: 100, height: 100 }}
                />
                  <Text style={{ marginLeft: 7 }}>{item.material}</Text>
                  <TouchableHighlight style={{ position: "absolute", right: 7 }}
                    onPress={() => this.removeRows(index)}>
                    <FontAwesomeIcon icon={faTimes} size='20px' style={{ color: '#0083d5' }} />
                  </TouchableHighlight>
                  </>
            ))}
            <View style={{ height: "100%", alignSelf: "center", position: "absolute", top: 520 }}>
              <TouchableHighlight style={{
                width: 250,
                height: 60,
                backgroundColor: '#ff9933',
                alignItems: 'center',
                justifyContent: 'center',
                borderRadius: 4,
                marginBottom: 12,
              }}
                onPress={this.cameraLaunch}   >

                <Text >Slikaj</Text>

              </TouchableHighlight>
              
            </View>


          </View>
          <Footer props={this.props} type={"basic"} location={"Materiali"} />
        </View>

      </>);
  }
};



export default Photos;

