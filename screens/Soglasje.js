import React, { createRef } from 'react';
import {
  View,
  Text,
  TouchableHighlight,
  SafeAreaView,
} from 'react-native';
import StyleSheet from '../Styles/Style';
import Header from './Header';
import Footer from './Footer';
import { Navigation } from "react-native-navigation";


class Soglasje extends React.Component {
  render() {

  return (
    <View style={StyleSheet.background}>
      <Header props={this.props} title="Soglasje" location={"Soglasje"} />
      <Text style={{
            justifyContent: 'center',
            alignItems: 'center',
            margin: 15,
          }}>S podpisom potrjujem točnost vnešenih podatkov delovnega naloga in na to nimam pripomb.</Text>
      <View style={{ flexDirection: 'row' }}>
        <TouchableHighlight
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            height: 50,
            backgroundColor: '#ff9933',
            margin: 10,
            borderRadius:5
          }}
          onPress={() => {
            Navigation.push(this.props.componentId, {
                                                        component: {
                                                            name: 'Podpis',
                                                            options: {
                                                                topBar: {
                                                                    visible: false,
                                                                    title: {
                                                                        text: 'Podpis',
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    })
          }}>
          <Text style={StyleSheet.buttonText}>Se strinjam</Text>
        </TouchableHighlight>
        <TouchableHighlight
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            height: 50,
            backgroundColor: '#ff9933',
            margin: 10,
            borderRadius:5
          }}
          onPress={() => {
            Navigation.push(this.props.componentId, {
                                                        component: {
                                                            name: 'CustomerAnnotations',
                                                            options: {
                                                                topBar: {
                                                                    visible: false,
                                                                    title: {
                                                                        text: 'CustomerAnnotations',
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    })
          }}>
          <Text style={StyleSheet.buttonText}>Opombe</Text>
        </TouchableHighlight>
      </View>


      <Footer props={this.props} type={"basic"} location={"Soglasje"} />
    </View>
  );
}
};
export default Soglasje;
