import { Card, CardTitle, CardContent, CardAction, CardButton, CardImage, ScrollView } from 'react-native-cards';
import React from 'react';

import Logo from './src/Logo';
import StyleSheet from '../Styles/Style';
import { Navigation } from "react-native-navigation";
import {
  SafeAreaView,
  View,
  Text,
  StatusBar,
  Image,
  TextInput,
  type,
  Pressable,
  onPress,
  title,
  Button,
  TouchableHighlight,

} from 'react-native';

const SplashScreen = (props) => {



  setTimeout(() => {


    Navigation.push(props.componentId, {
      component: {
        name: 'LoginScreen',
        options: {
          topBar: {
            visible: false,
            title: {
              text: 'LoginScreen',
            }
          }
        }
      }
    })

  }, 5000);
  return (


    <>
      <View style={StyleSheet.background}>

        <View style={StyleSheet.logoSplash}>
          <Logo />
        </View>

        <Image source={require('./src/splash.gif')} style={StyleSheet.splash} />
      </View>
    </>
  );
}

export default SplashScreen