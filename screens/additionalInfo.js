import React from 'react';
import {
  Picker,
  View,
  Text,
  StatusBar,
  Image,
  type,
  Pressable,
  onPress,
  title,
  AsyncStorage,
  Button,
  FlatList,
  TouchableHighlight,
  TextInput
} from 'react-native';
import { Navigation } from "react-native-navigation";
import StyleSheet from '../Styles/Style';
import Header from './Header';
import Footer from './Footer';
import localStorage from 'react-native-sync-localstorage'
import ToggleSwitch from 'toggle-switch-react-native'

const AditionalInfo = (props) => {
  const [notifications, onChangeNotifications] = React.useState(false);
  const [reminders, onChangeReminders] = React.useState(false);
  const [setting1, onChangeSetting1] = React.useState(false);
  const [setting2, onChangeSetting2] = React.useState(false);
  const [setting3, onChangeSetting3] = React.useState(false);
  const [setting4, onChangeSetting4] = React.useState("Tesnost");

  return (
    <>
      <View style={StyleSheet.background}>
        <Header props={props} title="Dodatni podatki" location={"additionalInfo"} />

        <View style={StyleSheet.switchTextContainer}>
          <View style={StyleSheet.switchTextWrap} >
            <Text style={StyleSheet.switchText}>Čiščenje filtrov, loputk in mask</Text>
            <View style={StyleSheet.switchmargin}>
              <ToggleSwitch
                isOn={reminders}
                onColor="#0083d5"
                offColor="grey"
                style={StyleSheet.switchMarginSwitch}
                size='small'
                onToggle={isOn => onChangeReminders(isOn)}
              />
            </View>
          </View>
          <View style={StyleSheet.switchTextWrap} >
            <Text style={StyleSheet.switchText}>Dezinfekcija upravljalnika N.E.</Text>
            <View style={StyleSheet.switchmargin}>
              <ToggleSwitch
                isOn={setting1}
                onColor="#0083d5"
                offColor="grey"
                style={StyleSheet.switchMarginSwitch}
                size='small'
                onToggle={isOn => onChangeSetting1(isOn)}
              />
            </View>
          </View>
          <View style={StyleSheet.switchTextWrap} >
            <Text style={StyleSheet.switchText}>Test kondenčne povezave</Text>
            <View style={StyleSheet.switchmargin}>
              <ToggleSwitch
                isOn={setting2}
                onColor="#0083d5"
                offColor="grey"
                style={StyleSheet.switchMarginSwitch}
                size='small'
                onToggle={isOn => onChangeSetting2(isOn)}
              />
            </View>
          </View>
          <View style={StyleSheet.switchTextWrap} >
            <Text style={StyleSheet.switchText}>Pregled oziroma čiščenje uparjalnika Z.E.</Text>
            <View style={StyleSheet.switchmargin}>
              <ToggleSwitch
                isOn={setting3}
                onColor="#0083d5"
                offColor="grey"
                style={StyleSheet.switchMarginSwitch}
                size='small'
                onToggle={isOn => onChangeSetting3(isOn)}
              />
            </View>
          </View>
          <TextInput
            style={{
              width: "90%",
              height: 40,
              borderWidth: 0.5,
              marginBottom: "3%",
              borderRadius: 7,
              borderColor: "#bfbfbf",
            }}
            onChangeText={null}
            value={null}
            placeholder="Meritev vpiha (°C)"
          />

          <TextInput
            style={{
              width: "90%",
              height: 40,
              borderWidth: 0.5,
              marginBottom: "3%",
              borderRadius: 7,
              borderColor: "#bfbfbf",
            }}
            onChangeText={null}
            value={null}
            placeholder="Meritev tlaka (bar)"
          />
          <View style={{ flexDirection: "row" }}>
            <Text style={{ fontSize: 16, marginTop: 13, marginRight: 10 }} >Tesnost:</Text>
            <Picker
              selectedValue={setting4}
              style={{ width: 170 }}
              onValueChange={onChangeSetting4}
            >
              <Picker.Item label="Dobra" value="Dobra" />
              <Picker.Item label="Zadovoljiva" value="Zadovoljiva" />
              <Picker.Item label="Slaba" value="Slaba" />
            </Picker>
          </View>


          <View style={{ flexDirection: "row" }}>
            <Text style={{ fontSize: 16, marginTop: 13, marginRight: 10 }} >Čistost:</Text>
            <Picker
              selectedValue={setting4}
              style={{ width: 170 }}
              onValueChange={onChangeSetting4}
            >
              <Picker.Item label="Dobra" value="Dobra" />
              <Picker.Item label="Zadovoljiva" value="Zadovoljiva" />
              <Picker.Item label="Slaba" value="Slaba" />
            </Picker>
          </View>

          <View style={{ flexDirection: "row" }}>
            <Text style={{ fontSize: 16, marginTop: 13, marginRight: 10 }} >Ocena v zd.:</Text>
            <Picker
              selectedValue={setting4}
              style={{ width: 170 }}
              onValueChange={onChangeSetting4}
            >
              <Picker.Item label="Dobra" value="Dobra" />
              <Picker.Item label="Zadovoljiva" value="Zadovoljiva" />
              <Picker.Item label="Slaba" value="Slaba" />
            </Picker>
          </View>
        </View>






        <Footer props={props} type={"basic"} location={"additionalInfo"} />
      </View>
    </>);
};

export default AditionalInfo;