import React, { useState } from 'react'
import { Button } from 'react-native'
import DatePicker from 'react-native-modern-datepicker';
import Dialog from "react-native-dialog";
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faAngleLeft, faCalendar, faSearch } from '@fortawesome/free-solid-svg-icons'
import osnovniPodatki from "./../OsnovniPodatki"
import {
  TouchableHighlight,
} from 'react-native';

export default () => {
  const [visible, setVisible] = useState(false);
  const [date, onChangedate] = React.useState("");


  const showDialog = () => {
    setVisible(true);
  };

  const handleCancel = () => {
    setVisible(false);
    console.log(date)
  };


  return (
    <>
      <TouchableHighlight
              onPress={showDialog}>
              <FontAwesomeIcon icon={faCalendar} size='20px' />
            </TouchableHighlight>
            <Dialog.Container visible={visible}  style={{ width:"90%", height:"80%" }}>
              <Dialog.Title>Spremeni datum</Dialog.Title>
              <Dialog.Description>
              <DatePicker
              style={{ width:300, height:"100%" }}
              value={date}
              onSelectedChange={onChangedate}
            />
              </Dialog.Description>
              <Dialog.Button label="Zapri" onPress={handleCancel} />
              
            </Dialog.Container>
          
    </>
  )
}