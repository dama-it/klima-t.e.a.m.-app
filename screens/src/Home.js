import * as React from 'react';
import { SvgXml } from 'react-native-svg';
import StyleSheet from '../../Styles/Style';
const xml = `
<svg
      xmlns="http://www.w3.org/2000/svg"
      x="0"
      y="0"
      enableBackground="new 0 0 300 300"
      version="1.1"
      viewBox="0 0 300 300"
      xmlSpace="preserve"
    >
      <path
        d="M269.1 114.8L269.1 114.4 149.7 52.2 30.2 114.4 30.2 114.8 30 114.8 30 185.9 30 256 101.1 256 101.1 185.9 198.9 185.9 198.9 256 270 256 270 185.9 270 114.8z" 
        className="st0"
        stroke-width="14px"  stroke="#737373"></path>
    </svg>
`;

const Home = () => {
   return (
     <SvgXml xml={xml} style={StyleSheet.iconDefaultFooter} />
   );
}

export default Home;


