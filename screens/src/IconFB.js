import * as React from 'react';
import { SvgXml } from 'react-native-svg';

const xml = `
<svg
xmlns="http://www.w3.org/2000/svg"
x="0"
y="0"
enableBackground="new 0 0 300 300"
version="1.1"
viewBox="0 0 300 300"
xmlSpace="preserve"
>
<path d="M240.88 29H60.13C43.51 29 30 42.51 30 59.13v180.75C30 256.49 43.51 270 60.13 270h90.38v-82.84h-30.13V149.5h30.13v-30.13c0-24.96 20.23-45.19 45.19-45.19h30.13v37.66h-15.06c-8.31 0-15.06-.78-15.06 7.53v30.13h37.66l-15.06 37.66h-22.59V270h45.19c16.61 0 30.13-13.51 30.13-30.13V59.13C271 42.51 257.49 29 240.88 29z"
fill="#000"></path>
</svg>
`;

const Home = () => {
   return (
     <SvgXml xml={xml} width="30px" height="30px" />
   );
}

export default Home;
