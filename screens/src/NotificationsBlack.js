import * as React from 'react';
import { SvgXml } from 'react-native-svg';
import StyleSheet from '../../Styles/Style';
const xml = `
<svg
xmlns="http://www.w3.org/2000/svg"
x="0"
y="0"
enableBackground="new 0 0 48 48"
version="1.1"
viewBox="0 0 300 300"
xmlSpace="preserve"
>
<path
  d="M266.6 234.9c-28-12.7-29.1-67-29.1-68.3v-36.4c0-35.1-21.4-65.2-51.9-78-.1-18-14.7-32.7-32.8-32.7-18 0-32.7 14.6-32.8 32.7-30.4 12.8-51.9 43-51.9 78v36.4c0 1.4-1.1 55.7-29.1 68.3-3.2 1.4-4.9 4.9-4.2 8.3.7 3.4 3.8 5.9 7.3 5.9h71c1.5 8 5.2 15.5 11 21.5 7.7 8 17.9 12.4 28.6 12.4 10.8 0 21-4.4 28.6-12.4 5.8-6 9.6-13.5 11-21.5h71c3.5 0 6.5-2.4 7.3-5.9 1-3.4-.8-6.9-4-8.3zM152.9 34.3c8.2 0 15.1 5.5 17.2 13-5.6-1.2-11.3-1.8-17.2-1.8-5.9 0-11.7.6-17.2 1.8 2.1-7.6 9-13 17.2-13zm0 233.8c-11.3 0-21.2-8.2-24.4-19.1h48.9c-3.3 11-13.2 19.1-24.5 19.1zM63 234.2c5.9-7.5 10.1-16.4 13-25.4 1.3-5 2.6-9.9 3.9-14.9 3-14.5 3.1-26.3 3.1-27.4V130c0-38.5 31.3-69.8 69.8-69.8s69.8 31.3 69.8 69.8v36.5c0 1.1.2 12.9 3.1 27.3 1.3 5 2.6 9.9 3.9 14.9 2.9 8.9 7.1 17.9 13 25.4H63z "
  className="st0"
  stroke-width="4px" stroke="#000" fill="#000" > </path>
</svg>
`;

const Notifications = () => {
   return (
     <SvgXml xml={xml} style={StyleSheet.iconDefaultFooterNotifications} />
   );
}

export default Notifications;


