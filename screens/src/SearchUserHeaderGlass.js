import * as React from 'react';
import { SvgXml } from 'react-native-svg';
import StyleSheet from '../../Styles/Style';
const xml = `
<svg
      xmlns="http://www.w3.org/2000/svg"
      x="0"
      y="0"
      enableBackground="new 0 0 300 300"
      version="1.1"
      viewBox="0 0 300 300"
      xmlSpace="preserve"
    >
      <path d="M194.29 48.86c-39.82-39.82-104.62-39.82-144.43 0s-39.81 104.62 0 144.44c35.46 35.45 90.68 39.25 130.47 11.57.84 3.96 2.75 7.74 5.83 10.82l57.98 57.98c8.45 8.43 22.1 8.43 30.51 0 8.44-8.44 8.44-22.1 0-30.51l-57.98-58c-3.06-3.06-6.85-4.98-10.81-5.82 27.69-39.79 23.89-95-11.57-130.48zM175.98 175c-29.73 29.73-78.1 29.73-107.82 0-29.71-29.73-29.71-78.09 0-107.82 29.72-29.72 78.09-29.72 107.82 0 29.73 29.72 29.73 78.09 0 107.82z"
       fill="#000"></path>
    </svg>
`;

const SearchUserHeaderGlass = () => {
   return (
     <SvgXml xml={xml} style={StyleSheet.iconDefaultHeaderGlass} />
   );
}

export default SearchUserHeaderGlass;


