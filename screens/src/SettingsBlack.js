import * as React from 'react';
import { SvgXml } from 'react-native-svg';
import StyleSheet from '../../Styles/Style';
const xml = `
<svg
      xmlns="http://www.w3.org/2000/svg"
      x="0"
      y="0"
      enableBackground="new 0 0 612 612"
      version="1.1"
      viewBox="0 0 612 612"
      xmlSpace="preserve"
    >
      <g>
        <path
          d="M71.8 159.9h475.3c8.7 0 15.8-7.1 15.8-15.8 0-8.7-7.1-15.8-15.8-15.8H71.8c-8.7 0-15.8 7.1-15.8 15.8s7.1 15.8 15.8 15.8zM547.2 445.1H71.8c-8.7 0-15.8 7.1-15.8 15.8 0 8.7 7.1 15.8 15.8 15.8h475.3c8.7 0 15.8-7.1 15.8-15.8.1-8.7-7-15.8-15.7-15.8zM499.6 239.2c-29.5 0-54.1 20.2-61.1 47.5H71.8c-8.7 0-15.8 7.1-15.8 15.8s7.1 15.8 15.8 15.8h366.6c7.1 27.3 31.6 47.5 61.1 47.5 35 0 63.4-28.4 63.4-63.4s-28.3-63.2-63.3-63.2zm0 95c-17.5 0-31.7-14.2-31.7-31.7s14.2-31.7 31.7-31.7 31.7 14.2 31.7 31.7-14.2 31.7-31.7 31.7z"
          className="st0"
          stroke-width="6px" stroke="#000" fill="#000"></path>
      </g>
    </svg>
`;

const Settings = () => {
   return (
     <SvgXml xml={xml} style={StyleSheet.iconDefaultFooterSettings} />
   );
}

export default Settings;


