import * as React from 'react';
import { SvgXml } from 'react-native-svg';

const xml = `
<svg
      xmlns="http://www.w3.org/2000/svg"
      x="0"
      y="0"
      enableBackground="new 0 0 300 300"
      version="1.1"
      viewBox="0 0 300 300"
      xmlSpace="preserve"
    >
      <path
        d="M299.76 150.76C299.76 68.05 232.71 1 150 1S.24 68.05.24 150.76C.24 225.51 55 287.46 126.6 298.7V194.05H88.58v-43.29h38.02v-32.99c0-37.53 22.36-58.26 56.56-58.26 16.38 0 33.52 2.92 33.52 2.92v36.85H197.8c-18.6 0-24.4 11.54-24.4 23.39v28.09h41.53l-6.64 43.29H173.4V298.7c71.59-11.24 126.36-73.2 126.36-147.94z"
        className="st0"
      fill='#1877F2'></path>
    </svg>
`;

const Home = () => {
   return (
     <SvgXml xml={xml} width="30px" height="30px" />
   );
}

export default Home;
