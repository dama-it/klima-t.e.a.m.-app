import * as React from 'react';
import { SvgXml } from 'react-native-svg';

const xml = `
<?xml version="1.0" encoding="utf-8"?>
<!-- Generator: Adobe Illustrator 23.0.1, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg
      xmlns="http://www.w3.org/2000/svg"
      x="0"
      y="0"
      enableBackground="new 0 0 300 300"
      version="1.1"
      viewBox="0 0 300 300"
      xmlSpace="preserve"
    >
      <path
        d="M67.6 180.88l-10.3 38.46-37.66.8C8.38 199.26 2 175.38 2 150c0-24.54 5.97-47.69 16.55-68.06h.01l33.53 6.15 14.69 33.33a88.063 88.063 0 00-4.75 28.59 87.403 87.403 0 005.57 30.87z"
        className="st0"
      fill='#F14336' fill='#FBBB00'></path>
      <path
        d="M295.41 122.35c1.7 8.95 2.59 18.2 2.59 27.65 0 10.6-1.11 20.93-3.24 30.9-7.2 33.93-26.03 63.55-52.11 84.51l-.01-.01-42.23-2.15-5.98-37.31c17.3-10.15 30.83-26.03 37.95-45.04h-79.14v-58.55H295.41z"
        className="st1"
        fill='#518EF8'></path>
      <path
        d="M242.65 265.41C217.29 285.8 185.07 298 150 298c-56.36 0-105.36-31.5-130.36-77.86l47.96-39.26c12.5 33.36 44.68 57.1 82.4 57.1 16.21 0 31.41-4.38 44.44-12.04l48.21 39.47z"
        className="st2"
        fill='#28B446'></path>
      <path
        d="M244.47 36.07l-47.95 39.25c-13.49-8.43-29.44-13.3-46.52-13.3-38.58 0-71.36 24.83-83.23 59.39L18.56 81.94h-.01C43.18 34.45 92.8 2 150 2c35.91 0 68.84 12.79 94.47 34.07z"
        className="st3"
        fill='#F14336'></path>
    </svg>

`;

const Home = () => {
   return (
     <SvgXml xml={xml} width="30px" height="30px" />
   );
}

export default Home;
