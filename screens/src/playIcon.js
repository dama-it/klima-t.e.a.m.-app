import * as React from 'react';
import { SvgXml } from 'react-native-svg';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import StyleSheet from '../../Styles/Style';

const xml = `
<svg
      xmlns="http://www.w3.org/2000/svg"
      x="0"
      y="0"
      enableBackground="new 0 0 48 48"
      version="1.1"
      viewBox="0 0 612 612"
      xmlSpace="preserve"
    >
      <path
        d="M477 132c-45.8-45.8-106.7-71-171.5-71S179.8 86.2 134 132 63 238.7 63 303.5 88.2 429.2 134 475s106.7 71 171.5 71 125.7-25.2 171.5-71 71-106.7 71-171.5-25.2-125.7-71-171.5zM305.5 516C188.3 516 93 420.7 93 303.5S188.3 91 305.5 91 518 186.3 518 303.5 422.7 516 305.5 516z"
        className="st0" stroke="#000" stroke-width="20px" fill="#000"
      ></path>
      <path d="M244.1 397.6L406.9 303.5 244.1 209.4z" className="st0" fill="#000"></path>
    </svg>
`;

const PlayIcon = () => {
   return (
     <SvgXml xml={xml} style={StyleSheet.iconDefault} />
   );
}

export default PlayIcon;


