import { SET_PROFILE_ACHIEVEMENTS, ADD_PROFILE_ACHIEVEMENTS } from './actionTypes';

export const setProfileAchievements = achievements => {
  return {
    type: SET_PROFILE_ACHIEVEMENTS,
    achievements: achievements
  };
};

export const addProfileAchievements = achievements => {
  return {
    type: ADD_PROFILE_ACHIEVEMENTS,
    achievements: achievements
  };
};
