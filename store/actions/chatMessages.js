import { ADD_CHAT_MESSAGES, SET_CHAT_MESSAGES, SET_UNREAD_COUNTER, PUSH_CHAT_MESSAGE } from '../actions/actionTypes';

export const setChatMessages = chatMessages => {
  return {
    type: SET_CHAT_MESSAGES,
    chatMessages: chatMessages
  };
};

export const addChatMessages = chatMessages => {
  return {
    type: ADD_CHAT_MESSAGES,
    chatMessages: chatMessages
  };
};

export const setUnreadCounter = unreadChatMessages => {
  return {
    type: SET_UNREAD_COUNTER,
    unreadChatMessages: unreadChatMessages
  };
};

export const pushChatMssage = chatMessage => {
  return {
    type: PUSH_CHAT_MESSAGE,
    chatMessage: chatMessage
  };
};
