import { SET_CLUB_MEMBERS, ADD_CLUB_MEMBERS, SET_CLUB, ADD_CLUB_EVENTS, SET_CLUB_EVENTS, SET_CLUBS, ADD_CLUBS } from './actionTypes';

export const setClubs = clubs => {
  return {
    type: SET_CLUBS,
    list: clubs
  };
};

export const addClubs = clubs => {
  return {
    type: ADD_CLUBS,
    list: clubs
  };
};

export const setClubMembers = clubMembers => {
  return {
    type: SET_CLUB_MEMBERS,
    clubMembers: clubMembers
  };
};

export const addClubMembers = clubMembers => {
  return {
    type: ADD_CLUB_MEMBERS,
    clubMembers: clubMembers
  };
};

export const setClubEvents = events => {
  return {
    type: SET_CLUB_EVENTS,
    events: events
  };
};

export const addClubEvents = events => {
  return {
    type: ADD_CLUB_EVENTS,
    events: events
  };
};

export const setClub = club => {
  return {
    type: SET_CLUB,
    club: club
  };
};
