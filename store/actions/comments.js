import { SET_COMMENTS, ADD_COMMENTS, SELECT_COMMENT, DELETE_COMMENT, DOWN_VOTE_COMMENT, UP_VOTE_COMMENT, SET_PROFILE_COMMENTS, ADD_PROFILE_COMMENTS } from './actionTypes';

export const setComments = comments => {
  return {
    type: SET_COMMENTS,
    comments: comments
  };
};

export const addComments = comments => {
  return {
    type: ADD_COMMENTS,
    comments: comments
  };
};

export const setProfileComments = comments => {
  return {
    type: SET_PROFILE_COMMENTS,
    comments: comments
  };
};

export const addProfileComments = comments => {
  return {
    type: ADD_PROFILE_COMMENTS,
    comments: comments
  };
};

export const selectComment = comment => {
  return {
    type: SELECT_COMMENT,
    comment: comment
  };
};

export const upVoteCommentAction = comment => {
  return {
    type: UP_VOTE_COMMENT,
    comment: comment
  };
};

export const downVoteCommentAction = comment => {
  return {
    type: DOWN_VOTE_COMMENT,
    comment: comment
  };
};

export const deleteCommentAction = comment => {
  return {
    type: DELETE_COMMENT,
    comment: comment
  };
};
