import {
  SET_COMPETITION_TYPE,
  SET_COMPETITION_POST_BANNER_PATH,
  SET_COMPETITION_CONTENT,
  SET_COMPETITION_GOAL,
  SET_COMPETITION_TIME_UNIT,
  SET_COMPETITION_TIME_VALUE,
  SET_COMPETITION_TITLE,
  RESET_COMPETITION,
  SET_COMPETITION_UNIT,
  SET_COMPETITION_SIGN,
  SET_COMPETITION_START_VALUE,
  SET_COMPETITION_DIFFERENCE,
  SET_COMPETITION_TOPIC,
  SET_COMPETITION
} from './actionTypes/';

export const setCompetition = post => {
  return {
    type: SET_COMPETITION,
    post: post
  };
};

export const setCompetitionTopic = topic => {
  return {
    type: SET_COMPETITION_TOPIC,
    topic: topic
  };
};

export const setCompetitionType = competitionType => {
  return {
    type: SET_COMPETITION_TYPE,
    competitionType: competitionType
  };
};

export const setCompetitionPostBannerPath = postBannerPath => {
  return {
    type: SET_COMPETITION_POST_BANNER_PATH,
    postBannerPath: postBannerPath
  };
};

export const setCompetitionContent = content => {
  return {
    type: SET_COMPETITION_CONTENT,
    content: content
  };
};

export const setCompetitionGoal = goal => {
  return {
    type: SET_COMPETITION_GOAL,
    goal: goal
  };
};

export const setCompetitionTimeUnit = timeUnit => {
  return {
    type: SET_COMPETITION_TIME_UNIT,
    timeUnit: timeUnit
  };
};

export const setCompetitionTimeValue = timeValue => {
  return {
    type: SET_COMPETITION_TIME_VALUE,
    timeValue: timeValue
  };
};

export const setCompetitionTitle = title => {
  return {
    type: SET_COMPETITION_TITLE,
    title: title
  };
};

export const setCompetitionUnit = unit => {
  return {
    type: SET_COMPETITION_UNIT,
    unit: unit
  };
};

export const setCompetitionSign = sign => {
  return {
    type: SET_COMPETITION_SIGN,
    sign: sign
  };
};

export const setCompetitionDifference = difference => {
  return {
    type: SET_COMPETITION_DIFFERENCE,
    difference: difference
  };
};

export const setCompetitionStartValue = startValue => {
  return {
    type: SET_COMPETITION_START_VALUE,
    startValue: startValue
  };
};

export const resetCompetition = () => {
  return {
    type: RESET_COMPETITION
  };
};
