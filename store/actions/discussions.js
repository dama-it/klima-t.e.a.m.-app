import {
  SET_DISCUSSIONS,
  ADD_DISCUSSIONS,
  ADD_ARCHIVED_DISCUSSIONS,
  SET_ARCHIVED_DISCUSSIONS,
  SYNC_DISCUSSION,
  SET_CURRENT_DISCUSSION_ID,
  UPDATE_DISCUSSION_LATEST_MESSAGE,
  READ_DISCUSSION,
  ARCHIVE_DISCUSSION,
  UN_ARCHIVE_DISCUSSION,
  BLOCK_DISCUSSION,
  UN_BLOCK_DISCUSSION,
  BLOCK_AND_ARCHIVE_DISCUSSION
} from '../actions/actionTypes';

export const setDiscussions = discussions => {
  return {
    type: SET_DISCUSSIONS,
    discussions: discussions
  };
};

export const addDiscussions = discussions => {
  return {
    type: ADD_DISCUSSIONS,
    discussions: discussions
  };
};

export const setArchivedDiscussions = discussions => {
  return {
    type: SET_ARCHIVED_DISCUSSIONS,
    discussions: discussions
  };
};

export const addArchivedDiscussions = discussions => {
  return {
    type: ADD_ARCHIVED_DISCUSSIONS,
    discussions: discussions
  };
};

export const archiveDiscussion = discussion => {
  return {
    type: ARCHIVE_DISCUSSION,
    discussion: discussion
  };
};

export const unArchiveDiscussion = discussion => {
  return {
    type: UN_ARCHIVE_DISCUSSION,
    discussion: discussion
  };
};

export const syncDiscussion = discussion => {
  return {
    type: SYNC_DISCUSSION,
    discussion: discussion
  };
};

export const setCurrentDiscussionId = currentDiscussionId => {
  return {
    type: SET_CURRENT_DISCUSSION_ID,
    currentDiscussionId: currentDiscussionId
  };
};

export const updateDiscussionLatestMessage = (discussionId, latestMessage, latestMessageAt) => {
  return {
    type: UPDATE_DISCUSSION_LATEST_MESSAGE,
    latestMessage: latestMessage,
    discussionId: discussionId,
    latestMessageAt: latestMessageAt
  };
};

export const readDiscussion = (discussionId) => {
  return {
    type: READ_DISCUSSION,
    discussionId: discussionId
  };
};

export const blockDiscussion = (discussion) => {
  return {
    type: BLOCK_DISCUSSION,
    discussion: discussion
  };
};

export const blockAndArchiveDiscussion = (discussion) => {
  return {
    type: BLOCK_AND_ARCHIVE_DISCUSSION,
    discussion: discussion
  };
};

export const unBlockDiscussion = (discussion) => {
  return {
    type: UN_BLOCK_DISCUSSION,
    discussion: discussion
  };
};
