import { UPDATE_ERROR } from './actionTypes';

export const updateError = errorData => {
  return {
    type: UPDATE_ERROR,
    error: errorData
  };
};
