import { SET_FAVOURITE_TOPICS, ADD_FAVOURITE_TOPICS } from './actionTypes';

export const setFavouriteTopics = favouriteTopics => {
  return {
    type: SET_FAVOURITE_TOPICS,
    favouriteTopics: favouriteTopics
  };
};

export const addFavouriteTopics = favouriteTopics => {
  return {
    type: ADD_FAVOURITE_TOPICS,
    favouriteTopics: favouriteTopics
  };
};
