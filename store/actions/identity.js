import {
  UPDATE_IDENTITY, CLEAR_IDENTITY, INCREMENT_NEW_CHAT_MESSAGES_COUNT
} from './actionTypes';

export const updateIdentity = data => {
  return {
    type: UPDATE_IDENTITY,
    data: data
  };
};

export const clearIdentity = () => {
  return {
    type: CLEAR_IDENTITY
  };
};

export const incrementNewChatMessagesCounter = () => {
  return {
    type: INCREMENT_NEW_CHAT_MESSAGES_COUNT
  };
};
