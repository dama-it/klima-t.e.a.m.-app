import {
  SET_MESSAGES,
  SET_NEW_MESSAGES,
  ADD_MESSAGES,
  READ_MESSAGE,
  READ_ALL_MESSAGES,
  REMOVE_LAST_MESSAGE
} from './actionTypes';

export const setMessages = messages => {
  return {
    type: SET_MESSAGES,
    messages: messages
  };
};

export const addMessages = messages => {
  return {
    type: ADD_MESSAGES,
    messages: messages
  };
};

export const setNewMessages = newMessages => {
  return {
    type: SET_NEW_MESSAGES,
    newMessages: newMessages
  };
};

export const readMessage = message => {
  return {
    type: READ_MESSAGE,
    message: message
  };
};

export const readAllMessages = () => {
  return {
    type: READ_ALL_MESSAGES
  };
};

export const removeLastMessage = () => {
  return {
    type: REMOVE_LAST_MESSAGE
  };
};
