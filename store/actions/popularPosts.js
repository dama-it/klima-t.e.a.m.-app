import { SET_POPULAR_POSTS, ADD_POPULAR_POSTS, SELECT_POPULAR_POST, SYNC_POPULAR_POST, SET_FAV_POPULAR_POSTS, SYNC_FAV_POPULAR_POST, ADD_FAV_POPULAR_POSTS, FAV_POPULAR_POST, DOWN_VOTE_POPULAR_POST, UP_VOTE_POPULAR_POST } from './actionTypes';

export const setPopularPosts = posts => {
  return {
    type: SET_POPULAR_POSTS,
    posts: posts
  };
};
export const setFavPopularPosts = posts => {
  return {
    type: SET_FAV_POPULAR_POSTS,
    posts: posts
  };
};

export const addPopularPosts = posts => {
  return {
    type: ADD_POPULAR_POSTS,
    posts: posts
  };
};

export const addFavPopularPost = post => {
  return {
    type: ADD_FAV_POPULAR_POSTS,
    post: post
  };
};

export const selectPopularPost = post => {
  return {
    type: SELECT_POPULAR_POST,
    post: post
  };
};

export const syncPopularPost = post => {
  return {
    type: SYNC_POPULAR_POST,
    post: post
  };
};

export const syncFavPopularPost = post => {
  return {
    type: SYNC_FAV_POPULAR_POST,
    post: post
  };
};

export const favPopularPost = post => {
  return {
    type: FAV_POPULAR_POST,
    post: post
  };
};

export const downVotePopularPost = post => {
  return {
    type: DOWN_VOTE_POPULAR_POST,
    post: post
  };
};

export const upVotePopularPost = post => {
  return {
    type: UP_VOTE_POPULAR_POST,
    post: post
  };
};
