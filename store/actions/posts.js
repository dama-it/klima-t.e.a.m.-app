import {
  SET_POSTS,
  ADD_POSTS,
  SELECT_POST,
  SYNC_POST,
  SET_FAV_POSTS,
  SYNC_FAV_POST,
  ADD_FAV_POSTS,
  INCREMENT_COMMENT_COUNT,
  UP_VOTE_POST,
  DOWN_VOTE_POST,
  FAVOURITE_POST,
  ADD_POST_AT_TOP,
  DELETE_POST,
  SET_PROFILE_POSTS,
  ADD_PROFILE_POSTS
} from './actionTypes';

export const setPosts = posts => {
  return {
    type: SET_POSTS,
    posts: posts
  };
};

export const setFavPosts = posts => {
  return {
    type: SET_FAV_POSTS,
    posts: posts
  };
};

export const addPosts = posts => {
  return {
    type: ADD_POSTS,
    posts: posts
  };
};

export const addFavPosts = posts => {
  return {
    type: ADD_FAV_POSTS,
    posts: posts
  };
};

export const incrementMessageCount = () => {
  return {
    type: INCREMENT_COMMENT_COUNT
  };
};

export const selectPost = post => {
  return {
    type: SELECT_POST,
    post: post
  };
};

export const syncPost = post => {
  return {
    type: SYNC_POST,
    post: post
  };
};

export const syncFavPost = post => {
  return {
    type: SYNC_FAV_POST,
    post: post
  };
};

export const upVotePost = post => {
  return {
    type: UP_VOTE_POST,
    post: post
  };
};

export const downVotePost = post => {
  return {
    type: DOWN_VOTE_POST,
    post: post
  };
};

export const favouritePost = post => {
  return {
    type: FAVOURITE_POST,
    post: post
  };
};

export const addPostAtTop = post => {
  return {
    type: ADD_POST_AT_TOP,
    post: post
  };
};

export const deletePostAction = postId => {
  return {
    type: DELETE_POST,
    postId: postId
  };
};

export const setProfilePosts = posts => {
  return {
    type: SET_PROFILE_POSTS,
    posts: posts
  };
};

export const addProfilePosts = posts => {
  return {
    type: ADD_PROFILE_POSTS,
    posts: posts
  };
};
