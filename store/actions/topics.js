import { SELECT_TOPIC_SCREEN, SET_FAVOURITE_TOPICS, ADD_FAVOURITE_TOPICS, SET_TOPICS, ADD_TOPICS, ADD_COMPETITION_TO_TOPIC, ADD_POST_TO_TOPIC, SELECT_TOPIC, SYNC_TOPIC, SET_TOPIC_POSTS, ADD_TOPIC_POSTS, SYNC_TOPIC_POST, SET_TOPIC_COMPETITIONS, ADD_TOPIC_COMPETITIONS, FAVOURITE_TOPIC, FAVOURITE_TOPIC_POST, DOWN_VOTE_TOPIC_POST, UP_VOTE_TOPIC_POST, DELETE_TOPIC_POST } from './actionTypes';

export const setTopics = topics => {
  return {
    type: SET_TOPICS,
    topics: topics
  };
};

export const addTopics = topics => {
  return {
    type: ADD_TOPICS,
    topics: topics
  };
};

export const addPostToTopic = post => {
  return {
    type: ADD_POST_TO_TOPIC,
    post: post
  };
};

export const addCompetitionToTopic = competition => {
  return {
    type: ADD_COMPETITION_TO_TOPIC,
    competition: competition
  };
};

export const setTopicPosts = posts => {
  return {
    type: SET_TOPIC_POSTS,
    posts: posts
  };
};

export const addTopicPosts = posts => {
  return {
    type: ADD_TOPIC_POSTS,
    posts: posts
  };
};

export const setTopicCompetitions = posts => {
  return {
    type: SET_TOPIC_COMPETITIONS,
    posts: posts
  };
};

export const addTopicCompetitions = posts => {
  return {
    type: ADD_TOPIC_COMPETITIONS,
    posts: posts
  };
};

export const selectTopic = topic => {
  return {
    type: SELECT_TOPIC,
    topic: topic
  };
};

export const syncTopic = topic => {
  return {
    type: SYNC_TOPIC,
    topic: topic
  };
};

export const syncTopicPost = post => {
  return {
    type: SYNC_TOPIC_POST,
    post: post
  };
};

export const favouriteTopic = topic => {
  return {
    type: FAVOURITE_TOPIC,
    topic: topic
  };
};

export const favouriteTopicPost = post => {
  return {
    type: FAVOURITE_TOPIC_POST,
    post: post
  };
};

export const upVoteTopicPost = post => {
  return {
    type: UP_VOTE_TOPIC_POST,
    post: post
  };
};

export const downVoteTopicPost = post => {
  return {
    type: DOWN_VOTE_TOPIC_POST,
    post: post
  };
};

export const selectTopicScreen = selected => {
  return {
    type: SELECT_TOPIC_SCREEN,
    selected: selected
  };
};

export const deleteTopicPostAction = postId => {
  return {
    type: DELETE_TOPIC_POST,
    postId: postId
  };
};
