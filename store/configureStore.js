import { createStore, combineReducers, compose } from 'redux';
import errorReducer from './reducers/error';
import postsReducer from './reducers/posts';
import commentsReducer from './reducers/comments';
import popularPostsReducer from './reducers/popularPosts';
import identityReducer from './reducers/identity';
import topicReducer from './reducers/topics';
import favouriteTopicsReducer from './reducers/favouriteTopics';
import achievementsReducer from './reducers/achievements';
import competitionReducer from './reducers/competition';
import messagesReducer from './reducers/messages';
import discussionsReducer from './reducers/discussions';
import chatMessagesReducer from './reducers/chatMessages';
import clubsReducer from './reducers/clubs';

import { reducer as formReducer } from 'redux-form';

// TODO: add custom reducers...
const rootReducer = combineReducers({
  error: errorReducer,
  posts: postsReducer,
  comments: commentsReducer,
  achievements: achievementsReducer,
  popularPosts: popularPostsReducer,
  form: formReducer,
  identity: identityReducer,
  topic: topicReducer,
  competition: competitionReducer,
  messages: messagesReducer,
  discussions: discussionsReducer,
  chatMessages: chatMessagesReducer,
  clubs: clubsReducer,
  favouriteTopics: favouriteTopicsReducer
});

let composeEnhancers = compose;
const reduxEnhancer = compose;
const enableEnhancer = true;

// eslint-disable-next-line no-undef
if (enableEnhancer && __DEV__) {
  composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
}

const store = createStore(
  rootReducer,

  composeEnhancers()
  // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()

);

export default store;
