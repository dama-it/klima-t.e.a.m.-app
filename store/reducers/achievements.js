import { SET_PROFILE_ACHIEVEMENTS, ADD_PROFILE_ACHIEVEMENTS } from '../actions/actionTypes/';

const INITIAL_STATE = {
  profileList: []
};

const addAchievements = (achievements, newAcheivements) => {
  return newAcheivements && newAcheivements.length > 0 ? achievements.concat(newAcheivements) : achievements;
};

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_PROFILE_ACHIEVEMENTS:
      return {
        profileList: action.achievements
      };
    case ADD_PROFILE_ACHIEVEMENTS:
      return {
        profileList: addAchievements(state.profileList, action.achievements)
      };

    default:
      return state;
  }
};

export default reducer;
