import { SET_CHAT_MESSAGES, ADD_CHAT_MESSAGES, SET_UNREAD_COUNTER, PUSH_CHAT_MESSAGE } from '../actions/actionTypes';

const INITIAL_STATE = {
  list: [],
  unreadChatMessages: 0
};

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_CHAT_MESSAGES:
      return {
        ...state,
        list: action.chatMessages
      };
    case ADD_CHAT_MESSAGES:
      return {
        ...state,
        list: [...state.list, ...action.chatMessages]
      };
    case SET_UNREAD_COUNTER:
      return {
        ...state,
        unreadChatMessages: action.unreadChatMessages
      };
    case PUSH_CHAT_MESSAGE:
      return {
        ...state,
        list: [action.chatMessage, ...state.list]
      };
    default:
      return state;
  }
};

export default reducer;
