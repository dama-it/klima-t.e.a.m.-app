import {
  ADD_CLUB_MEMBERS,
  SET_CLUB_MEMBERS,
  SET_CLUB,
  SET_CLUB_EVENTS,
  ADD_CLUB_EVENTS,
  ADD_CLUBS,
  SET_CLUBS
} from '../actions/actionTypes';

const INITIAL_STATE = {
  currentClub: null,
  clubMembers: [],
  events: [],
  list: []
};

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_CLUBS:
      return {
        ...state,
        list: action.list
      };
    case ADD_CLUBS:
      return {
        ...state,
        list: [...action.list, ...state.list]
      };
    case SET_CLUB_MEMBERS:
      return {
        ...state,
        clubMembers: action.clubMembers
      };
    case ADD_CLUB_MEMBERS:
      return {
        ...state,
        clubMembers: [...action.clubMembers, ...state.clubMembers]
      };
    case SET_CLUB_EVENTS:
      return {
        ...state,
        events: action.events
      };
    case ADD_CLUB_EVENTS:
      return {
        ...state,
        events: [...action.events, ...state.events]
      };
    case SET_CLUB:
      return {
        ...state,
        currentClub: action.club
      };

    default:
      return state;
  }
};

export default reducer;
