import {
  SET_COMMENTS,
  ADD_COMMENTS,
  SELECT_COMMENT,
  SYNC_COMMENT,
  DELETE_COMMENT,
  UP_VOTE_COMMENT,
  DOWN_VOTE_COMMENT,
  ADD_PROFILE_COMMENTS,
  SET_PROFILE_COMMENTS
} from '../actions/actionTypes/';

const INITIAL_STATE = {
  list: [],
  profileList: [],
  favList: [],
  currentComment: null,
  counter: 0
};

const addComments = (comments, newComments) => {
  const ps = newComments && newComments.length > 0 ? comments.concat(newComments) : comments;
  return ps;
};

const updateComments = (comments, comment) => {
  const objIndex = comments.findIndex(obj => obj.id === comment.id);
  comments[objIndex] = comment;
  return comments;
};

const updateCurrentComment = (comment, newComment) => {
  if (comment && comment.id === newComment.id) {
    return newComment;
  }
  return comment;
};

const upVoteListComment = (comments, upVotedComment) => {
  const newComments = comments.map(comment => {
    if (comment.id === upVotedComment.id) {
      return upVoteComment(upVotedComment);
    }
    return comment;
  });
  return newComments;
};

const downVoteListComment = (comments, downVotedComment) => {
  const newComments = comments.map(comment => {
    if (comment.id === downVotedComment.id) {
      return downVoteComment(downVotedComment);
    }
    return comment;
  });
  return newComments;
};

const upVoteComment = (comment) => {
  const newComment = comment;
  if (comment.currentUsersVote == null) {
    newComment.upVotesCount++;
    newComment.currentUsersVote = true;
  }
  if (comment.currentUsersVote === false) {
    newComment.downVotesCount--;
    newComment.upVotesCount++;
    newComment.currentUsersVote = true;
  }
  if (comment.currentUsersVote === true) {
    return newComment;
  }
  return newComment;
};

const downVoteComment = (comment) => {
  const newComment = comment;
  if (comment.currentUsersVote == null) {
    newComment.downVotesCount++;
    newComment.currentUsersVote = false;
  }
  if (comment.currentUsersVote === true) {
    newComment.downVotesCount++;
    newComment.upVotesCount--;
    newComment.currentUsersVote = false;
  }
  if (comment.currentUsersVote === false) {
    return newComment;
  }
  return newComment;
};

const deleteComment = comment => {
  comment.deletedAt = Date.now().toLocaleString();
  comment.text = null;
  return comment;
};

const deleteListComment = (profileList, toDeleteComment) => {
  const newComments = profileList.map(comment => {
    if (comment.id === toDeleteComment.id) {
      return deleteComment(toDeleteComment);
    }
    return comment;
  });
  return newComments;
};

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_COMMENTS:
      return {
        list: action.comments,
        profileList: state.profileList,
        favList: state.favList,
        currentPost: state.currentPost,
        counter: state.counter
      };
    case SET_PROFILE_COMMENTS:
      return {
        list: state.list,
        profileList: action.comments,
        favList: state.favList,
        currentPost: state.currentPost,
        counter: state.counter
      };
    case ADD_COMMENTS:
      return {
        list: addComments(state.list, action.comments),
        profileList: state.profileList,
        favList: state.favList,
        currentComment: state.currentComment,
        counter: state.counter
      };
    case ADD_PROFILE_COMMENTS:
      return {
        list: state.list,
        profileList: addComments(state.profileList, action.comments),
        favList: state.favList,
        currentComment: state.currentComment,
        counter: state.counter
      };
    case SELECT_COMMENT:
      return {
        currentComment: action.comment,
        favList: state.favList,
        list: state.list,
        profileList: state.profileList,
        counter: state.counter
      };
    case SYNC_COMMENT:
      return {
        currentComment: updateCurrentComment(state.currentComment, action.comment),
        list: updateComments(state.list, action.comment),
        profileList: state.profileList,
        favList: state.favList,
        counter: ++state.counter
      };
    case UP_VOTE_COMMENT:
      return {
        currentComment: upVoteComment(action.comment),
        list: upVoteListComment(state.list, action.comment),
        profileList: upVoteListComment(state.profileList, action.comment),
        favList: state.favList,
        counter: state.counter
      };

    case DOWN_VOTE_COMMENT:
      return {
        currentComment: downVoteComment(action.comment),
        list: downVoteListComment(state.list, action.comment),
        profileList: downVoteListComment(state.profileList, action.comment),
        favList: state.favList,
        counter: state.counter
      };

    case DELETE_COMMENT:
      return {
        currentComment: deleteComment(action.comment),
        favList: state.favList,
        list: deleteListComment(state.list, action.comment),
        profileList: deleteListComment(state.profileList, action.comment),
        counter: state.counter
      };
    default:
      return state;
  }
};

export default reducer;
