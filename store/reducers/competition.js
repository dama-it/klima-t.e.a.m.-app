import {
  SET_COMPETITION_TYPE,
  SET_COMPETITION_POST_BANNER_ID,
  SET_COMPETITION_CONTENT,
  SET_COMPETITION_GOAL,
  SET_COMPETITION_TIME_UNIT,
  SET_COMPETITION_TIME_VALUE,
  SET_COMPETITION_TITLE,
  SET_COMPETITION_UNIT,
  SET_COMPETITION_START_VALUE,
  SET_COMPETITION_DIFFERENCE,
  SET_COMPETITION_SIGN,
  RESET_COMPETITION,
  SET_COMPETITION,
  SET_COMPETITION_TOPIC
} from '../actions/actionTypes/';
import { SET_COMPETITION_POST_BANNER_PATH } from '../actions/actionTypes';

const INITIAL_STATE = {
  competitionType: null,
  postBannerPath: null,
  content: null,
  goal: null,
  timeUnit: 'days',
  timeValue: 14,
  title: null,
  unit: null,
  startValue: null,
  difference: null,
  sign: null,
  id: null,
  topic: null
};

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_COMPETITION:
      return {
        competitionType: action.post.competition.type,
        postBannerPath: action.post.banner ? action.post.banner.path : null,
        content: action.post.content,
        goal: action.post.competition.goal,
        timeUnit: action.post.competition.timeUnit,
        timeValue: action.post.competition.timeAmount,
        title: action.post.title,
        unit: action.post.competition.unit,
        startValue: action.post.competition.startValue,
        difference: Math.abs(action.post.competition.startValue - action.post.competition.endValue),
        sign: action.post.competition.startValue < action.post.competition.endValue,
        id: action.post.id,
        topic: action.post.topic
      };
    case SET_COMPETITION_TOPIC:
      return {
        competitionType: state.competitionType,
        postBannerPath: state.postBannerPath,
        content: state.content,
        goal: state.goal,
        timeUnit: state.timeUnit,
        timeValue: state.timeValue,
        title: state.title,
        unit: state.unit,
        startValue: state.startValue,
        difference: state.difference,
        sign: state.sign,
        id: state.id,
        topic: action.topic
      };
    case SET_COMPETITION_TYPE:
      return {
        competitionType: action.competitionType,
        postBannerPath: state.postBannerPath,
        content: state.content,
        goal: state.goal,
        timeUnit: state.timeUnit,
        timeValue: state.timeValue,
        title: state.title,
        unit: state.unit,
        startValue: state.startValue,
        difference: state.difference,
        sign: state.sign,
        id: state.id,
        topic: state.topic
      };
    case SET_COMPETITION_POST_BANNER_PATH:
      return {
        competitionType: state.competitionType,
        postBannerPath: action.postBannerPath,
        content: state.content,
        goal: state.goal,
        timeUnit: state.timeUnit,
        timeValue: state.timeValue,
        title: state.title,
        unit: state.unit,
        startValue: state.startValue,
        difference: state.difference,
        sign: state.sign,
        id: state.id,
        topic: state.topic
      };
    case SET_COMPETITION_CONTENT:
      return {
        competitionType: state.competitionType,
        postBannerPath: state.postBannerPath,
        content: action.content,
        goal: state.goal,
        timeUnit: state.timeUnit,
        timeValue: state.timeValue,
        title: state.title,
        unit: state.unit,
        startValue: state.startValue,
        difference: state.difference,
        sign: state.sign,
        id: state.id,
        topic: state.topic
      };
    case SET_COMPETITION_GOAL:
      return {
        competitionType: state.competitionType,
        postBannerPath: state.postBannerPath,
        content: state.content,
        goal: action.goal,
        timeUnit: state.timeUnit,
        timeValue: state.timeValue,
        title: state.title,
        unit: state.unit,
        startValue: state.startValue,
        difference: state.difference,
        sign: state.sign,
        id: state.id,
        topic: state.topic
      };
    case SET_COMPETITION_TIME_UNIT:
      return {
        competitionType: state.competitionType,
        postBannerPath: state.postBannerPath,
        content: state.content,
        goal: state.goal,
        timeUnit: action.timeUnit,
        timeValue: state.timeValue,
        title: state.title,
        unit: state.unit,
        startValue: state.startValue,
        difference: state.difference,
        sign: state.sign,
        id: state.id,
        topic: state.topic
      };
    case SET_COMPETITION_TIME_VALUE:
      return {
        competitionType: state.competitionType,
        postBannerPath: state.postBannerPath,
        content: state.content,
        goal: state.goal,
        timeUnit: state.timeUnit,
        timeValue: action.timeValue,
        title: state.title,
        unit: state.unit,
        startValue: state.startValue,
        difference: state.difference,
        sign: state.sign,
        id: state.id,
        topic: state.topic
      };
    case SET_COMPETITION_TITLE:
      return {
        competitionType: state.competitionType,
        postBannerPath: state.postBannerPath,
        content: state.content,
        goal: state.goal,
        timeUnit: state.timeUnit,
        timeValue: state.timeValue,
        title: action.title,
        unit: state.unit,
        startValue: state.startValue,
        difference: state.difference,
        sign: state.sign,
        id: state.id,
        topic: state.topic
      };

    case SET_COMPETITION_UNIT:
      return {
        competitionType: state.competitionType,
        postBannerPath: state.postBannerPath,
        content: state.content,
        goal: state.goal,
        timeUnit: state.timeUnit,
        timeValue: state.timeValue,
        title: state.title,
        unit: action.unit,
        startValue: state.startValue,
        difference: state.difference,
        sign: state.sign,
        id: state.id,
        topic: state.topic
      };

    case SET_COMPETITION_SIGN:
      return {
        competitionType: state.competitionType,
        postBannerPath: state.postBannerPath,
        content: state.content,
        goal: state.goal,
        timeUnit: state.timeUnit,
        timeValue: state.timeValue,
        title: state.title,
        unit: state.unit,
        startValue: state.startValue,
        difference: state.difference,
        sign: action.sign,
        id: state.id,
        topic: state.topic
      };

    case SET_COMPETITION_DIFFERENCE:
      return {
        competitionType: state.competitionType,
        postBannerPath: state.postBannerPath,
        content: state.content,
        goal: state.goal,
        timeUnit: state.timeUnit,
        timeValue: state.timeValue,
        title: state.title,
        unit: state.unit,
        startValue: state.startValue,
        difference: action.difference,
        sign: state.sign,
        id: state.id,
        topic: state.topic
      };

    case SET_COMPETITION_START_VALUE:
      return {
        competitionType: state.competitionType,
        postBannerPath: state.postBannerPath,
        content: state.content,
        goal: state.goal,
        timeUnit: state.timeUnit,
        timeValue: state.timeValue,
        title: state.title,
        unit: state.unit,
        startValue: action.startValue,
        difference: state.difference,
        sign: state.sign,
        id: state.id,
        topic: state.topic
      };

    case RESET_COMPETITION:
      return {
        type: null,
        postBannerPath: null,
        content: null,
        goal: null,
        timeUnit: 'days',
        timeValue: 14,
        title: null,
        unit: null,
        startValue: null,
        difference: null,
        sign: null,
        id: null,
        topic: null
      };

    default:
      return state;
  }
};

export default reducer;
