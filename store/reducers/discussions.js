import {
  SET_DISCUSSIONS,
  ADD_DISCUSSIONS,
  SET_ARCHIVED_DISCUSSIONS,
  ADD_ARCHIVED_DISCUSSIONS,
  SYNC_DISCUSSION,
  SET_CURRENT_DISCUSSION_ID,
  UPDATE_DISCUSSION_LATEST_MESSAGE,
  READ_DISCUSSION,
  ARCHIVE_DISCUSSION,
  UN_ARCHIVE_DISCUSSION,
  BLOCK_DISCUSSION,
  UN_BLOCK_DISCUSSION, BLOCK_AND_ARCHIVE_DISCUSSION
} from '../actions/actionTypes';

const INITIAL_STATE = {
  list: [],
  archivedList: [],
  currentDiscussionId: null
};

const updateDiscussion = (list, updatedDiscussion) => {
  const objIndex = list.findIndex(obj => obj.id === updatedDiscussion.id);
  list[objIndex] = updatedDiscussion;
  return [...list];
};

const updateLatestMessage = (list, discussionId, lastMessage, lastActionAt, hasNewMessage) => {
  return list.map((item, index) => {
    if (item && item.id !== discussionId) {
      return item;
    }
    return {
      ...item,
      lastActionAt: lastActionAt,
      lastMessage: { ...item.mesage, message: lastMessage },
      hasNewMessage: hasNewMessage
    };
  });
};

const readDiscussion = (list, discussionId) => {
  return list.map((item, index) => {
    if (item && item.id !== discussionId) {
      return item;
    }
    return {
      ...item,
      hasNewMessage: false
    };
  });
};

const unBlockDiscussion = (discussion, list) => {
  return list.map((item, index) => {
    if (item && item.id !== discussion.id) {
      return item;
    }
    return {
      ...item,
      blockedForUser: false
    };
  });
};

const blockDiscussion = (discussion, list) => {
  return list.map((item, index) => {
    if (item && item.id !== discussion.id) {
      return item;
    }
    return {
      ...item,
      blockedForUser: true
    };
  });
};

const removeDiscussion = (discussion, list) => {
  return list.map((item, index) => {
    if (item.id !== discussion.id) {
      return item;
    }
  });
};

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_DISCUSSIONS:
      return {
        ...state,
        list: action.discussions
      };
    case ADD_DISCUSSIONS:
      return {
        ...state,
        list: [...action.discussions, ...state.list]
      };
    case SET_ARCHIVED_DISCUSSIONS:
      return {
        ...state,
        archivedList: action.discussions
      };
    case ADD_ARCHIVED_DISCUSSIONS:
      return {
        ...state,
        archivedList: [...action.discussions, ...state.list]
      };
    case SYNC_DISCUSSION:
      return {
        ...state,
        list: updateDiscussion(state.list, action.discussion),
        archivedList: updateDiscussion(state.archivedList, action.discussion)
      };
    case SET_CURRENT_DISCUSSION_ID:
      return {
        ...state,
        currentDiscussionId: action.currentDiscussionId
      };
    case UPDATE_DISCUSSION_LATEST_MESSAGE:
      return {
        ...state,
        archivedList: updateLatestMessage(state.archivedList, action.discussionId, action.latestMessage, action.latestMessageAt, action.hasNewMessage),
        list: updateLatestMessage(state.list, action.discussionId, action.latestMessage, action.latestMessageAt, action.hasNewMessage)
      };
    case READ_DISCUSSION:
      return {
        ...state,
        archivedList: readDiscussion(state.archivedList, action.discussionId),
        list: readDiscussion(state.list, action.discussionId)
      };
    case ARCHIVE_DISCUSSION:
      return {
        ...state,
        archivedList: [action.discussion, ...state.archivedList],
        list: removeDiscussion(action.discussion, state.list)
      };
    case UN_ARCHIVE_DISCUSSION:
      return {
        ...state,
        archivedList: removeDiscussion(action.discussion, state.archivedList),
        list: [action.discussion, ...state.list]
      };
    case BLOCK_DISCUSSION:
      return {
        ...state,
        list: blockDiscussion(action.discussion, state.list),
        archivedList: blockDiscussion(action.discussion, state.archivedList)
      };
    case UN_BLOCK_DISCUSSION:
      return {
        ...state,
        archivedList: unBlockDiscussion(action.discussion, state.archivedList),
        list: unBlockDiscussion(action.discussion, state.list)
      };
    case BLOCK_AND_ARCHIVE_DISCUSSION: {
      return {
        ...state,
        archivedList: [{ ...action.discussion, blockedForUser: true }, ...state.archivedList],
        list: removeDiscussion(action.discussion, state.list)
      };
    }
    default: return state;
  }
};

export default reducer;
