import { UPDATE_ERROR } from '../actions/actionTypes';

const INITIAL_STATE = {
  code: 0,
  status: ''
};

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case UPDATE_ERROR:
      return { ...state, code: action.error.code, status: action.error.status };
    default:
      return state;
  }
};

export default reducer;
