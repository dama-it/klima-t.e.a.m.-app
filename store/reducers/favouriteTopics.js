
import {
  ADD_FAVOURITE_TOPICS,
  SET_FAVOURITE_TOPICS
} from '../actions/actionTypes';

const INITIAL_STATE = {
  list: [],
  currentTopic: null,
  currentTopicPostsList: [],
  currentTopicCompetitionsList: [],
  counter: 0,
  selected: false

};
const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_FAVOURITE_TOPICS:
      return {
        list: action.favouriteTopics,
        currentTopicPostsList: state.currentTopicPostsList,
        currentTopic: state.currentTopic,
        currentTopicCompetitionsList: state.currentTopicCompetitionsList,
        counter: state.counter,
        selected: state.selected
      };
    case ADD_FAVOURITE_TOPICS:
      return {
        list: action.favouriteTopics && action.favouriteTopics.length > 0 ? state.list.concat(action.favouriteTopics) : state.list,
        currentTopicPostsList: state.currentTopicPostsList,
        currentTopic: state.currentTopic,
        currentTopicCompetitionsList: state.currentTopicCompetitionsList,
        counter: state.counter,
        selected: state.selected
      };
    default:
      return state;
  }
};

export default reducer;
