import {
  UPDATE_IDENTITY, CLEAR_IDENTITY, INCREMENT_NEW_CHAT_MESSAGES_COUNT
} from '../actions/actionTypes';

const INITIAL_STATE = {
  id: '',
  username: '',
  email: '',
  firstName: '',
  lastName: '',
  userFullName: '',
  locale: '',
  status: '',
  xp: '',
  userImagePath: null,
  newMessages: 0,
  moderatorTopics: [],
  blockedMessagingUsers: []
};

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case UPDATE_IDENTITY:
      return {
        ...state,
        id: action.data.id,
        username: action.data.username,
        email: action.data.userEmail,
        firstName: action.data.userFirstName,
        lastName: action.data.userLastName,
        userFullName: action.data.userFullName,
        locale: action.data.locale,
        moderatorTopics: action.data.moderatorTopics,
        xp: action.data.xp,
        status: action.data.status,
        coins: action.data.coins,
        userImagePath: action.data.userImagePath,
        newMessages: action.data.newMessages,
        blockedMessagingUsers: action.data.blockedMessagingUsers
      };
    case CLEAR_IDENTITY:
      return {
        id: '',
        username: '',
        email: '',
        firstName: '',
        lastName: '',
        userFullName: '',
        locale: '',
        xp: '',
        status: '',
        userImagePath: '',
        coins: '',
        newMessages: 0,
        moderatorTopics: [],
        blockedMessagingUsers: []
      };
    case INCREMENT_NEW_CHAT_MESSAGES_COUNT:
      return {
        ...state,
        newMessages: state.newMessages + 1
      };
    default:
      return state;
  }
};

export default reducer;
