import {
  SET_MESSAGES,
  ADD_MESSAGES,
  SET_NEW_MESSAGES,
  READ_MESSAGE,
  READ_ALL_MESSAGES, REMOVE_LAST_MESSAGE
} from '../actions/actionTypes';

const INITIAL_STATE = {
  list: [],
  newMessages: false
};

const addMessages = (messages, newMessages) => {
  return newMessages && newMessages.length > 0 ? messages.concat(newMessages) : messages;
};
const readMessage = (message, messages) => {
  if (!message) {
    return null;
  }
  message.seen = true;
  const objIndex = messages.findIndex(obj => obj.id === message.id);
  messages[objIndex] = message;
  return [...messages];
};

const readAllMessages = (messages) => {
  messages.forEach((message, index) => { messages[index].seen = true; });
  return [...messages];
};

const checkIfMessagesUnread = (messages) => {
  let hasUnread = false;
  messages.forEach(message => { if (!message.seen) hasUnread = true; });
  return hasUnread;
};

const removeLastMessage = list => {
  list.shift();
  return [...list];
};

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_MESSAGES:
      return {
        list: action.messages,
        newMessages: state.newMessages
      };
    case ADD_MESSAGES:
      return {
        list: addMessages(state.list, action.messages),
        newMessages: state.newMessages
      };
    case SET_NEW_MESSAGES:
      return {
        list: state.list,
        newMessages: action.newMessages
      };
    case READ_MESSAGE:
      return {
        list: readMessage(action.message, state.list),
        newMessages: checkIfMessagesUnread(readMessage(action.message, state.list))
      };
    case READ_ALL_MESSAGES:
      return {
        list: readAllMessages(state.list),
        newMessages: 0
      };
    case REMOVE_LAST_MESSAGE: {
      return {
        list: removeLastMessage(state.list),
        newMessages: state.newMessages
      };
    }
    default:
      return state;
  }
};

export default reducer;
