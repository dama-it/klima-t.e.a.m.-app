import { SET_POPULAR_POSTS, ADD_POPULAR_POSTS, SELECT_POPULAR_POST, SYNC_POPULAR_POST, SET_FAV_POPULAR_POSTS, SYNC_FAV_POPULAR_POST, ADD_FAV_POPULAR_POSTS, FAV_POPULAR_POST, UP_VOTE_POPULAR_POST, DOWN_VOTE_POPULAR_POST, FAVOURITE_POPULAR_POST } from '../actions/actionTypes';

const INITIAL_STATE = {
  list: [],
  favList: [],
  currentPost: null,
  counter: 0
};

const updatePosts = (posts, post) => {
  if (!posts) {
    return null;
  }
  if (!posts) return;
  let updated = false;
  const modifiedPosts = posts.map((item, index) => {
    if (item.id !== post.id) {
      return item;
    }
    updated = true;
    return { ...post };
  });
  if (updated) { return modifiedPosts; } else { return posts; }
};

const upVoteListPost = (posts, upVotedPost) => {
  let updated = false;
  const modifiedPosts = posts.map(post => {
    if (post.id !== upVotedPost.id) {
      return post;
    }
    updated = true;
    return { ...upVotePost(upVotedPost) };
  });
  if (updated) { return modifiedPosts; } else { return posts; }
};

const downVoteListPost = (posts, downVotedPost) => {
  let updated = false;
  const modifiedPosts = posts.map(post => {
    if (post.id !== downVotedPost.id) {
      return post;
    }
    updated = true;
    return { ...downVotePost(downVotedPost) };
  });
  if (updated) { return modifiedPosts; } else { return posts; }
};

const upVotePost = (post) => {
  const newPost = { ...post };
  if (post.currentUsersVote == null) {
    newPost.upVotesCount++;
    newPost.currentUsersVote = true;
  }
  if (post.currentUsersVote === false) {
    newPost.downVotesCount--;
    newPost.upVotesCount++;
    newPost.currentUsersVote = true;
  }
  if (post.currentUsersVote === true) {
    return { ...newPost };
  }
  return { ...newPost };
};

const downVotePost = (post) => {
  const newPost = { ...post };
  if (post.currentUsersVote == null) {
    newPost.downVotesCount++;
    newPost.currentUsersVote = false;
  }
  if (post.currentUsersVote === true) {
    newPost.downVotesCount++;
    newPost.upVotesCount--;
    newPost.currentUsersVote = false;
  }
  if (post.currentUsersVote === false) {
    return { ...newPost };
  }
  return { ...newPost };
};

const getPinnedPostsIndezes = (posts) => {
  const pinnedPostsAt = [];
  for (let i = 0; i < posts.length; i++) {
    if (posts[i].isPinned) {
      pinnedPostsAt.push(i);
    }
  }
  return pinnedPostsAt;
};

const setPinnedPostsAtTheBeginning = (posts) => {
  const pinnedPostsIndexes = getPinnedPostsIndezes(posts);
  const pinnedPosts = [];
  for (let i = 0; i < pinnedPostsIndexes.length; i++) {
    const p = posts.splice(pinnedPostsIndexes[i], 1);
    if (p.length) {
      pinnedPosts.push(p[0]);
    }
  }
  return pinnedPosts.concat(posts);
};

const addPopularPosts = (posts, newPosts) => {
  const ps = newPosts && newPosts.length > 0 ? posts.concat(newPosts) : posts;
  return setPinnedPostsAtTheBeginning(ps);
};

const updatePopularPosts = (posts, post) => {
  if (!posts) return null;
  let updated = false;
  const modifiedPosts = posts.map((item, index) => {
    if (item.id !== post.id) {
      return item;
    }
    updated = true;
    return { ...post };
  });
  if (updated) { return modifiedPosts; } else { return posts; }
};

const updateFavPopularPosts = (posts, post) => {
  const objIndex = posts.findIndex(obj => obj.id === post.id);
  if (post.isFavourite !== true) {
    posts.splice(objIndex, 1);
  } else {
    posts[objIndex] = post;
  }
  return setPinnedPostsAtTheBeginning(posts);
};

const updateCurrentPopularPost = (post, newPost) => {
  if (post && post.id === newPost.id) {
    return newPost;
  }
  return post;
};

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_FAV_POPULAR_POSTS:
      return {
        list: state.list,
        favList: setPinnedPostsAtTheBeginning(action.posts),
        currentPost: state.currentPost,
        counter: state.counter
      };
    case SET_POPULAR_POSTS:
      return {
        list: setPinnedPostsAtTheBeginning(action.posts),
        favList: state.favList,
        currentPost: state.currentPost,
        counter: state.counter
      };
    case ADD_POPULAR_POSTS:
      return {
        list: addPopularPosts(state.list, action.posts),
        favList: state.favList,
        currentPost: state.currentPost,
        counter: state.counter
      };
    case ADD_FAV_POPULAR_POSTS:
      return {
        list: updatePosts(state.list, { ...action.post, isFavourite: !action.post.isFavourite }),
        favList: updatePosts(state.favList, { ...action.post, isFavourite: !action.post.isFavourite }),
        currentPost: state.currentPost,
        counter: state.counter
      };
    case SELECT_POPULAR_POST:
      return {
        currentPost: action.post,
        favList: state.favList,
        list: state.list,
        counter: state.counter
      };
    case SYNC_POPULAR_POST:
      return {
        currentPost: updateCurrentPopularPost(state.currentPost, action.post),
        list: updatePopularPosts(state.list, action.post),
        favList: state.favList,
        counter: ++state.counter
      };
    case SYNC_FAV_POPULAR_POST:
      return {
        currentPost: updateCurrentPopularPost(state.currentPost, action.post),
        favList: updateFavPopularPosts(state.favList, action.post),
        list: state.list,
        counter: ++state.counter
      };
    case UP_VOTE_POPULAR_POST:
      return {
        currentPost: state.currentPost,
        favList: state.favList,
        list: upVoteListPost(state.list, action.post),
        counter: ++state.counter
      };
    case DOWN_VOTE_POPULAR_POST:
      return {
        currentPost: state.currentPost,
        favList: state.favList,
        list: downVoteListPost(state.list, action.post),
        counter: ++state.counter
      };
    case FAV_POPULAR_POST:
      return {
        currentPost: state.currentPost,
        favList: state.favList,
        list: updatePopularPosts(state.list, { ...action.post, isFavourite: !action.post.isFavourite }),
        counter: ++state.counter
      };
    default:
      return state;
  }
};

export default reducer;
