import {
  SET_POSTS,
  ADD_POSTS,
  SELECT_POST,
  SYNC_POST,
  SET_FAV_POSTS,
  SYNC_FAV_POST,
  ADD_FAV_POSTS,
  INCREMENT_COMMENT_COUNT,
  SET_PROFILE_POSTS,
  ADD_PROFILE_POSTS,
  UP_VOTE_POST,
  DOWN_VOTE_POST,
  FAVOURITE_POST,
  ADD_POST_AT_TOP,
  DELETE_POST
} from '../actions/actionTypes';

const INITIAL_STATE = {
  list: [],
  profileList: [],
  favList: [],
  currentPost: null,
  counter: 0
};

const getPinnedPostsIndezes = (posts) => {
  const pinnedPostsAt = [];
  for (let i = 0; i < posts.length; i++) {
    if (posts[i].isPinned) {
      pinnedPostsAt.push(i);
    }
  }
  return pinnedPostsAt;
};

const setPinnedPostsAtTheBeginning = (posts) => {
  const pinnedPostsIndexes = getPinnedPostsIndezes(posts);
  const pinnedPosts = [];
  for (let i = 0; i < pinnedPostsIndexes.length; i++) {
    const p = posts.splice(pinnedPostsIndexes[i], 1);
    if (p.length) {
      pinnedPosts.push(p[0]);
    }
  }
  return pinnedPosts.concat(posts);
};

const addPosts = (posts, newPosts) => {
  const ps = newPosts && newPosts.length > 0 ? posts.concat(newPosts) : posts;
  return setPinnedPostsAtTheBeginning(ps);
};

const addItemAtTop = (item, list) => {
  return [item, ...list];
};

const updatePosts = (posts, post) => {
  if (!posts) {
    return null;
  }
  if (!posts) return;
  let updated = false;
  const modifiedPosts = posts.map((item, index) => {
    if (item.id !== post.id) {
      return item;
    }
    updated = true;
    return { ...post };
  });
  if (updated) { return modifiedPosts; } else { return posts; }
};

const updateFavPosts = (posts, post) => {
  const objIndex = posts.findIndex(obj => obj.id === post.id);
  if (post.isFavourite !== true) {
    posts.splice(objIndex, 1);
  } else {
    posts[objIndex] = post;
  }
  return setPinnedPostsAtTheBeginning(posts);
};

const updateCurrentPost = (post, newPost) => {
  if (post && post.id === newPost.id) {
    return newPost;
  }
  return post;
};

const incrementCommentsCount = (post) => {
  post.commentsCount = post.commentsCount + 1;
  return post;
};

const upVoteListPost = (posts, upVotedPost) => {
  if (!posts) return;
  let updated = false;
  const modifiedPosts = posts.map(post => {
    if (post.id !== upVotedPost.id) {
      return post;
    }
    updated = true;
    return { ...upVotePost(upVotedPost) };
  });
  if (updated) { return modifiedPosts; } else { return posts; }
};

const downVoteListPost = (posts, downVotedPost) => {
  if (!posts) return;
  let updated = false;
  const modifiedPosts = posts.map(post => {
    if (post.id !== downVotedPost.id) {
      return post;
    }
    updated = true;
    return { ...downVotePost(downVotedPost) };
  });
  if (updated) { return modifiedPosts; } else { return posts; }
};

const upVotePost = (post) => {
  const newPost = { ...post };
  if (post.currentUsersVote == null) {
    newPost.upVotesCount++;
    newPost.currentUsersVote = true;
  }
  if (post.currentUsersVote === false) {
    newPost.downVotesCount--;
    newPost.upVotesCount++;
    newPost.currentUsersVote = true;
  }
  if (post.currentUsersVote === true) {
    return { ...newPost };
  }
  return { ...newPost };
};

const downVotePost = (post) => {
  const newPost = { ...post };
  if (post.currentUsersVote == null) {
    newPost.downVotesCount++;
    newPost.currentUsersVote = false;
  }
  if (post.currentUsersVote === true) {
    newPost.downVotesCount++;
    newPost.upVotesCount--;
    newPost.currentUsersVote = false;
  }
  if (post.currentUsersVote === false) {
    return { ...newPost };
  }
  return { ...newPost };
};

const deletePostFromList = (list, postId) => {
  return list.filter(post => post.id !== postId);
};

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_FAV_POSTS:
      return {
        list: state.list,
        profileList: state.profileList,
        favList: setPinnedPostsAtTheBeginning(action.posts),
        currentPost: state.currentPost,
        counter: state.counter
      };
    case SET_POSTS:
      return {
        list: setPinnedPostsAtTheBeginning(action.posts),
        profileList: state.profileList,
        favList: state.favList,
        currentPost: state.currentPost,
        counter: state.counter
      };
    case ADD_POSTS:
      return {
        list: addPosts(state.list, action.posts),
        profileList: state.profileList,
        favList: state.favList,
        currentPost: state.currentPost,
        counter: state.counter
      };
    case ADD_POST_AT_TOP:
      return {
        list: addItemAtTop(action.post, state.list),
        profileList: addItemAtTop(action.post, state.profileList),
        favList: addItemAtTop(action.post, state.favList),
        currentPost: action.post,
        counter: state.counter

      };
    case SET_PROFILE_POSTS:
      return {
        profileList: setPinnedPostsAtTheBeginning(action.posts),
        list: state.list,
        favList: state.favList,
        currentPost: state.currentPost,
        counter: state.counter
      };
    case ADD_PROFILE_POSTS:
      return {
        profileList: addPosts(state.profileList, action.posts),
        list: state.list,
        favList: state.favList,
        currentPost: state.currentPost,
        counter: state.counter
      };

    case ADD_FAV_POSTS:
      return {
        list: state.list,
        profileList: state.profileList,
        favList: addPosts(state.favList, action.posts),
        currentPost: state.currentPost,
        counter: state.counter
      };
    case SELECT_POST:
      return {
        currentPost: action.post,
        favList: state.favList,
        list: state.list,
        profileList: state.profileList,
        counter: state.counter
      };
    case SYNC_POST:
      return {
        currentPost: updateCurrentPost(state.currentPost, action.post),
        list: updatePosts(state.list, action.post),
        profileList: updatePosts(state.profileList, action.post),
        favList: state.favList,
        counter: ++state.counter
      };
    case SYNC_FAV_POST:
      return {
        currentPost: updateCurrentPost(state.currentPost, action.post),
        favList: updateFavPosts(state.favList, action.post),
        list: state.list,
        profileList: state.profileList,
        counter: ++state.counter
      };
    case INCREMENT_COMMENT_COUNT:
      return {
        currentPost: incrementCommentsCount(state.currentPost),
        favList: state.favList,
        list: state.list,
        profileList: state.profileList,
        counter: state.counter
      };
    case UP_VOTE_POST:
      return {
        currentPost: upVotePost(action.post),
        favList: upVoteListPost(state.favList, action.post),
        list: upVoteListPost(state.list, action.post),
        profileList: upVoteListPost(state.profileList, action.post),
        counter: state.counter
      };
    case DOWN_VOTE_POST:
      return {
        currentPost: downVotePost(action.post),
        favList: downVoteListPost(state.favList, action.post),
        list: downVoteListPost(state.list, action.post),
        profileList: downVoteListPost(state.profileList, action.post),
        counter: state.counter
      };
    case FAVOURITE_POST:
      return {
        currentPost: { ...action.post, isFavourite: !action.post.isFavourite },
        favList: updatePosts(state.favList, { ...action.post, isFavourite: !action.post.isFavourite }),
        list: updatePosts(state.list, { ...action.post, isFavourite: !action.post.isFavourite }),
        profileList: updatePosts(state.profileList, { ...action.post, isFavourite: !action.post.isFavourite }),
        counter: state.counter
      };
    case DELETE_POST:
      return {
        currentPost: state.currentPost,
        favList: deletePostFromList(state.favList, action.postId),
        list: deletePostFromList(state.list, action.postId),
        profileList: deletePostFromList(state.list, action.postId),
        counter: state.counter
      };
    default:
      return state;
  }
};

export default reducer;
