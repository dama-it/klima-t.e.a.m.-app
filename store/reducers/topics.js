
import {
  SET_TOPICS,
  ADD_TOPICS,
  SELECT_TOPIC,
  SYNC_TOPIC,
  ADD_TOPIC_POSTS,
  ADD_COMPETITION_TO_TOPIC,
  SET_TOPIC_POSTS,
  SET_FAVOURITE_TOPICS,
  ADD_POST_TO_TOPIC,
  SYNC_TOPIC_POST,
  SET_TOPIC_COMPETITIONS,
  ADD_TOPIC_COMPETITIONS,
  FAVOURITE_TOPIC_POST,
  FAVOURITE_TOPIC,
  UP_VOTE_TOPIC_POST,
  DOWN_VOTE_TOPIC_POST,
  SELECT_TOPIC_SCREEN,
  DELETE_TOPIC_POST,
  ADD_FAVOURITE_TOPICS
} from '../actions/actionTypes';

const INITIAL_STATE = {
  list: [],
  currentTopic: null,
  currentTopicPostsList: [],
  currentTopicCompetitionsList: [],
  counter: 0,
  selected: false

};

const updateTopics = (topics, topic) => {
  if (!topics) {
    return null;
  }
  return topics.map((item, index) => {
    if (item.id !== topic.id) {
      return item;
    }
    return { ...item, ...topic };
  });
};

const upVotePost = (post) => {
  const newPost = { ...post };
  if (post.currentUsersVote == null) {
    newPost.upVotesCount++;
    newPost.currentUsersVote = true;
  }
  if (post.currentUsersVote === false) {
    newPost.downVotesCount--;
    newPost.upVotesCount++;
    newPost.currentUsersVote = true;
  }
  if (post.currentUsersVote === true) {
    return { ...newPost };
  }
  return { ...newPost };
};

const downVotePost = (post) => {
  const newPost = { ...post };
  if (post.currentUsersVote == null) {
    newPost.downVotesCount++;
    newPost.currentUsersVote = false;
  }
  if (post.currentUsersVote === true) {
    newPost.downVotesCount++;
    newPost.upVotesCount--;
    newPost.currentUsersVote = false;
  }
  if (post.currentUsersVote === false) {
    return { ...newPost };
  }
  return { ...newPost };
};

const upVoteListPost = (posts, upVotedPost) => {
  return posts.map(post => {
    if (post.id !== upVotedPost.id) {
      return post;
    }
    return { ...upVotePost(upVotedPost) };
  });
};

const downVoteListPost = (posts, downVotedPost) => {
  return posts.map(post => {
    if (post.id !== downVotedPost.id) {
      return post;
    }
    return { ...downVotePost(downVotedPost) };
  });
};

const updateCurrentTopic = (topic, newTopic) => {
  if (topic && topic.id === newTopic.id) {
    return newTopic;
  }
  return topic;
};

const updateCurrentTopicPostList = (posts, post) => {
  const objIndex = posts.findIndex(obj => obj.id === post.id);
  posts[objIndex] = post;
  return posts;
};

const deletePostFromList = (list, postId) => {
  return list.filter(post => post.id !== postId);
};

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_TOPICS:
      return {
        list: action.topics,
        currentTopicPostsList: state.currentTopicPostsList,
        currentTopic: state.currentTopic,
        currentTopicCompetitionsList: state.currentTopicCompetitionsList,
        counter: state.counter,
        selected: state.selected
      };

    case ADD_TOPICS:
      return {
        list: action.topics && action.topics.length > 0 ? state.list.concat(action.topics) : state.list,
        currentTopicPostsList: state.currentTopicPostsList,
        currentTopic: state.currentTopic,
        currentTopicCompetitionsList: state.currentTopicCompetitionsList,
        counter: state.counter,
        selected: state.selected
      };
    case ADD_COMPETITION_TO_TOPIC:
      return {
        ...state,
        currentTopicCompetitionsList: [action.competition, ...state.currentTopicCompetitionsList]
      };

    case ADD_POST_TO_TOPIC:
      return {
        ...state,
        currentTopicPostsList: [action.post, ...state.currentTopicPostsList]
      };

    case SELECT_TOPIC:
      return {
        currentTopic: action.topic,
        currentTopicPostsList: state.currentTopicPostsList,
        list: state.list,
        currentTopicCompetitionsList: state.currentTopicCompetitionsList,
        counter: state.counter,
        selected: state.selected
      };
    case SYNC_TOPIC:
      return {
        currentTopic: updateCurrentTopic(state.currentTopic, action.topic),
        currentTopicPostsList: state.currentTopicPostsList,
        currentTopicCompetitionsList: state.currentTopicCompetitionsList,
        list: updateTopics(state.list, action.topic),
        counter: ++state.counter,
        selected: state.selected
      };
    case SET_TOPIC_POSTS:
      return {
        currentTopic: state.currentTopic,
        currentTopicPostsList: action.posts,
        currentTopicCompetitionsList: state.currentTopicCompetitionsList,
        list: state.list,
        counter: ++state.counter,
        selected: state.selected
      };
    case SET_TOPIC_COMPETITIONS:
      return {
        currentTopic: state.currentTopic,
        currentTopicPostsList: state.currentTopicPostsList,
        currentTopicCompetitionsList: action.posts,
        list: state.list,
        counter: ++state.counter,
        selected: state.selected
      };
    case ADD_TOPIC_POSTS:
      return {
        currentTopicPostsList: action.posts && action.posts.length > 0 ? state.currentTopicPostsList.concat(action.posts) : state.currentTopicPostsList,
        list: state.list,
        currentTopic: state.currentTopic,
        currentTopicCompetitionsList: state.currentTopicCompetitionsList,
        counter: ++state.counter,
        selected: state.selected
      };
    case ADD_TOPIC_COMPETITIONS:
      return {
        currentTopicPostsList: state.currentTopicPostsList,
        list: state.list,
        currentTopic: state.currentTopic,
        currentTopicCompetitionsList: action.posts && action.posts.length > 0
          ? state.currentTopicCompetitionsList.concat(action.posts)
          : state.currentTopicCompetitionsList,
        counter: ++state.counter,
        selected: state.selected
      };
    case SYNC_TOPIC_POST:
      return {
        currentTopic: state.currentTopic,
        currentTopicPostsList: updateCurrentTopicPostList(state.currentTopicPostsList, action.post),
        currentTopicCompetitionsList: updateCurrentTopicPostList(state.currentTopicCompetitionsList, action.post),
        list: state.list,
        counter: ++state.counter,
        selected: state.selected
      };
    case FAVOURITE_TOPIC:
      return {
        currentTopic: { ...action.topic, isFavourite: !action.topic.isFavourite },
        currentTopicPostsList: state.currentTopicPostsList,
        currentTopicCompetitionsList: state.currentTopicCompetitionsList,
        list: updateTopics(state.list, { ...action.topic, isFavourite: !action.topic.isFavourite }),
        selected: state.selected
      };
    case FAVOURITE_TOPIC_POST:
      return {
        currentTopic: state.currentTopic,
        currentTopicPostsList: updateTopics(state.currentTopicPostsList, { ...action.post, isFavourite: !action.post.isFavourite }),
        currentTopicCompetitionsList: updateTopics(state.currentTopicCompetitionsList, { ...action.post, isFavourite: !action.post.isFavourite }),
        list: state.list,
        selected: state.selected
      };
    case UP_VOTE_TOPIC_POST:
      return {
        currentTopic: state.currentTopic,
        currentTopicPostsList: upVoteListPost(state.currentTopicPostsList, action.post),
        currentTopicCompetitionsList: upVoteListPost(state.currentTopicCompetitionsList, action.post),
        list: state.list,
        selected: state.selected
      };
    case DOWN_VOTE_TOPIC_POST:
      return {
        currentTopic: state.currentTopic,
        currentTopicPostsList: downVoteListPost(state.currentTopicPostsList, action.post),
        currentTopicCompetitionsList: downVoteListPost(state.currentTopicCompetitionsList, action.post),
        list: state.list,
        selected: state.selected
      };
    case SELECT_TOPIC_SCREEN:
      return {
        ...state,
        selected: action.selected
      };
    case DELETE_TOPIC_POST:
      return {
        ...state,
        currentTopicPostsList: deletePostFromList(state.currentTopicPostsList, action.postId),
        currentTopicCompetitionsList: deletePostFromList(state.currentTopicCompetitionsList, action.postId)
      };
    default:
      return state;
  }
};

export default reducer;
