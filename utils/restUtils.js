import restClient from '../providers/dataProvider/rest';
import {GET_ONE} from '../providers/types';


export const getIdentity = () => {
    return new Promise((resolve, reject) => {
      restClient(GET_ONE, 'identity').then(data => {
        if (data.status && data.code !== 200) {
          reject(data);
        }
        resolve(data);
      })
        .catch(e => {
          reject(e);
        });
    });
  };